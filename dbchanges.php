05/02/2021

CREATE TABLE `customer_master` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`first_name` varchar(45) DEFAULT NULL,
`last_name` varchar(45) DEFAULT NULL,
`mobile` varchar(45) DEFAULT NULL,
`email` varchar(45) DEFAULT NULL,
`google_id` varchar(45) DEFAULT NULL,
`username_alias` varchar(45) DEFAULT NULL,
`otp` varchar(45) DEFAULT NULL,
`email_otp` varchar(45) DEFAULT NULL,
`profile_pic` varchar(100) DEFAULT NULL,
`login_type` varchar(45) DEFAULT NULL,
`wallet_money` decimal(10,2) NOT NULL DEFAULT '0.00',
`referral_code` varchar(45) DEFAULT NULL,
`referred_user_id` int(11) NOT NULL DEFAULT '0',
`benificiary_id` varchar(45) DEFAULT NULL,
`created` datetime DEFAULT NULL,
`is_referral_amount_disbursed` int(11) NOT NULL DEFAULT '0',
`payment_mode` varchar(45) NOT NULL DEFAULT '0',
`is_active` int(11) DEFAULT '0',
`is_deleted` int(11) DEFAULT '0',
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8mb4