<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Home_model extends CI_Model
{
    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */
    public function addnNewUserData($userDatas)
    {
        $this->db->insert('user_files', $userDatas);
        $insid =   $this->db->insert_id();
        if ($insid) {
            $filenme =   $this->db->select('file,filesid')->where('filesid', $insid)->get('user_files')->row();
            $file = FCPATH . $filenme->file;
            $tmppath = FCPATH . 'assets/videothump/' . $filenme->filesid . '_' . time() . '.jpg';
            $savepath = 'assets/videothump/' . $filenme->filesid . '_' . time() . '.jpg';
            // windows command
            // $cmd = "ffmpeg -i $file -an -ss 5  -s 120*90 -vframes 1 $tmppath";
            // linux command
            $cmd = " ffmpeg -i $file -an -ss 5  -vframes 1 $tmppath";
            exec($cmd);
            $this->db->set('video_thump', $savepath)->where('filesid', $insid)->update('user_files');
            return $this->db->affected_rows();
        }
    }
    public function getAllFilesByUserId($userId)
    {
        $user_reslt = $this->db->select('userid,file,hashtag,file_type,created_date,filesid')->where('userid', $userId)->where('is_active', 0)->get('user_files')->result();
        if ($user_reslt) {
            return  $user_reslt;
        } else {
            return 'null';
        }
    }
    public function deleteUserFileById($userId, $filesId)
    {
        $files =  $this->db->select('file_location as file')->where('filesid', $filesId)->get('user_files')->row();
        unlink($files->file);
        $this->db->set('is_active', 3)->where('filesid', $filesId)->update('user_files');
        return $this->db->affected_rows();
    }
    public function getHomdeDatas($userId, $latitude, $longitude, $postalcode, $districtName)
    {
        // $this->db->select('restaurants.restaurant_id,restaurants.district,district.district_name,settings.maximum_delivery_distance')->join('district', 'district.district_id = restaurants.district', 'inner')->join('settings', 'settings.restaurant_id = restaurants.restaurant_id', 'inner')->where('district.district_name', $districtName)->get('restaurants')->result();

        $dname =  $this->db->query("select district_id from district Where district.district_name = '$districtName'")->row();
        if ($dname) {
            $restauranthome = [];
            $allrestaurant =   $this->db->query("SELECT `restaurants`.`restaurant_id`,`restaurants`.`latitude`,`restaurants`.`longitude`, `settings`.`maximum_delivery_distance`,`partners_login`.`is_active`,`partners_login`.`is_online`
            FROM `restaurants`
            INNER JOIN `settings` ON `settings`.`restaurant_id` = `restaurants`.`restaurant_id`
            INNER JOIN `partners_login` ON `partners_login`.`partners_id` = `restaurants`.`restaurant_id`
            WHERE `restaurants`.`district` = $dname->district_id AND `partners_login`.`is_active` = 1")->result();

            if (!empty($allrestaurant)) {

                foreach ($allrestaurant as $key =>  $allres_val) {
                    $res_lati = trim($allres_val->latitude);
                    $res_longi = trim($allres_val->longitude);
                    // $respicode = $allres_val->pincode;
                    // $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=km&origins=$postalcode&destinations=$respicode&key=AIzaSyC3QHmx8r2yfay_GG0IhfCJW8mNjBjmCyU";
                    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=km&origins=$latitude,$longitude&destinations=$res_lati,$res_longi&key=AIzaSyC3QHmx8r2yfay_GG0IhfCJW8mNjBjmCyU";

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $response_a = json_decode($response, true);





                    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];

                    $imp = explode(' ', $dist);


                    if ($imp[1] == 'km') {
                        $distances = floor($imp[0]);
                    } else {
                        $distances  = 1;
                    }

                    if (($distances <=  $allres_val->maximum_delivery_distance)) {
                        $restResult =   $this->db->query("SELECT `restaurants`.`restaurant_id`,`restaurants`.`reasturant_image_one`,`restaurants`.`reasturant_image_two`,`restaurants`.`reasturant_image_three`, `restaurants`.`restaurant_name`, `restaurants`.`contact_no`, `restaurants`.`pincode`, `restaurants`.`longitude`, `restaurants`.`latitude`, `restaurants`.`status`,`restaurants`.`restaurant_category`, `partners_login`.`is_active` FROM `restaurants` 
                        INNER JOIN `partners_login` ON `partners_login`.`partners_id` = `restaurants`.`restaurant_id`
                         WHERE `restaurants`.`restaurant_id` = $allres_val->restaurant_id AND `restaurants`.`status` = 1  AND `partners_login`.`partner_type` = 2")->result();
                        if ($restResult) {
                            $nearRestaurant = new stdClass();
                            foreach ($restResult as $key2 => $r_values) {
                                $nearRestaurant->id = $r_values->restaurant_id;
                                $nearRestaurant->rest_name = $r_values->restaurant_name;
                                $nearRestaurant->rest_image = base_url() . $r_values->reasturant_image_one;
                                $nearRestaurant->rest_timing = $this->restaurantSettings($r_values->restaurant_id);
                                $nearRestaurant->rest_min_order = $this->restaurantMInorder($r_values->restaurant_id);
                                $nearRestaurant->rest_address = array('longitude' => $r_values->longitude, 'latitude' => $r_values->latitude, 'location_name' => $this->getRestaurantLocName($r_values->restaurant_id), 'location_pin' => $r_values->pincode, 'location_distance' => $dist);
                                $nearRestaurant->rest_type = $r_values->restaurant_category;
                                $nearRestaurant->isRestaurantOpen = $this->isRestaurantOpen($r_values->restaurant_id);
                                // $nearRestaurant->contact_no = $r_values->contact_no;
                                // $nearRestaurant->pincode = $r_values->pincode;
                                // $nearRestaurant->distance = $dist;
                                // $nearRestaurant->longitude = $r_values->longitude;
                                // $nearRestaurant->latitude = $r_values->latitude;
                                // $nearRestaurant->latitude = $r_values->latitude;
                                // $nearRestaurant->restaurant_category = $r_values->restaurant_category;

                                // $nearRestaurant->reasturant_image_two = $r_values->reasturant_image_two;
                                // $nearRestaurant->reasturant_image_three = $r_values->reasturant_image_three;
                                // if ($r_values->status == 1) {
                                //     $nearRestaurant->restaurantStatus = true;
                                // } else {
                                //     $nearRestaurant->restaurantStatus = false;
                                // }
                                // $nearRestaurant->restaurant_promotion = $this->restaurantsAdPromotions($r_values->restaurant_id);
                                // $nearRestaurant->restaurant_settings = $this->restaurantSettings($r_values->restaurant_id);
                                $restauranthome[] = $nearRestaurant;
                            }
                        }
                    }
                }
                return $restauranthome;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    public function isRestaurantOpen($resId)
    {

        // $settings =   $this->db->select('opening_time,closing_time')->where('restaurant_id', $resId)->get('settings')->row();
        $isonline =   $this->db->select('is_online')->where('partners_id', $resId)->where('partner_type', 2)->get('partners_login')->row();

        if ($isonline->is_online == 1) {


            $time = date('H:i');
            $wre = " '$time' between 'opening_time' AND 'closing_time'";
            $sts =  $this->db->query("SELECT `opening_time`
           FROM `settings`
           WHERE `restaurant_id` = $resId
           AND '$time' between opening_time AND closing_time")->row();

            if ($sts) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    function getRestaurantLocName($id)
    {
        $cityname = $this->db->select('restaurants.city as cityId,city.city_name,district.district_name')->join('city', 'city.city_id = restaurants.city', 'inner')->join('district', 'district.district_id = restaurants.district', 'inner')->get('restaurants')->row();
        return $cityname->district_name . ',' . $cityname->city_name;
    }
    function restaurantsAdPromotions($id)
    {
        $promotion =   $this->db->select('promotion_image,promotion_text')->where('restaurant_id', $id)->get('restaurant_promotion')->row();
        if (!$promotion) {
            return (object)['status' => false,  'message' => 'Sorry,promotion not  found'];
        } else {
            return (object)['status' => true,  'message' => $promotion];
        }
    }
    function restaurantSettings($id)
    {
        $settings =   $this->db->select('opening_time,closing_time')->where('restaurant_id', $id)->get('settings')->row();
        if (!$settings) {
            return [];
        } else {
            return  array('opening_time' => date('h:i A', strtotime($settings->opening_time)), 'closing_time' => date('h:i A', strtotime($settings->closing_time)));
            // return $settings;
        }
    }

    public function restaurantMInorder($id)
    {
        $settings =   $this->db->select('minimum_order_amount')->where('restaurant_id', $id)->get('settings')->row();
        if (!$settings) {
            return '';
        } else {
            return $settings->minimum_order_amount;
        }
    }

    public function getHashtags()
    {
        return   $this->db->get('hash_tag')->result();
    }

    public function getuserDeliveryAddress($userId)
    {
        $result =  $this->db->select('first_name,nickName,profile_image,id,is_active,account_type')->where('id', $userId)->get('users')->row();

        if ($result) {

            $usrs = new stdClass();
            $usrs->first_name = $result->first_name;
            $usrs->nickName = $result->nickName;
            $usrs->profile_image = base_url() . $result->profile_image;
            $usrs->is_active = $result->is_active;
            $usrs->account_type = $result->account_type;
            $usrs->deliveryAddresses = $this->getuserDefaultDeliveryAdrs($result->id);
            return $usrs;
        } else {
            return false;
        }
    }

    public function getuserDefaultDeliveryAdrs($id)
    {
        $adrs = $this->db->select('users_address.users_address_id as addressId,users_address.latitude,users_address.longitude,users_address.landmark,users_address.location_address,users_address.address,users_address.pincode,users_address.phone_number')->where('users_address.user_id', $id)->where('users_address.is_deleted', 0)->where('users_address.is_default', 1)->get('users_address')->row();
        if ($adrs) {
            return  $adrs;
        } else {
            return [];
        }
    }

    public function getAdpromotions($districtName)
    {
        $ads = [];
        $dname =  $this->db->query("select district_id,country_id,state_id from district Where district.district_name = '$districtName'")->result();
        if ($dname) {

            $allcountry = [];
            $allstates = [];
            $alldistricts = [];
            $allparticular_district = [];
            foreach ($dname  as $k => $catvl) {
                $allcountry[] = $catvl->country_id;
                $allstates[] = $catvl->state_id;
                $alldistricts[] = $catvl->district_id;
            }
            $allcntry = (implode(",", $allcountry));
            $allsts = (implode(",", $allstates));
            $alldstrcts = (implode(",", $alldistricts));

            $date = date('Y-m-d');
            $wre = " 'starting_date' >= ' " . $date . " ' AND '" . $date . "' <='ending_date' ";
            $country =   $this->db->select('ad_promotion_id')->where_in('country_id', $allcntry)->where('is_active', 1)->where('is_deleted', 0)->where('state_id', 'all')->where($wre)->get('ad_promotion')->result();

            $state =   $this->db->select('ad_promotion_id')->where_in('country_id',  $allcntry)->where('state_id', 'all')->where('district_id', 'all')->where('is_active', 1)->where('is_deleted', 0)->where($wre)->get('ad_promotion')->result();

            $district =   $this->db->select('ad_promotion_id')->where_in('country_id',  $allcntry)->where_in('state_id', $allsts)->where('district_id', 'all')->where('is_active', 1)->where('is_deleted', 0)->where($wre)->get('ad_promotion')->result();

            $particular_district =   $this->db->select('ad_promotion_id')->where_in('country_id',  $allcntry)->where_in('state_id', $allsts)->where_in('district_id', $alldstrcts)->where($wre)->where('is_active', 1)->where('is_deleted', 0)->get('ad_promotion')->result();

            $worldId = array_merge($country, $state, $district, $particular_district);
            $cleanArray = [];
            foreach ($worldId as $key =>  $worldId_val) {
                if (in_array($worldId_val, $cleanArray)) {
                } else {
                    array_push($cleanArray, $worldId_val);
                }
            }

            // $worldId = [];
            // if (!empty($country)) {
            //     $worldId[] = $country;
            // }
            // if (!empty($state)) {
            //     $worldId[] = $state;
            // }
            // if (!empty($district)) {
            //     $worldId[] = $district;
            // } {
            //     if ($particular_district)
            //         $worldId[] = $particular_district;
            // }
            $result = [];
            if ($cleanArray) {

                foreach ($cleanArray as $wkey => $placesVl) :

                    $worldImages =  $this->db->select('ad_promotion.*,restaurants.restaurant_category')->join('restaurants', 'restaurants.restaurant_id = ad_promotion.restaurant_id', 'inner')->where('ad_promotion.ad_promotion_id', $placesVl->ad_promotion_id)->get('ad_promotion')->result();

                    if ($worldImages) {
                        foreach ($worldImages as $wrldimgs) :
                            $ads = new stdClass();
                            $ads->id = $wrldimgs->ad_promotion_id;
                            $ads->rest_id = $wrldimgs->restaurant_id;
                            $ads->rest_type = $wrldimgs->restaurant_category;
                            $ads->ad_image = $this->getAdImages($wrldimgs->ad_promotion_id);
                            // $ads->ads = base_url() . $imgval->ad_images;
                            $result[] = $ads;
                        endforeach;
                    } else {
                        return [];
                    }
                endforeach;
                return $result;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    function getAdImages($id)
    {
        $ads =  $this->db->select('ad_images')->where('ad_promotion_id', $id)->get('ad_promotion_images')->result();
        $alads = [];
        foreach ($ads as $key =>  $ads_vl) {
            $add = new stdClass();
            $alads[] =  $add->adsimg =  base_url() . $ads_vl->ad_images;
        }
        return $alads;
    }

    public function getTrendingVideo($offset)
    {
        $offset = 5 * $offset;
        $limit =  5;

        $this->db->select('user_files.file,user_files.video_thump,user_files.user_post_location,user_files.hashtag,user_files.post_title,user_files.is_active,user_files.filesid,users.id,users.first_name,users.nickName,users.is_active')->join('users', 'users.id = user_files.userid', 'inner');
        $this->db->where('user_files.is_active', 1)->where('user_files.file_type', 1)->order_by('user_files.filesid', 'desc');
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($limit, $offset);
        }

        $videos =  $this->db->get('user_files')->result();
        $v_arr = [];
        foreach ($videos as $pkey => $v_val) {
            $videoobj = new stdClass();
            $videoobj->id = $v_val->filesid;
            $videoobj->title = $v_val->post_title;
            $videoobj->thumb = base_url() . $v_val->video_thump;
            $videoobj->video = base_url() . $v_val->file;
            $videoobj->user = ['id' => $v_val->id, 'name' => $v_val->first_name, 'isVerified' => $v_val->is_active];
            $v_arr[] = $videoobj;
        }

        if ($v_arr) {
            return  $v_arr;
        }
        return [];
        // else {
        //     return (object)['status' => false, 'statusCode' => Not_Found, 'message' => 'Sorry, no Trending videos'];
        // }
    }

    public function getfoobiesPost($offset)
    {
        $offset = 5 * $offset;
        $limit =  5;

        $this->db->select('user_files.created_date,user_files.file_type,user_files.file,user_files.video_thump,user_files.user_post_location,user_files.hashtag,user_files.post_title,user_files.is_active,user_files.filesid,users.id,users.first_name,users.nickName,users.is_active,users.profile_image')->join('users', 'users.id = user_files.userid', 'inner');
        $this->db->where('user_files.is_active', 1)->order_by('user_files.filesid', 'desc');

        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($limit, $offset);
        }

        $videos =  $this->db->get('user_files')->result();

        $userVerified = '';
        if ($videos) {
            if ($videos) {
                $files = [];
                foreach ($videos as $key => $videos_val) {
                    $userfiles = new stdClass();
                    $userfiles->id = $videos_val->filesid;
                    if ($videos_val->is_active == 1) {
                        $userVerified = 'true';
                    } else {
                        $userVerified =  'false';
                    }
                    $userfiles->user = array('id' => $videos_val->id, 'name' => $videos_val->first_name, 'profile_image' => base_url() . $videos_val->profile_image, 'isVerified' => $userVerified);
                    // $userfiles->usernickname = $videos_val->nickName;
                    $userfiles->post_location = $videos_val->user_post_location;
                    $userfiles->post_created_time = $videos_val->created_date;
                    $userfiles->post_title = $videos_val->post_title;
                    $userfiles->post_hashtags = $this->getAllHashtags($videos_val->hashtag);
                    $userfiles->post_likes = $this->getPostLikes($videos_val->filesid);
                    $userfiles->post_comments = $this->getPostCommentsCnt($videos_val->filesid);
                    $userfiles->post_reports = false;
                    $userfiles->share_url = base_url() . $videos_val->file;

                    if ($videos_val->file_type == 1) {
                        // $userfiles->postMediaType = 'true';
                        $userfiles->is_video = 'true';
                    } else {
                        // $userfiles->postMediaType =  'false';
                        $userfiles->is_video = 'false';
                    }
                    $userfiles->thumb =  base_url() . $videos_val->video_thump;
                    $userfiles->media_url = base_url() . $videos_val->file;
                    $userfiles->bannerImage = '';

                    // $userfiles->thumb =  base_url() . $videos_val->video_thump;
                    // $userfiles->postUrl = base_url() . $videos_val->file;
                    // $userfiles->postTag = $videos_val->hashtag;
                    // $userfiles->report = true;
                    // $userfiles->postType = true;
                    // $userfiles->postTitle = $videos_val->post_title;
                    // $userfiles->postLike = $this->getPostLikes($videos_val->filesid);
                    // $userfiles->postCommentCount = $this->getPostCommentsCnt($videos_val->filesid);
                    // $userfiles->postComments = $this->getPostCommens($videos_val->filesid);
                    // $userfiles->media_url = base_url() . $videos_val->file;
                    // $userfiles->postOwnerId = $videos_val->id;
                    $files[] = $userfiles;
                }
                return $files;
            }
        } else {
            return [];
        }
    }

    function getAllHashtags($id)
    {

        $htags = $this->db->select('hash_tag_name')->where_in('hash_tag_id', $id)->get('hash_tag')->result();
        $tags = [];
        foreach ($htags as $ekys => $hvals) {
            $tags[] = $hvals->hash_tag_name;
        }
        if ($tags) {
            $tag = implode(',', $tags);
            return ($tag . ',');
        }
    }
    function getPostLikes($postId)
    {
        $lks =  $this->db->where('post_id', $postId)->get('post_likes')->num_rows();
        if ($lks) {
            return $lks->likescount;
        } else {
            return 0;
        }
    }

    function getPostCommentsCnt($postId)
    {

        $cnts =   $this->db->where('post_comments_id', $postId)->get(' post_comments')->num_rows();
        if ($cnts) {
            return  $cnts;
        } else {
            return 0;
        }
    }

    function getPostCommens($postId)
    {
        $posts =  $this->db->select('post_comments.post_comments_id,post_comments.post_comments_text,post_comments.posted_user,post_comments.created_at,users.first_name,users.nickName,users.is_active')->join('users', 'users.id = post_comments.posted_user', 'inner')->where('post_comments.post_comments_id', $postId)->get('post_comments')->result();
        if ($posts) {
            return $posts;
        } else {
            return [];
        }
    }

    public function addNewAddress($userId, $pincode, $latitude, $longitude, $location_address, $address, $phone_number, $landmark)
    {
        $addresses = [
            'user_id' => $userId,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'landmark' => $landmark,
            'location_address' => $location_address,
            'address' => $address,
            'phone_number' => $phone_number,
            'pincode' => $pincode,
            'is_default' => 1
        ];
        $this->db->insert('users_address', $addresses);
        return $this->db->insert_id();
    }
    public function updateAddress($userId, $addressId, $pincode, $latitude, $longitude, $location_address, $address, $phone_number, $landmark)
    {
        $addresses = [
            'user_id' => $userId,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'landmark' => $landmark,
            'location_address' => $location_address,
            'address' => $address,
            'phone_number' => $phone_number,
            'pincode' => $pincode,
            'is_default' => 1
        ];
        $this->db->where('users_address_id', $addressId)->update('users_address', $addresses);
        return $this->db->affected_rows();
    }
    public function deleteAddress($userId, $addressId)
    {
        $this->db->where('users_address_id', $addressId)->delete('users_address');
        return $this->db->affected_rows();
    }


    public function videoListing($offset)
    {
        $offset = 5 * $offset;
        $limit =  5;

        $this->db->select('user_files.file,user_files.video_thump,user_files.user_post_location,user_files.hashtag,user_files.post_title,user_files.is_active,user_files.filesid,users.id,users.first_name,users.nickName,users.is_active')->join('users', 'users.id = user_files.userid', 'inner');
        $this->db->where('user_files.is_active', 1)->where('user_files.file_type', 1)->order_by('user_files.filesid', 'desc');
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($limit, $offset);
        }

        $videos =  $this->db->get('user_files')->result();
        $v_arr = [];
        foreach ($videos as $pkey => $v_val) {
            $videoobj = new stdClass();
            $videoobj->id = $v_val->filesid;
            $videoobj->title = $v_val->post_title;
            $videoobj->thumb = base_url() . $v_val->video_thump;
            $videoobj->video = base_url() . $v_val->file;
            $videoobj->user = ['id' => $v_val->id, 'name' => $v_val->first_name, 'isVerified' => $v_val->is_active];
            $v_arr[] = $videoobj;
        }

        if ($v_arr) {
            return  $v_arr;
        }
        return [];
        // else {
        //     return (object)['status' => false, 'statusCode' => Not_Found, 'message' => 'Sorry, no Trending videos'];
        // }
    }
}
