<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Common_model extends CI_Model
{

    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */

    public function checkUserExist($uid)
    {
        $usrId =  $this->db->select('id')->where('id', $uid)->get('users')->row();
        if ($usrId) {
            return  $usrId;
        } else {
            return 'null';
        }
    }

    public function getRestaurantNameById($id)
    {
        $rest =   $this->db->select('restaurant_name')->where('restaurant_id', $id)->get('restaurants')->row();
        return $rest->restaurant_name;
    }
}
