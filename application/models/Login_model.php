<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Login_model extends CI_Model
{

	/**
	 * function to signup new mobile number
	 * @param  [type] $mobileNumber [description]
	 * @param  [type] $otp          [description]
	 * @return [type]               [description]
	 */
	public function signup($mobileNumber, $otp, $email = null)
	{

		if ($email) {
			$id =	$this->db->select('id')->where('email', $email)->get('users')->row();
			if ($id) {
				$this->db->set('mobile', $mobileNumber)->set('otp', $otp)->where('id', $id->id)->update('users');
				$inserted_id = $id->id;
			} else {
				$inserted_id = '';
			}
		} else {
			$isnert_a = ['mobile' => $mobileNumber, 'otp' => $otp];
			$this->db->insert('users', $isnert_a);
			$inserted_id = $this->db->insert_id();
		}
		if ($inserted_id) {
			$resp = $this->sendOtp($mobileNumber, $otp);
			return (object)['status' => true, 'statusCode' => Created, 'message' => 'otp generated'];
		} else {
			return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Sorry, please try again later'];
		}
	}


	public function updateMobile($userId, $mobileNumber, $referalUserId, $myreferralCode)
	{

		$this->db
			->select('id as id')
			->from('users')
			->where('mobile', $mobileNumber)
			->where('is_active', 1)
			->where('is_deleted', 0);
		$data = $this->db->get()->row();
		if ($data) {
			return ['status' => false, 'message' => 'Mobile Number Already Registered. Try Mobile Login'];
		}


		$otp = $this->common->otpGenerator(4);
		$this->db
			->where('id', $userId)
			->set('mobile', $mobileNumber)
			->set('referred_user_id', $referalUserId)
			->set('referral_code', $myreferralCode)
			->set('otp', $otp)
			->update('users');
		$update_res = $this->db->affected_rows();
		if ($update_res) {
			$resp = $this->sendOtp($mobileNumber, $otp);
			return ['error' => 'login otp generated', 'status' => true, 'message' => 'Otp Generated'];
		} else {
			return ['status' => false, 'message' => 'Sorry some error occured! Please contact our customer support'];
		}
	}

	public function signupFacebookWithMob($facebookId, $name, $emailId, $mobileNumber, $image)
	{


		$this->db
			->select('id as id,is_active as isActive')
			->from('users')
			->where('facebook_id', $facebookId);
		$data = $this->db->get()->row();
		if ($data) {
			return (object)['status' => true, 'statusCode' => Successfull, 'data' => $data->id, 'is_active' => $data->isActive];
		} else {

			$this->db
				->select('mobile as mobile')
				->from('users')
				->where('mobile', $mobileNumber);
			$data = $this->db->get()->row();
			if ($data) {
				return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'mobile number already registered. Try using Mobile login'];
			}
			$isnert_a = ['facebook_id' => $facebookId, 'first_name' => $name, 'email' => $emailId, 'mobile' => $mobileNumber, 'profile_pic' => $image, 'is_active' => '1'];
			$this->db->insert('users', $isnert_a);
			$inserted_id = $this->db->insert_id();
			if ($inserted_id) {
				return (object)['status' => true, 'statusCode' => Created, 'data' => $inserted_id, 'is_active' => '1'];
			} else {
				return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Sorry, please try again later'];
			}
		}
	}
	public function updatePassword($userId, $c_password)
	{
		$result = $this->db->select('id')->where('id', $userId)->get('users')->row();
		if ($result) {
			$result = $this->db->set('password', md5($c_password))->where('id', $userId)->update('users');
			return (object)['status' => true, 'statusCode' => Successfull, 'message' => 'Password updated successfully'];
		} else {
			return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Sorry, user not found'];
		}
	}
	public function forgotPassword($mobileNumber)
	{
		$this->db
			->select('id as id,is_active as isActive')
			->from('users')
			->where('mobile', $mobileNumber);
		$data = $this->db->get()->row();
		if ($data) {
			$otp = $this->common->otpGenerator(4);
			$this->db
				->where('mobile', $mobileNumber)
				->set('otp', $otp)
				->update('users');

			$update_res = $this->db->affected_rows();
			$resp = $this->sendOtp($mobileNumber, $otp);

			return (object)['status' => true, 'statusCode' => Successfull, 'userId' => $data->id, 'message' => 'otp generated', 'isActive' => $data->isActive];
		} else {
			return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'User not found'];
		}
	}
	public function signupGoogle($email)
	{

		$this->db
			->select('id as id,is_active as isActive')
			->from('users')
			->where('email', $email);
		$data = $this->db->get()->row();
		if ($data) {
			return (object)['status' => true, 'statusCode' => Successfull, 'userId' => $data->id, 'message' => 'login successfull', 'isActive' => $data->isActive];
		} else {

			$isnert_a = ['email' => $email];
			$this->db->insert('users', $isnert_a);
			$inserted_id = $this->db->insert_id();
			if ($inserted_id) {
				return (object)['status' => true, 'statusCode' => Created, 'userId' => $inserted_id, 'isActive' => '0', 'message' => 'User created successfull'];
			} else {
				return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Sorry, please try again later'];
			}
		}
	}

	public function signupGoogleWithMob($googleId, $name, $emailId, $mobileNumber, $image)
	{

		$this->db
			->select('id as id,is_active as isActive')
			->from('users')
			->where('google_id', $googleId);
		$data = $this->db->get()->row();
		if ($data) {
			return (object)['status' => true, 'statusCode' => Successfull, 'data' => $data->id, 'is_active' => $data->isActive];
		} else {

			$this->db
				->select('mobile as mobile')
				->from('users')
				->where('mobile', $mobileNumber);
			$data = $this->db->get()->row();
			if ($data) {
				return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'mobile number already registered. Try using Mobile login'];
			}
			$isnert_a = ['google_id' => $googleId, 'first_name' => $name, 'email' => $emailId, 'mobile' => $mobileNumber, 'profile_pic' => $image, 'is_active' => '1'];
			$this->db->insert('users', $isnert_a);
			$inserted_id = $this->db->insert_id();
			if ($inserted_id) {
				return (object)['status' => true, 'statusCode' => Created, 'data' => $inserted_id, 'is_active' => '1'];
			} else {
				return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Sorry, please try again later'];
			}
		}
	}

	public function signupFacebook($facebookId, $name, $emailId, $image)
	{
		$this->db
			->select('id as id,is_active as isActive')
			->from('users')
			->where('facebook_id', $facebookId);
		$data = $this->db->get()->row();
		if ($data) {
			return (object)['status' => true, 'statusCode' => Successfull, 'data' => $data->id, 'is_active' => $data->isActive];
		} else {

			$isnert_a = ['facebook_id' => $facebookId, 'first_name' => $name, 'email' => $emailId, 'profile_pic' => $image];
			$this->db->insert('users', $isnert_a);
			$inserted_id = $this->db->insert_id();
			if ($inserted_id) {
				return (object)['status' => true, 'statusCode' => Created, 'data' => $inserted_id, 'is_active' => '0'];
			} else {
				return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Sorry, please try again later'];
			}
		}
	}

	/**
	 * checking entered mobile number is already registered in the application
	 * @param  [type] $mobileNumber [description]
	 * @return [type]               [description]
	 */
	public function checkMobile($mobileNumber)
	{
		$this->db
			->select('*')
			->from('users')
			->where('mobile', $mobileNumber);

		$data = $this->db->get()->row();

		if ($data) {
			// return ['error' => 'aready available', 'status' => false, 'message' => 'Sorry! you entered mobile number is already used'];
			return $this->loginWithOtp($data->id, $mobileNumber);
		} else {
			return ['status' => Bad_Request, 'message' => 'Invalid mobile number'];
		}
	}

	public function loginWithOtp($userId, $mobile)
	{
		$otp = $this->common->otpGenerator(4);

		$this->db
			->where('id', $userId)
			->set('otp', $otp)
			->update('users');

		$update_res = $this->db->affected_rows();
		if ($update_res) {
			$resp = $this->sendOtp($mobile, $otp);
			return ['error' => 'login otp generated', 'status' => true, 'message' => 'Otp Generated'];
		} else {
			return ['status' => false, 'message' => 'Sorry some error occured! Please contact our customer support'];
		}
	}

	public function loginWithPassword($mobile, $password)
	{
		$userId = $this->db->select('id')->where('mobile', $mobile)->where('password', md5($password))->get('users')->row();
		if ($userId) {
			return (object)['status' => true, 'statusCode' => Successfull, 'userId' => $userId->id, 'message' => 'login successfull'];
		} else {
			return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Invalid login credential'];
		}
	}


	public function varifyOtpGoogleSignup($otp, $mobileNumber)
	{
		date_default_timezone_set('Asia/Kolkata');
		$dateNow = date("Y-m-d H:i:s");
		$this->db
			->select('id as id')
			->from('users')
			->where('otp', $otp)
			->where('mobile', $mobileNumber);

		$data = $this->db->get()->row();

		if ($data) {
			$update_a = ['otp' => '', 'is_active' => '1', 'created' => $dateNow];
			$this->db
				->where('id', $data->id)
				->update('users', $update_a);


			$update_res = $this->db->affected_rows();
			return (object)['status' => true, 'message' => 'Otp is valid', 'user_id' => $data->id, 'is_active' => '1'];
		} else {
			return (object)['status' => false, 'message' => 'Please enter a valid otp'];
		}
	}

	/**
	 * return a message after checking the entered mobile number and otp is valid
	 * @param  [type] $otp          [description]
	 * @param  [type] $mobileNumber [description]
	 * @return [type]               [description]
	 */
	public function validateOtp($otp, $mobileNumber)
	{
		$this->db
			->select('*')
			->from('users')
			->where('otp', $otp)
			->where('mobile', $mobileNumber);

		$data = $this->db->get()->row();

		if ($data) {
			$update_a = ['otp' => ''];
			$this->db
				->where('id', $data->id)
				->update('users', $update_a);


			$update_res = $this->db->affected_rows();
			return (object)['status' => true, 'statusCode' => Successfull, 'message' => 'Otp is valid', 'user_id' => $data->id, 'mobile' => $data->mobile, 'is_active' => $data->is_active];
		} else {
			return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Please enter a valid otp'];
		}
	}

	/**
	 * Activating account and return success and failure messages
	 * @param  [type] $userId [description]
	 * @return [type]         [description]
	 */
	public function activateAccount($userId, $is_active)
	{
		date_default_timezone_set('Asia/Kolkata');
		$dateNow = date("Y-m-d H:i:s");
		//$update_a = ['is_active' => 1, 'otp' => '', 'first_name' => 'user-'.$userId];
		$update_a = ['is_active' => $is_active, 'created' => $dateNow];
		$this->db
			->where('id', $userId)
			->update('users', $update_a);

		$update_res = $this->db->affected_rows();
		if ($update_res) {
			return (object)['status' => true, 'statusCode' => Successfull, 'message' => 'Account Created Successfully', 'userId' => $userId, 'firstName' => 'user-' . $userId];
		} else {
			$rslt = 	$this->db->select('id')->where('id', $userId)->get('users')->row();
			if ($rslt->id) {
				return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Sorry account is not activated'];
			} else {
				return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'User not exist'];
			}
		}
	}

	public function checkUserExist($email, $nickName, $mobileNumber)
	{
		$eml = $this->db->select('nickName')->where('email', $email)->get('users')->row();
		$mobile = $this->db->select('nickName')->where('mobile', $mobileNumber)->get('users')->row();
		$nknme = $this->db->select('nickName')->where('nickName', $nickName)->get('users')->row();

		if (!empty($eml->nickName)) {
			return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Email Already Exist'];
		} else if (!empty($mobile->nickName)) {
			return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Mobile Already Exist'];
		} else if (!empty($nknme->nickName)) {
			return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'NickName Already Exist'];
		} else {
			return (object)['status' => true, 'statusCode' => Successfull];
		}
	}

	function resetOtp($userId, $update_a)
	{

		$this->db
			->where('id', $userId)
			->update('users', $update_a);

		$update_res = $this->db->affected_rows();
		if ($update_res) {
			return (object)['status' => true, 'message' => 'Account activated', 'userId' => $userId, 'firstName' => 'user-' . $userId];
		} else {
			return (object)['status' => false, 'message' => 'Sorry account is not activated'];
		}
	}


	public function customerFaq()
	{
		$this->db
			->select('question,answer')
			->from('customer_faq')
			->where('is_active', 1)
			->where('is_deleted', 0);
		$data = $this->db->get()->result();
		return (object)['status' => true, 'data' => $data];
	}

	public function getAddress($userId)
	{

		$this->db
			->select('id as id,customer_id as customerId, address_type as addressType, latitude as latitude,longitude as longitude,landmark as landmark,location_address as locationAddress,
			address as address,tag as tag,pincode as pincode')
			->from('customer_address')
			->where('is_active', 1)
			->where('is_deleted', 0)
			->where('customer_id', $userId)
			->order_by('id', 'desc');

		$addressData = $this->db->get()->result();
		if ($addressData) {
			return (object)['status' => true, 'data' => $addressData];
		} else {
			return (object)['status' => false, 'message' => 'No Address found'];
		}
	}

	public function deleteAddress($userId, $addressId)
	{
		$update_a = ['is_active' => 0, 'is_deleted' => 1];
		$this->db
			->where('id', $addressId)
			->where('customer_id', $userId)
			->update('customer_address', $update_a);

		$update_res = $this->db->affected_rows();
		if ($update_res) {
			return (object)['status' => true, 'message' => 'Address removed successfully'];
		} else {
			return (object)['status' => false, 'message' => 'Address removal failed'];
		}
	}


	public function updateAddress($userId, $locationAddress, $address, $landmark, $tag, $latitude, $longitude, $pincode, $addressType, $addressId)
	{
		$update_a = [
			'customer_id' => $userId, 'location_address' => $locationAddress, 'address' => $address, 'landmark' => $landmark,
			'tag' => $tag, 'latitude' => $latitude, 'longitude' => $longitude, 'pincode' => $pincode, 'address_type' => $addressType
		];
		$this->db
			->where('id', $addressId)
			->where('customer_id', $userId)
			->update('customer_address', $update_a);

		$update_res = $this->db->affected_rows();
		if ($update_res) {
			return (object)['status' => true, 'message' => 'Address updated successfully'];
		} else {
			return (object)['status' => false, 'message' => 'Address updation failed'];
		}
	}


	public function addAddress($userId, $locationAddress, $address, $landmark, $tag, $latitude, $longitude, $pincode, $addressType)
	{


		$isnert_a = [
			'customer_id' => $userId, 'location_address' => $locationAddress, 'address' => $address, 'landmark' => $landmark,
			'tag' => $tag, 'latitude' => $latitude, 'longitude' => $longitude, 'pincode' => $pincode, 'address_type' => $addressType
		];
		$this->db->insert('customer_address', $isnert_a);
		$inserted_id = $this->db->insert_id();
		if ($inserted_id) {
			return (object)['status' => true, 'data' => $inserted_id, 'message' => 'Address added successfully'];
		} else {
			return (object)['status' => false, 'message' => 'Adding Address failed'];
		}
	}
	public function deleteUserByMob($mobilenumber)
	{
		$update_res = $this->db->where('mobile', $mobilenumber)->delete('users');
		$update_res = $this->db->affected_rows();
		if ($update_res) {
			return (object)['status' => true, 'statusCode' => Successfull, 'message' => 'User deleted successfully'];
		}
	}
	public function updateProfile($update_a, $userId)
	{


		$this->db
			->where('id', $userId)
			->update('users', $update_a);
		$update_res = $this->db->affected_rows();
		if ($update_res > 0) {
			/*$timelineData = array();
			$timelineData['userId'] = $userId;
			$timelineData['type'] = "cr";
			$timelineData['detail'] = "Signup Bonus";
			$timelineData['amt'] = $update_a['bonus_coins'];
			$this->common->addToTimeline($timelineData);*/
			return (object)['status' => true, 'statusCode' => Successfull, 'message' => 'Profile updated!'];
		} else {
			return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Sorry some error occured, please contact our customer support'];
		}
	}

	/**
	 * return user data with respect to the userId entered
	 * @param  [type] $userId [description]
	 * @return [type]         [description]
	 */
	public function getProfile($userId)
	{


		$this->db
			->select('first_name as firstName, email as email, mobile as mobileNumber')
			->from('users')
			->where('is_active', 1)
			->where('is_deleted', 0)
			->where('id', $userId);

		$profileData = $this->db->get()->row();
		$orderData = array();
		$currentOrders = $this->getCurrentOrders($userId);
		$pastOrders = $this->getPastOrders($userId);

		if ($profileData) {
			return (object)['status' => true, 'profileData' => $profileData, 'currentOrders' => $currentOrders, 'pastOrders' => $pastOrders];
		} else {
			return (object)['status' => false, 'message' => 'Sorry no user data found!'];
		}
	}

	public function updatePubgName($userId, $playerDetails = array())
	{
		$pubgName = "";
		$codName = "";
		$eightballName = "";
		$eightballId = "";

		for ($i = 0; $i < sizeof($playerDetails); $i++) {
			$tag = $playerDetails[$i]->tag;
			$data = $playerDetails[$i]->playerName;
			if ($tag == "PUBG NAME") {
				$pubgName = $data;
			} elseif ($tag == "COD NAME") {
				$codName = $data;
			} elseif ($tag == "8 BALL POOL NAME") {
				$eightballName = $data;
			} elseif ($tag == "8 BALL POOL ID") {
				$eightballId = $data;
			}
		}

		if ($pubgName == null) {
			$pubgName = "";
		}
		if ($codName == null) {
			$codName = "";
		}
		if ($eightballName == null) {
			$eightballName = "";
		}
		if ($eightballId == null) {
			$eightballId = "";
		}

		$this->db
			->set('pubg_name', $pubgName)
			->set('cod_name', $codName)
			->set('8ball_name', $eightballName)
			->set('8ball_id', $eightballId)
			->where('id', $userId)
			->update('user');

		$update_res = $this->db->affected_rows();

		return (object)['status' => true, 'message' => 'Profile updated!'];
	}



	public function getReferalCode($userId)
	{
		$this->db
			->select('referral_code as referralCode')
			->from('users')
			->where('id', $userId);

		$data = $this->db->get()->row();
		$referalCode = $data->referralCode;
		$this->db
			->select('message as referralMessage,referal_sending_message as referalSendingMessage')
			->from('referal_crieria');
		$msg = 	$this->db->get()->row();
		$referralMessage = $msg->referralMessage;
		$referalSendingMessage = $msg->referalSendingMessage;

		if ($referalCode) {
			return (object)['status' => true, 'referralCode' => $referalCode, 'referralMessage' => $referralMessage, 'referalSendingMessage' => $referalSendingMessage . " " . $referalCode];
		} else {
			return (object)['status' => false, 'message' => 'Sorry no user data found!'];
		}
	}


	public function aboutUs($userId)
	{
		$this->db
			->select('about as about')
			->from('app_about');


		$data = $this->db->get()->row();



		if ($data) {
			return (object)['status' => true, 'data' => $data];
		} else {
			return (object)['status' => false, 'message' => 'Sorry no user data found!'];
		}
	}

	public function resendOtp($mobile)
	{

		$this->db
			->select('otp as otp')
			->from('users')
			->where('mobile', $mobile);
		$data = $this->db->get()->row();

		if (!$data) {
			return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Sorry no user data found!'];
		} else {

			$otp = $data->otp;

			$status =  $this->sendOtp($mobile, $otp);

			$status_s = $status->status;

			if ($status_s) {
				return (object)['status' => true, 'statusCode' => Created, 'message' => 'Otp resend successfully'];
			} else {
				return (object)['status' => false, 'statusCode' => Bad_Request, 'message' => 'Sorry no user data found!'];
			}
		}
	}


	public function deleteUserFirebaseDetails($userId, $deviceId)
	{
		$this->db
			->select('user_id as userId,device_id as deviceId,is_active as isActive,is_deleted as isDeleted')
			->from('notification_token')
			->where('user_id', $userId)
			->where('device_id', $deviceId);
		$data = $this->db->get()->row();
		if ($data) {
			$this->db
				->where('user_id', $userId)
				->where('device_id', $deviceId)
				->set('is_active', 0)
				->set('is_deleted', 1)
				->update('notification_token');
			$update_res = $this->db->affected_rows();
			if ($update_res) {
				return (object)['status' => true, 'data' => 'token removed successfully'];
			} else {
				return (object)['status' => false, 'message' => 'Sorry, please try again later'];
			}
		} else {

			return (object)['status' => false, 'message' => 'Sorry, please try again later'];
		}
	}


	public function addUserFirebaseDetails($userId, $token, $deviceId, $deviceType)
	{

		$this->db
			->select('user_id as userId,device_id as deviceId,is_active as isActive,is_deleted as isDeleted')
			->from('notification_token')
			->where('user_id', $userId)
			->where('device_id', $deviceId);
		$data = $this->db->get()->row();
		if ($data) {
			$isActive = $data->isActive;
			$isDeleted = $data->isDeleted;
			$this->db
				->where('user_id', $userId)
				->where('device_id', $deviceId)
				->set('firebase_token', $token)
				->set('is_active', 1)
				->set('is_deleted', 0)
				->update('notification_token');

			$update_res = $this->db->affected_rows();
			if ($update_res) {
				return (object)['status' => true, 'data' => 'token updated successfully'];
			} else {
				return (object)['status' => false, 'message' => 'Sorry, please try again later'];
			}
		} else {

			$isnert_a = ['user_id' => $userId, 'firebase_token' => $token, 'device_id' => $deviceId, 'device_type' => $deviceType];
			$this->db->insert('notification_token', $isnert_a);
			$inserted_id = $this->db->insert_id();
			if ($inserted_id) {
				return (object)['status' => true, 'data' => 'token added successfully'];
			} else {
				return (object)['status' => false, 'message' => 'Sorry, please try again later'];
			}
		}
	}

	public function readJson()
	{
		$json_obj = json_decode('');
		$postdata = file_get_contents("php://input");
		$json_obj = json_decode($postdata);
		return $json_obj;
	}

	public function sendNotification($title, $message)
	{

		$isnert_a = ['title' => $title, 'message' => $message];
		$this->db->insert('game_notification', $isnert_a);
		$inserted_id = $this->db->insert_id();
		if ($inserted_id) {
			define('API_ACCESS_KEY', 'AAAAQ68COMU:APA91bEzRjMhZ1GmuTWmyC6CfPeamSHXOZTzIR-w6CEdTFzgqwIBnsXyduIm5fIwGnmplaKpqiskKc3u2A7_1cBr5z7BHU-44E0aZehBKVS_SQySBqMpy1CjK2LgqMXOPtyVDW81Mrdv');
			$this->db
				->select('user_id as userId,firebase_token as token')
				->from('notification_m')
				->where('is_active', 1)
				->where('is_deleted', 0);
			$data_not = $this->db->get()->result();
			if ($data_not) {
				for ($i = 0; $i < sizeof($data_not); $i++) {
					$data = array(
						"to" => $data_not[$i]->token,
						"notification" => array("title" => $title, "body" => $message)
					);
					$data_string = json_encode($data);
					//echo "The Json Data : ".$data_string; 
					$headers = array(
						'Authorization: key=' . API_ACCESS_KEY,
						'Content-Type: application/json'
					);

					$ch = curl_init();

					curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

					$result = curl_exec($ch);
					curl_close($ch);
				}
				return (object)['status' => true, 'data' => "notification send successfully"];
			} else {
				return (object)['status' => false, 'message' => 'no data found'];
			}
		} else {
			return (object)['status' => false, 'message' => 'something went wrong!'];
		}
	}


	public function sendNotificationSpecificAudience($title, $message, $gameId)
	{



		define('API_ACCESS_KEY', 'AAAAQ68COMU:APA91bEzRjMhZ1GmuTWmyC6CfPeamSHXOZTzIR-w6CEdTFzgqwIBnsXyduIm5fIwGnmplaKpqiskKc3u2A7_1cBr5z7BHU-44E0aZehBKVS_SQySBqMpy1CjK2LgqMXOPtyVDW81Mrdv');




		$this->db
			->select('nm.user_id as userId,nm.firebase_token as token')
			->from('game_participants as gp')
			->join('notification_m as nm', 'nm.user_id = gp.gamer_id')
			->where('nm.is_active', 1)
			->where('nm.is_deleted', 0)
			->where('gp.game_id', $gameId)
			->group_by('gp.gamer_id');


		$data_not = $this->db->get()->result();

		if ($data_not) {
			for ($i = 0; $i < sizeof($data_not); $i++) {
				$data = array(
					"to" => $data_not[$i]->token,
					"notification" => array("title" => $title, "body" => $message)
				);
				$data_string = json_encode($data);
				//echo "The Json Data : ".$data_string; 
				$headers = array(
					'Authorization: key=' . API_ACCESS_KEY,
					'Content-Type: application/json'
				);

				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

				$result = curl_exec($ch);
				curl_close($ch);
			}
			return (object)['status' => true, 'data' => "notification send successfully"];
		} else {
			return (object)['status' => false, 'message' => 'no data found'];
		}
	}



	public function getAppState()
	{

		$this->db
			->select('version_code as versionCode,is_maintainance_break as isMaintainance')
			->from('app_status')
			->order_by("id", "desc")
			->limit(1);
		$data = $this->db->get()->row();
		if ($data) {
			return (object)['status' => true, 'data' => $data];
		} else {
			return (object)['status' => false, 'message' => 'something went wrong!'];
		}
	}


	public function getCustomerNotification($userId, $offset)
	{
		//id, order_id, customer_id, title, message, is_active, is_deleted
		$limit = 50;
		$this->db
			->select('cn.order_id as orderId,cn.title as title,cn.message as message,cn.date as date,om.order_status as orderStatusId')
			->from('customer_notification as cn')
			->join('order_master as om', 'om.id = cn.order_id')
			->where('cn.customer_id', $userId)
			->order_by('cn.id', 'desc')
			->limit($limit, $offset);
		$data = $this->db->get()->result();
		if ($data) {
			return (object)['status' => true, 'data' => $data];
		} else {
			return (object)['status' => false, 'message' => 'no notifications found'];
		}
	}

	public function sendOtp($mobile, $otp)
	{

		$curl = curl_init();
		// $url = "https://sms.nettyfish.com/vendorsms/pushsms.aspx?apikey=38e906fa-6b13-4595-bf81-9effd6c442d8&clientId=a33f172b-55c4-47da-871f-7a2ec88e6422&msisdn=+91$mobile&sid=FOODOY&msg=Foodoyes%20otp:$otp&fl=0&gwid=2";

		$url = "https://sms.nettyfish.com/vendorsms/pushsms.aspx?apikey=38e906fa-6b13-4595-bf81-9effd6c442d8&clientId=a33f172b-55c4-47da-871f-7a2ec88e6422&msisdn=+91$mobile&sid=FOODOY&msg=Foodoyes%20otp:$otp...&fl=0&gwid=2";

		// $url = "https://sms.nettyfish.com/vendorsms/pushsms.aspx?apikey=38e906fa-6b13-4595-bf81-9effd6c442d8&clientId=a33f172b-55c4-47da-871f-7a2ec88e6422&msisdn=+91$mobile&sid=FOODOY&msg=Foodoyes%20otp:%23%23$otp%23%23...&fl=0&gwid=2";

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "",
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded"
			),
		));

		$response = curl_exec($curl);

		$err = curl_error($curl);
		curl_close($curl);

		if ($err) {
			return (object)['status' => false, 'data' => $err];
		} else {
			return (object)['status' => true, 'data' => $response];
		}
	}

	public 	function PostRequest($url, $_data)
	{
		// convert variables array to string:
		$data = array();
		while (list($n, $v) = ($_data)) {

			$data[] = "$n=$v";
		}
		$data = implode('&', $data);
		// format --> test1=a&test2=b etc.
		// parse the given URL
		$url = parse_url($url);
		if ($url['scheme'] != 'http') {
			die('Only HTTP request are supported !');
		}
		// extract host and path:
		$host = $url;
		// $path = $url['path'];
		// open a socket connection on port 80
		$fp = fsockopen($host, 80);
		// send the request headers:
		// fputs($fp, "POST $path HTTPS/1.1\r\n");
		fputs($fp, "Host: $host\r\n");
		fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
		fputs($fp, "Content-length: " . strlen($data) . "\r\n");
		fputs($fp, "Connection: close\r\n\r\n");
		fputs($fp, $data);
		$result = '';
		while (!feof($fp)) {
			// receive the results of the request
			$result .= fgets($fp, 128);
		}
		// close the socket connection:
		fclose($fp);
		// split the result header from the content
		$result = explode("\r\n\r\n", $result, 2);
		$header = isset($result[0]) ? $result[0] : '';
		$content = isset($result[1]) ? $result[1] : '';
		// return as array:
		print_r($content);
		// return array($header, $content);
	}





	private function getCurrentOrders($userId)
	{
		//item count,storename,storeaddress
		/*
		(select count(id) from cart_detail where cart_id = om.cart_id and is_active = 1 and is_deleted = 0) as items,			
			(select sum(sub_total) from cart_detail where cart_id = om.cart_id) as grandTotal,
		*/
		$this->db
			->select('om.id as orderId,om.cart_id as cartId, om.order_amount as orderAmount,om.order_status as orderStatusId,om.created_date as createdDate,om.cart_id as cartId,
			vm.company_name as storeName,vm.address as vendorAddress,om.delivery_charge	as deliveryCharge,			
			(select status from order_status_master where status_id = om.order_status) as orderStatus')
			->from('order_master as om')
			->join('vendor_master as vm', 'vm.id = om.vendor_id')
			->where('om.customer_id', $userId)
			->where('om.is_active', 1)
			->where('om.is_deleted', 0)
			->where('om.order_status < ', 3, false) // < 3 implements pending,confirmed or shipped
			->order_by('om.id', 'desc');
		$currentOrders = $this->db->get()->result();
		for ($i = 0; $i < sizeof($currentOrders); $i++) {
			if ($currentOrders[$i]->orderStatusId == 1) {
				$this->db
					->select('count(id) as items,sum(quantity*selling_price) as grandTotal')
					->from('cart_detail as cd')
					->where('cd.cart_id', $currentOrders[$i]->cartId)
					->where('cd.is_active', 1)
					->where('cd.is_deleted', 0);
				$data = $this->db->get()->row();
				$currentOrders[$i]->items = $data->items;
				$grandTotal = $data->grandTotal + $currentOrders[$i]->deliveryCharge;
				$currentOrders[$i]->grandTotal = $grandTotal;
			} elseif ($currentOrders[$i]->orderStatusId == 2) {
				$this->db
					->select('count(id) as items,sum(deliverable_quantity*selling_price) as grandTotal')
					->from('cart_detail as cd')
					->where('cd.cart_id', $currentOrders[$i]->cartId)
					->where('cd.deliverable_quantity >', 0)
					->where('cd.is_active', 1)
					->where('cd.is_deleted', 0);
				$data = $this->db->get()->row();
				$currentOrders[$i]->items = $data->items;
				$grandTotal = $data->grandTotal + $currentOrders[$i]->deliveryCharge;
				$currentOrders[$i]->grandTotal = $grandTotal;
			}
		}

		return $currentOrders;
	}
	private function getPastOrders($userId)
	{

		$this->db
			->select('om.id as orderId,om.order_amount as orderAmount,om.order_status as orderStatusId,om.created_date as createdDate,om.cart_id as cartId,
			vm.company_name as storeName,vm.address as vendorAddress,om.delivery_charge	as deliveryCharge,			
			(select status from order_status_master where status_id = om.order_status) as orderStatus')
			->from('order_master as om')
			->join('vendor_master as vm', 'vm.id = om.vendor_id')
			->where('om.customer_id', $userId)
			->where('om.is_active', 1)
			->where('om.is_deleted', 0)
			->where('order_status > ', 2, false) // > 3 implements delivered or cancelled
			->order_by('om.id', 'desc');
		$q = $this->db->get();
		$pastOrders = $q->result();

		$count = $q->num_rows();

		if ($count > 3) {
			$this->db
				->select('om.id as orderId,om.order_amount as orderAmount,om.order_status as orderStatusId,om.created_date as createdDate,om.cart_id as cartId,
			vm.company_name as storeName,vm.address as vendorAddress,om.delivery_charge	as deliveryCharge,
			(select status from order_status_master where status_id = om.order_status) as orderStatus')
				->from('order_master as om')
				->join('vendor_master as vm', 'vm.id = om.vendor_id')
				->where('om.customer_id', $userId)
				->where('om.is_active', 1)
				->where('om.is_deleted', 0)
				->where('order_status > ', 2, false) // > 3 implements delivered or cancelled
				->order_by('om.id', 'desc')
				->limit(4);
			$pastOrders = $this->db->get()->result();
		}


		for ($i = 0; $i < sizeof($pastOrders); $i++) {
			if ($pastOrders[$i]->orderStatusId == 3) {
				$this->db
					->select('count(id) as items,sum((deliverable_quantity - return_quantity)*selling_price) as grandTotal')
					->from('cart_detail as cd')
					->where('cd.cart_id', $pastOrders[$i]->cartId)
					->where('cd.is_active', 1)
					->where('cd.deliverable_quantity >', 0)
					->where('cd.is_deleted', 0);
				$data = $this->db->get()->row();
				$pastOrders[$i]->items = $data->items;
				$grandTotal = $data->grandTotal + $pastOrders[$i]->deliveryCharge;
				$pastOrders[$i]->grandTotal = $grandTotal;
			} elseif ($pastOrders[$i]->orderStatusId == 4) {
				$pastOrders[$i]->items = 0;
				$pastOrders[$i]->grandTotal = "0.00";
			}
		}




		return $pastOrders;
	}
	public function getMaxOfSupportTicketId()
	{
		return $this->db->query("select coalesce(max(id+1),+1) as id from support_ticket")->row();
	}
	public function insertNewTicket($ticket)
	{
		$this->db->insert('support_ticket', $ticket);
		return 	$this->db->insert_id();
	}

	public function getAllTickets($userId)
	{

		$tickets = [];
		$tickets['Pending_tickets'] = $this->db->select('id,subject_id,subject,user_id,status,is_active,date_added')->from('support_ticket')->where('user_id', $userId)->where('user_type', 1)->where('status', 1)->get()->result();

		$tickets['completed_tickets'] = $this->db->select('id,subject_id,subject,user_id,status,is_active,date_added')->from('support_ticket')->where('user_id', $userId)->where('user_type', 1)->where('status', 2)->get()->result();
		return $tickets;
	}
	public function addNewChat($cht)
	{
		$this->db->insert('ticket_chat', $cht);
		return 	$this->db->insert_id();
	}
	public function getAllChats($ticketId)
	{
		return $this->db->select('id,ticket_id,user_type,message,date,')->from('ticket_chat')->where('ticket_id', $ticketId)->get()->result();
	}
	public function getMaxOfChattTicketId()
	{
		return $this->db->query("select coalesce(max(id+1),+1) as id from ticket_chat")->row();
	}





	public function addBankDetails($userId, $accountHolderName, $accountNumber, $ifsc, $payment_type)
	{

		$tokenData = $this->common->payoutTokenGenerator();

		if ($tokenData != "error") {
			$token = $tokenData;
		} else {
			return (object)['status' => false, 'message' => 'Sorry token gen failed'];
		}

		$this->db
			->select('mobile as mobileNumber,email as email,id as benificiaryId,referral_code')
			->from('users')
			->where('is_active', 1)
			->where('is_deleted', 0)
			->where('id', $userId);
		$data = $this->db->get()->row();
		$nameAtBank = $accountHolderName;

		$response = $this->common->validateToken($token);
		if ($response) {
			$user = array();
			$benId = $data->benificiaryId . '_' . $data->referral_code;
			$user["beneId"] = $benId;
			$user["name"] = $nameAtBank;
			$user["email"] = $data->email;
			$user["bankAccount"] = $accountNumber;
			$user["ifsc"] = $ifsc;
			$user["phone"] = $data->mobileNumber;
			$response = $this->common->addPayoutBenificery($token, $user);

			if ($response) {
				$json_obj = json_decode($response);
				$status = $json_obj->status;
				$message = $json_obj->message;
				if ($status == "SUCCESS") {
					$insert_a = ['customer_id' => $data->benificiaryId, 'account_holder_name' => $accountHolderName, 'account_number' => $accountNumber, 'ifsc' => $ifsc];
					$this->db->insert('customer_bank_detail', $insert_a);
					$inserted_id = $this->db->insert_id();
					if ($inserted_id) {
						$this->db->set('payment_mode', $payment_type)->set('benificiary_id', $benId)->where('id', $userId)->update('users');
						$cus_respnse = $this->db->affected_rows();
						if ($cus_respnse > 0) {
							return (object)['status' => true, 'data' => $message];
						} else {
							return (object)['status' => false, 'message' => 'Failed to update customer payment mode'];
						}
					}
				} else {

					return (object)['status' => false, 'message' => $message];
				}
			} else {
				return (object)['status' => false, 'message' => 'Adding benificiary failed'];
			}
		} else {
			return (object)['status' => false, 'message' => 'Adding benificiary failed'];
		}
	}



	public function listFollowers($userId){

		$this->db
			->select('u.id as userId,u.first_name as name,u.profile_image as profileImage')
			->from('user_follower as uf')
			->from('users as u','u.id = uf.follower_id')
			->where('uf.is_active', 1)
			->where('uf.is_deleted', 0)
			->where('uf.user_id', $userId);
		$data = $this->db->get()->result();
		if(!$data){
			return (object)['status' => false, 'message' => 'no followers found'];
		}
		return (object)['status' => false, 'data' => $data];
	}

	public function listFollowing($userId){

		$this->db
			->select('u.id as userId,u.first_name as name,u.profile_image as profileImage')
			->from('user_follower as uf')
			->from('users as u','u.id = uf.user_id')
			->where('is_active', 1)
			->where('is_deleted', 0)
			->where('uf.follower_id', $userId);
		$data = $this->db->get()->result();
		if(!$data){
			return (object)['status' => false, 'message' => 'no followers found'];
		}
		return (object)['status' => false, 'data' => $data];
	}

	public function followProfile($userId,$profileId){
		date_default_timezone_set('Asia/Kolkata');
		$dateNow = date("Y-m-d H:i:s");
		$this->db
			->select('id as id')
			->from('user_follower as uf')			
			->where('is_active', 1)
			->where('is_deleted', 0)
			->where('follower_id', $userId)
			->where('user_id', $profileId);
		$data = $this->db->get()->row();
		if($data){
			return (object)['status' => false, 'message' => 'profile already followed'];
		}
		$isnert_a = ['user_id' => $profileId, 'follower_id' => $userId,'created' => $dateNow];
		$this->db->insert('user_follower', $isnert_a);
		$inserted_id = $this->db->insert_id();
		if($inserted_id){
			return (object)['status' => true, 'message' => 'successfully followed profile'];
		}
		return (object)['status' => false, 'message' => 'something went wrong'];

	}

	public function unfollowProfile($userId,$profileId){
		date_default_timezone_set('Asia/Kolkata');
		$dateNow = date("Y-m-d H:i:s");
		$this->db
			->select('id as id')
			->from('user_follower as uf')			
			->where('is_active', 1)
			->where('is_deleted', 0)
			->where('follower_id', $userId)
			->where('user_id', $profileId);
		$data = $this->db->get()->row();
		if(!$data){
			return (object)['status' => false, 'message' => 'profile not followed'];
		}
		$update_a = ['is_active' => '1', 'is_deleted' => '0', 'updated' => $dateNow];
		$this->db
			->where('id', $data->id)
			->update('user_follower', $update_a);
		$update_res = $this->db->affected_rows();
		if($update_res){
			return (object)['status' => true, 'message' => 'successfully unfollowed profile'];
		}
		return (object)['status' => false, 'message' => 'something went wrong'];
	}

	
}
