<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Foodcat_model extends CI_Model
{

    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */

    public function newFoodcategory($catarr)
    {
        $this->db->insert('food_category', $catarr);
        return $this->db->insert_id();
    }
    public function newFoodVariant($arr)
    {
        $this->db->insert('food_variant', $arr);
        return $this->db->insert_id();
    }
    public function getAllFudCats()
    {
        return    $this->db->where('is_active', 1)->get('food_category')->result();
    }
    public function getAllFudVarnts()
    {
        return    $this->db->where('is_active', 1)->get('food_variant')->result();
    }
    public function deletevarient($id)
    {
        $this->db->set('is_active', 0)->where('food_variant_id', $id)->update('food_variant');
        return $this->db->affected_rows();
    }
    public function deletecategory($id)
    {
        $this->db->set('is_active', 0)->where('food_category_id', $id)->update('food_category');
        return $this->db->affected_rows();
    }
}
