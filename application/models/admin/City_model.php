<?php
defined('BASEPATH') or exit('No direct script access allowed');
class City_model extends CI_Model
{

    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */

    public function newCountry($country_name)
    {
        $cntry['country_name'] = $country_name;
        $this->db->insert('county', $cntry);
        $usrId = $this->db->insert_id();
        if ($usrId) {
            return  $usrId;
        } else {
            return false;
        }
    }
    public function getAllCountrys()
    {
        return  $this->db->select('*')->get('county')->result();
    }
    public function newState($countryid, $stname)
    {
        $cntry['country_id'] = $countryid;
        $cntry['state_name'] = $stname;
        $this->db->insert('state', $cntry);
        $usrId = $this->db->insert_id();
        if ($usrId) {
            return  $usrId;
        } else {
            return false;
        }
    }
    public function newDistrict($countryid, $stateId, $district)
    {
        $cntry['country_id'] = $countryid;
        $cntry['state_id'] = $stateId;
        $cntry['district_name'] = $district;
        $this->db->insert('district', $cntry);
        $usrId = $this->db->insert_id();
        if ($usrId) {
            return  $usrId;
        } else {
            return false;
        }
    }
    public function newCity($countryid, $stateId, $districtId, $city)
    {
        $cntry['country_id'] = $countryid;
        $cntry['state_id'] = $stateId;
        $cntry['district_id'] = $districtId;
        $cntry['city_name'] = $city;
        $this->db->insert('city', $cntry);
        $usrId = $this->db->insert_id();
        if ($usrId) {
            return  $usrId;
        } else {
            return false;
        }
    }
    public function getStateNameByCntryId($cntryid)
    {
        return  $this->db->where('country_id', $cntryid)->get('state')->result();
    }
    public function getCountryNameById($cityid)
    {
        return  $this->db->where('state_id', $cityid)->get('district')->result();
    }
    public function getCityNameByDistrictId($disId)
    {
        return  $this->db->where('district_id', $disId)->get('city')->result();
    }
    public function getAllContry()
    {
        return   $this->db->where('is_active', 1)->get('county')->result();
    }
    public function getAllState()
    {
        return   $this->db->select('state.*,county.country_name')->join('county', 'state.country_id = state.country_id', 'inner')->get('state')->result();
    }
    public function getAlDistrict()
    {
        return   $this->db->select('district.*,county.country_name,state.state_name')->join('state', 'state.state_id = district.state_id')->join('county', 'county.country_id = state.country_id')->get('district')->result();
    }
    public function getAlcitys()
    {
        return   $this->db->select('city.*,district.district_name,county.country_name,state.state_name')->join('district', 'district.district_id = city.district_id')->join('state', 'state.state_id = district.state_id')->join('county', 'county.country_id = state.country_id')->get('city')->result();
    }
    public function deletecntry($id)
    {
        $this->db->where('country_id', $id)->delete('county');
    }
    public function searchstatebycountry($countryId)
    {
        return   $this->db->select('state_id,country_id,state_name')->where('country_id', $countryId)->where('is_active', 1)->get('state')->result();
    }
}
