<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Recentpost_model extends CI_Model
{

    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */

    public function getAllPosts()
    {
        $wre = 'user_files.is_active between <= 1';
        return   $this->db->select('user_files.*,users.first_name')->join('users', 'users.id = user_files.userid', 'inner')->where('user_files.is_active <= ', 1)->order_by('user_files.filesid', 'desc')->get('user_files')->result();
    }
    public function aprovepost($sts)
    {
        $sta_res =  $this->db->select('is_active')->where('filesid ', $sts)->get('user_files')->row();
        if ($sta_res->is_active == 0) {
            $this->db->set('is_active', 1)->where('filesid ', $sts)->update('user_files');
            return $this->db->affected_rows();
        } else {
            $this->db->set('is_active', 0)->where('filesid ', $sts)->update('user_files');
            return $this->db->affected_rows();
        }
    }
    public function deleteposts($id)
    {
        $files =  $this->db->select('file_location as file')->where('filesid', $id)->get('user_files')->row();
        unlink($files->file);
        $this->db->set('is_active', 3)->where('filesid', $id)->update('user_files');
        return $this->db->affected_rows();
    }
}
