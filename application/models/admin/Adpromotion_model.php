<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Adpromotion_model extends CI_Model
{

    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */

    public function newFoodcategory($catarr)
    {
        $this->db->insert('food_category', $catarr);
        return $this->db->insert_id();
    }
    public function newFoodVariant($arr)
    {
        $this->db->insert('food_variant', $arr);
        return $this->db->insert_id();
    }
    public function newBannerData($bnr)
    {
        $this->db->insert('ad_promotion', $bnr);
        return $this->db->insert_id();
    }
    public function newBannerImages($brnimgs)
    {
        $this->db->insert('ad_promotion_images', $brnimgs);
        $id =  $this->db->insert_id();
        if (!$id) {
            $this->db->where('ad_promotion_id', $id)->delete('ad_promotion');
            return false;
        } else {
            return true;
        }
    }
    public function getAllPromotions()
    {
        $world = [];
        $country = $this->db->select('ad_promotion.*,county.country_name')->join('county', 'county.country_id = ad_promotion.country_id')->where('ad_promotion.is_deleted', 0)->get('ad_promotion')->result();
        if ($country) {
            foreach ($country as $keys => $vl) {
                $wrld = new stdClass();
                $wrld->ad_promotion_id = $vl->ad_promotion_id;
                $wrld->country_id = $vl->country_id;
                $wrld->country_name = $vl->country_name;
                $wrld->starting_date = $vl->starting_date;
                $wrld->ending_date = $vl->ending_date;
                $wrld->is_active = $vl->is_active;
                $wrld->state_name = $this->getSname($vl->state_id);
                $wrld->district_name = $this->getDname($vl->district_id);
                $world[] = $wrld;
            }
            return $world;
        }
    }
    function getSname($id)
    {
        if ($id != 'all') {
            $state =   $this->db->select('state_name')->where('state_id', $id)->get('state')->row();

            return  $state->state_name;
        } else {
            return 'All';
        }
    }

    function getDname($id)
    {
        if ($id != 'all') {
            $state =   $this->db->select('district_name')->where('district_id', $id)->get('district')->row();

            return  $state->district_name;
        } else {
            return 'All';
        }
    }
    public function changeresSts($sts)
    {
        $sta_res =  $this->db->select('is_active')->where('ad_promotion_id ', $sts)->get('ad_promotion')->row();
        if ($sta_res->is_active == 0) {
            $this->db->set('is_active', 1)->where('ad_promotion_id ', $sts)->update('ad_promotion');
            return $this->db->affected_rows();
        } else {
            $this->db->set('is_active', 0)->where('ad_promotion_id ', $sts)->update('ad_promotion');
            return $this->db->affected_rows();
        }
    }
    public function deletepromo($id)
    {
        $this->db->set('is_deleted', 1)->where('ad_promotion_id ', $id)->update('ad_promotion');
        $result = $this->db->affected_rows();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    public function getcountryrestaurants($id)
    {
        return   $this->db->select('restaurants.restaurant_id,restaurants.restaurant_name,partners_login.is_active')->join('partners_login', 'partners_login.partners_id = restaurants.restaurant_id', 'inner')->where('partners_login.is_active', 1)->where('restaurants.country', $id)->get('restaurants')->result();
    }
    public function getdistrictsrestaurants($cityid)
    {
        // return   $this->db->select('restaurant_id,restaurant_name')->where('state', $cityid)->get('restaurants')->result();
        return   $this->db->select('restaurants.restaurant_id,restaurants.state,restaurants.restaurant_name,partners_login.is_active')->join('partners_login', 'partners_login.partners_id = restaurants.restaurant_id', 'inner')->where('partners_login.is_active', 1)->where('restaurants.state', $cityid)->get('restaurants')->result();
    }

    public function getdisres($id)
    {
        return   $this->db->select('restaurants.restaurant_id,restaurants.state,restaurants.restaurant_name,partners_login.is_active')->join('partners_login', 'partners_login.partners_id = restaurants.restaurant_id', 'inner')->where('partners_login.is_active', 1)->where('restaurants.district', $id)->get('restaurants')->result();
    }
}
