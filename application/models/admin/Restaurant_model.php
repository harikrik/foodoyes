<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Restaurant_model extends CI_Model
{

    /**
     * function to signup new mobile number
     * @param  [type] $mobileNumber [description]
     * @param  [type] $otp          [description]
     * @return [type]               [description]
     */

    public function addNewresturant($resturant, $uname, $password, $lon, $lat)
    {
        $this->db->insert('restaurants', $resturant);
        $resid = $this->db->insert_id();
        if ($resid) {
            $login['partners_id'] =  $resid;
            $login['username'] = $uname;
            $login['password'] =  md5($password);
            $login['partner_type'] =  2;
            $login['longitude'] =  $lon;
            $login['latitude'] =  $lat;
            $this->db->insert('partners_login', $login);
            $log_id = $this->db->insert_id();
            if ($log_id) {
                return true;
            } else {
                $this->db->where('restaurant_id', $resid)->delete('restaurants');
                return false;
            }
        } else {
            return false;
        }
    }

    public function getAllResTaurant()
    {
        return  $this->db->select('restaurants.*,city.city_name,district.district_name,partners_login.is_active')->join('district', 'district.district_id = restaurants.district', 'inner')->join('city', 'city.city_id = restaurants.city', 'inner')->join('partners_login', 'partners_login.partners_id = restaurants.restaurant_id')->where('partners_login.is_active', 1)->get('restaurants')->result();
    }
    public function changeresSts($sts)
    {
        $sta_res =  $this->db->select('status')->where('restaurant_id ', $sts)->get('restaurants')->row();
        if ($sta_res->status == 0) {
            $this->db->set('status', 1)->where('restaurant_id ', $sts)->update('restaurants');
            return $this->db->affected_rows();
        } else {
            $this->db->set('status', 0)->where('restaurant_id ', $sts)->update('restaurants');
            return $this->db->affected_rows();
        }
    }
    public function getrestaurantEdit($id)
    {

        return $this->db->query("SELECT `restaurants`.*, `partners_login`.`username` FROM `restaurants` INNER JOIN `partners_login` ON `partners_login`.`partners_id` = `restaurants`.`restaurant_id` WHERE `partners_login`.`partner_type` = 2  AND `restaurants`.`restaurant_id` = $id")->row();
    }

    public function updateResturant($resturant, $uname, $password, $lon, $lat, $resmasterid)
    {

        $this->db->where('restaurant_id ', $resmasterid)->update('restaurants', $resturant);
        $resid = $this->db->affected_rows();
        if ($resid) {
            // $login['partners_id'] =  $resid;
            if ($uname) {
                $login['username'] = $uname;
            }

            if ($password) {
                $login['password'] =  md5($password);
            }


            if ($lon) {
                $login['longitude'] =  $lon;
            }
            if ($lat) {
                $login['latitude'] =  $lat;
            }



            $this->db->where('partners_id ', $resmasterid)->update('partners_login', $login);
            $log_id = $this->db->affected_rows();
            if ($log_id) {
                return true;
            } else {

                return true;
            }
        } else {
            return false;
        }
    }

    public function deleterestaurant($id)
    {
        $this->db->set('is_active', 0)->where('partner_type', 2)->where('partners_id ', $id)->update('partners_login');
        return    $resid = $this->db->affected_rows();
    }

    public function getdistrictbyid($cityid)
    {
        $this->db->select('district_id,district_name')->where('is_active', 1)->get('district')->result();
    }
}
