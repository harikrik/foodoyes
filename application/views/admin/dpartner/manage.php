<style>
	.dataTables_filter {
		width: 50%;
		float: right;
		text-align: right;
	}

	.pagination {
		float: center;
		text-align: center;


		margin-top: 10px;

	}

	.paginate_button {
		color: #FFF;
		background-color: #f15733;
		border-color: #f15733;
		font-family: 'Oswald', sans-serif;
		padding-top: 10px;
		padding-bottom: 10px;
		border-radius: 10px 10px 10px 10px;
		padding-left: 15px;
		padding-right: 15px;
		margin-right: 10px;
	}

	#example3_previous {
		color: #FFF;
		background-color: #f15733;
		border-color: #f15733;
		font-family: 'Oswald', sans-serif;
		padding-top: 10px;
		padding-bottom: 10px;
		border-radius: 10px 10px 10px 10px;
		padding-left: 5px;
		padding-right: 5px;
	}

	.paginate_button active {
		background-color: red i !important;
	}

	.active {
		margin-left: 10px;
		margin-right: 10px;
		color: #FFF;
		background-color: #f15733;
		border-color: #f15733;
		font-family: 'Oswald', sans-serif;
		padding-top: 10px;
		padding-bottom: 10px;
		border-radius: 10px 10px 10px 10px;
		padding-left: 17px;
		padding-right: 17px;
	}

	#example3_next {
		color: #FFF;
		background-color: #f15733;
		border-color: #f15733;
		font-family: 'Oswald', sans-serif;
		padding-top: 10px;
		padding-bottom: 10px;
		border-radius: 10px 10px 10px 10px;
		padding-left: 15px;
		padding-right: 15px;
	}

	#example_next a {
		background-color: red;
	}

	#example3_filter {
		margin-bottom: 10px;
	}

	.pagination>li {
		display: inline-block;
	}
</style>

<div class="quote">

	<div class="popup_main">
		<div class="obscure" style="overflow-y: scroll;">
			<div class="popup2 animationClose">

				<div class="contact_form">
					<form action="mailsend.php" method="post" id="deliveryparform">

						<div class="items_display">
							<div class="items_display2_firstline" style="margin-top: 250px;">
								<ul>
									<li><label>Your Name <span>*</span></label>
										<input name="yourname" class="_yourname" type="text" id="#" placeholder="Your name" required />
										<span id="#1" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>contact no <span>*</span></label>
										<input name="contactno" class="_contactno" type="number" id="#6" placeholder="Contact No" required />
										<span id="#7" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>Address <span>*</span></label>
										<input name="address" class="_address" type="text" id="#" placeholder="Enter Address" required />
										<span id="#1" class="spn_Error" style="display:none;"></span>
									</li>


								</ul>


								<div class="clear"></div>
							</div>


							<div class="items_display2_firstline">
								<ul>
									<li><label>Date of Birth <span>*</span></label>
										<input name="dob" class="_dob" type="text" id="#" placeholder="Date Of Birth" required />
										<span id="#1" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>Country <span>*</span></label>
										<select id="#123" name="countryId" class="items_display_firstline2_dropdown country_name">

											<?php if ($country) {
											?> <option value="" selected>Select Country</option> <?php
																									foreach ($country as $key => $cntry) { ?>
													<option value="<?= $cntry->country_id; ?>"><?= $cntry->country_name; ?></option>
												<?php }
																								} else { ?> <option value="">Country not found</option> <?php } ?>




										</select>
									</li>
									<li><label>State <span>*</span></label>
										<select name="stateid" id="#123" class="items_display_firstline2_dropdown state_name">
											<option value="">Select State</option>
										</select>
									</li>


								</ul>


								<div class="clear"></div>
							</div>

							<div class="items_display2_firstline">
								<ul>

									<li><label>District <span>*</span></label>
										<select id="#123" name="districtId" class="items_display_firstline2_dropdown district_name">
											<option value="">District State</option>

										</select>
									</li>

									<li><label>City <span>*</span></label>
										<select name="cityId" id="#123" class="items_display_firstline2_dropdown city_name">
											<option value="" selected>Select City</option>

										</select>
									</li>





								</ul>

								<div class="clear"></div>
							</div>
							<div class="items_display2_firstline">
								<ul>
									<li><label>pin code<span>*</span></label>
										<input name="pincode" class="_pincode" type="text" id="#8" placeholder="pin code" required />
										<span id="#9" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>E-Mail<span>*</span></label>
										<input name="email" class="_email" type="email" id="#8" placeholder="E-Mail" required />
										<span id="#9" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>Working Preferences <span>*</span></label>
										<select id="#123" name="working_pref" class="items_display_firstline2_dropdown _working_pref">
											<option value="" selected>Select Preferences</option>
											<option value="full_time">full time</option>
											<option value="half_time">half time </option>
											<option value="fill/half">Full/half</option>

										</select>
									</li>
									<li><label>Driving Licence No<span>*</span></label>
										<input name="licenceno" class="_licenceno" type="text" id="#8" placeholder="Driving Licence No" required />
										<span id="#9" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>Insurance Ending Date<span>*</span></label>
										<input name="insrendingdate" class="_insrendingdate" type="file" id="#8" placeholder="Insurance Ending Date" required />
										<span id="#9" class="spn_Error" style="display:none;"></span>
									</li>



								</ul>

								<div class="clear"></div>
							</div>

							<div class="items_display2_firstline">
								<ul>
									<li><label>Blood group<span>*</span></label>
										<input name="bldgrp" type="text" class="_bldgrp" id="#8" placeholder="Blood group" required />
										<span id="#9" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>Preferred Working Location<span>*</span></label>
										<input name="pref_loc" class="_pref_loc" type="text" id="#8" placeholder="Working Location" required />
										<span id="#9" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>Joining Date<span>*</span></label>
										<input name="jod" class="_jod" type="date" id="#8" placeholder="Joining Date" required />
										<span id="#9" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>Latitude<span>*</span></label>
										<input name="lat" class="_lat" type="text" id="#8" placeholder="Latitude" required />
										<span id="#9" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>Longitude<span>*</span></label>
										<input name="long" class="_long" type="text" id="#8" placeholder="Longitude" required />
										<span id="#9" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>User Name<span>*</span></label>
										<input name="uname" class="_uname" type="text" id="#8" placeholder="User Name<" required />
										<span id="#9" class="spn_Error" style="display:none;"></span>
									</li>
									<li><label>Password<span>*</span></label>
										<input name="password" class="_password" type="text" id="#8" placeholder="Password" required />
										<span id="#9" class="spn_Error" style="display:none;"></span>
									</li>



								</ul>

								<div class="clear"></div>
							</div>

							<div class="add_food_btn">
								<div class="add_food_btn_left"><a href="#"><i class="fas fa-check"></i></a></div>
								<div class="add_food_btn_right"><a href="#"><i class="fas fa-redo-alt"></i></a></div>
								<div class="clear"></div>
							</div>
						</div>





					</form>
				</div>

				<a class="closeBtn" href="#"></a>
			</div>
		</div>
		<a class="openBtn" href="#">add delivery partner</a>
	</div>

</div>

<div class="space"></div>

<div class="recent_order">
	<a href="my-order.html">
		<div class="recent_order_left">
			<h1>our delivery partners</h1>
		</div>
	</a>
	<div class="recent_order_right">
		<div class="recent_order_right_left">
			<input type="text" class="search-place" placeholder="Search">
		</div>
		<div class="recent_order_right_right"><a href="#"><img src="images/search.png" alt="" /></a></div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>




<div class="list-wrapper">
	<div class="list-item">

		<div class="our_menu_data">


			<table class="dataTable" id="example31">
				<thead>
					<tr>
						<th>SL no</th>
						<th>delivery partner</th>
						<th>district</th>
						<th>city/location</th>
						<th> contact detail</th>
						<th>working time</th>
						<!-- <th>working hours</th> -->
						<th>Blood Group</th>
						<th>actions</th>
					</tr>
				</thead>

				<tbody>
					<?php if ($allpartners) {
						$i = 1;
						foreach ($allpartners as $key => $res) { ?>
							<tr>
								<td><?= $i; ?></td>
								<td><?= $res->partner_name; ?></td>
								<td><?= $res->district_name; ?></td>
								<td><?= $res->city_name; ?></td>

								<td><?= $res->contact_no; ?></td>
								<td><?= $res->working_preference; ?></td>
								<!-- <td>8 Hours</td> -->
								<td><?= $res->blood_group; ?></td>
								<td>
									<div class="data_butn">
										<ul>
											<li>
												<div class="edit_btn"><a href="#"><i class="fas fa-pen"></i></a></div>
											</li>
											<li>
												<div class="edit_btn"><a href="#"><i class="fas fa-trash-alt"></i></a></div>
											</li>
										</ul>
										<div class="clear"></div>
									</div>
								</td>
							</tr>
					<?php }
					} ?>
				</tbody>


			</table>

		</div>

	</div>






</div>
<div class="item_edit">
	<div class="recent_order">
		<a href="my-order.html">
			<div class="recent_order_left2">
				<h1>edit delivery partner</h1>
			</div>
		</a>

		<div class="clear"></div>
	</div>
	<div class="items_display2">
		<div class="items_display2_firstline">
			<ul>
				<li>
					<div class="items_display2_firstline_inn">
						<label>name<span>*</span></label>
						<input name="txtName" type="text" id="#" placeholder="Partner Name" required />
						<span id="#1" class="spn_Error" style="display:none;"></span>
					</div>
				</li>
				<li>
					<div class="items_display2_firstline_inn">
						<label>district <span>*</span></label>
						<select id="#123" class="items_display_firstline2_dropdown">
							<option value="Select tax" selected>Select District</option>
							<option value="gst0">district</option>
							<option value="gst5">district</option>
							<option value="gst12">district</option>


						</select>
					</div>
				</li>
				<li>
					<div class="items_display2_firstline_inn">
						<label>location <span>*</span></label>
						<select id="#123" class="items_display_firstline2_dropdown">
							<option value="Select tax" selected>Select location</option>
							<option value="gst0">location</option>
							<option value="gst5">location</option>
							<option value="gst12">location</option>


						</select>
					</div>
				</li>

			</ul>


			<div class="clear"></div>
		</div>




		<div class="items_display2_firstline">
			<ul>
				<li>
					<div class="items_display2_firstline_inn">
						<label>Contact Detail <span>*</span></label>
						<input type="number" id="myFile" name="filename" placeholder="Contact Detail" required />
						<span id="#12" class="spn_Error" style="display:none;"></span>
					</div>
				</li>
				<li>
					<div class="items_display2_firstline_inn">
						<label>working time <span>*</span></label>
						<select id="#123" class="items_display_firstline2_dropdown">
							<option value="Select tax" selected>Select working time</option>
							<option value="gst0">Part time</option>
							<option value="gst5">Full time</option>
							<option value="gst12">Part/full </option>


						</select>
					</div>
				</li>
				<li>
					<div class="items_display2_firstline_inn">
						<div class="menu_edit_btn">
							<ul>
								<li>

									<input class="submit_btn" name="txtName" type="submit" id="#34" required />
									<span id="#35" class="spn_Error" style="display:none;"></span>
								</li>
								<li><input class="submit_btn" name="txtName" type="reset" id="#34" required />
									<span id="#35" class="spn_Error" style="display:none;"></span>
								</li>
							</ul>
							<div class="clear"></div>
						</div>

					</div>
				</li>

			</ul>

			<div class="clear"></div>
		</div>



	</div>

</div>


<div id="pagination-container"></div>


</div>



<!--OWL SECTION ENDS HERE-->
</div>
<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>resources/js/popup.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {

		$('#example3').dataTable({

			"bLengthChange": false, //thought this line could hide the LengthMenu
			"bInfo": false,
			"lengthMenu": [
				[5]
			]
		});

		$('#example31').dataTable({

			"bLengthChange": false, //thought this line could hide the LengthMenu
			"bInfo": false,
			"lengthMenu": [
				[5]
			]
		});


	});
</script>
<script>
	$(function() {
		$('.country_name').change(function() {
			var _url = "<?= base_url() ?>";
			var _cntryId = $(this).val();
			if (_cntryId != '') {
				$.ajax({
					method: 'post',
					url: _url + 'admin/delivery_partner/getstatebyid',
					data: {
						'countryid': _cntryId
					},
					async: false,
					dataType: 'json',
					success: function(data) {
						if (data != '') {
							var html = '';
							html += '<option value="">Select State</option>';
							$.each(data, function(key, val) {
								html += '<option value="' + val.state_id + '">' + val.state_name + '</option>';
							});
							$('.state_name').html(html);
							$('.city_name').html('<option value="">Select City</option>');
							$('.district_name').html('<option value="">Select District</option>');

						} else {
							alert('no state found');
						}
					}
				});
			}
		});
		// get district by state changes
		$('.state_name').change(function() {
			var _url = "<?= base_url() ?>";
			var _cityId = $(this).val();
			if (_cityId != '') {
				$.ajax({
					method: 'post',
					url: _url + 'admin/delivery_partner/getdistrictbyid',
					data: {
						'cityid': _cityId
					},
					async: false,
					dataType: 'json',
					success: function(data) {
						if (data != '') {
							var html = '';
							html += '<option value="">Select District</option>';
							$.each(data, function(key, val) {
								html += '<option value="' + val.district_id + '">' + val.district_name + '</option>';
							});
							$('.district_name').html(html);
							$('.city_name').html('<option value="">Select City</option>');

						} else {
							alert('no district found');
						}
					}
				});
			}
		});
		// get cities by district id
		$('.district_name').change(function() {
			var _url = "<?= base_url() ?>";
			var _cityId = $(this).val();
			if (_cityId != '') {
				$.ajax({
					method: 'post',
					url: _url + 'admin/delivery_partner/getcitiesbydid',
					data: {
						'cityid': _cityId
					},
					async: false,
					dataType: 'json',
					success: function(data) {
						if (data != '') {
							var html = '';
							html += '<option value="">Select City</option>';
							$.each(data, function(key, val) {
								html += '<option value="' + val.city_id + '">' + val.city_name + '</option>';
							});
							$('.city_name').html(html);

						} else {
							alert('no district found');
						}
					}
				});
			}
		});
	});

	// // form submit 
	$('.add_food_btn_left').click(function() {
		var formData = new FormData($('#deliveryparform')[0]);
		var _url = "<?= base_url() ?>";
		$.ajax({

			url: _url + 'admin/delivery_partner/newpartner',
			type: 'post',
			data: formData,
			dataType: 'json',
			// enctype: 'multipart/form-data',
			cache: false,
			processData: false,
			contentType: false,
			success: function(data) {
				if (data != '') {
					alert('Partner added');
					location.reload(true);
				} else {
					alert('Failed to add partner');
				}
			}
		});
	});
</script>
<script>
	$('.add_food_btn_right').click(function() {
		$('.closeBtn').click();
	});
</script>
</body>

</html>