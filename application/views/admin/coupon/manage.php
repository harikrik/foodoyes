<style>
    .dataTables_filter {
        width: 50%;
        float: right;
        text-align: right;
    }

    .pagination {
        float: center;
        text-align: center;


        margin-top: 10px;

    }

    .paginate_button {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
        margin-right: 10px;
    }

    #example3_previous {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .paginate_button active {
        background-color: red i !important;
    }

    .active {
        margin-left: 10px;
        margin-right: 10px;
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 17px;
        padding-right: 17px;
    }

    #example3_next {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
    }

    #example_next a {
        background-color: red;
    }

    #example3_filter {
        margin-bottom: 10px;
    }

    .pagination>li {
        display: inline-block;
    }
</style>



<div class="head">
    <div class="profile_name">
        <div class="profile_name_inn">

            <div class="clear"></div>
        </div>
    </div>
    <style>
        .dataTables_filter {
            width: 50%;
            float: right;
            text-align: right;
        }

        .pagination {
            float: center;
            text-align: center;


            margin-top: 10px;

        }

        .paginate_button {
            color: #FFF;
            background-color: #f15733;
            border-color: #f15733;
            font-family: 'Oswald', sans-serif;
            padding-top: 10px;
            padding-bottom: 10px;
            border-radius: 10px 10px 10px 10px;
            padding-left: 15px;
            padding-right: 15px;
            margin-right: 10px;
        }

        #example3_previous {
            color: #FFF;
            background-color: #f15733;
            border-color: #f15733;
            font-family: 'Oswald', sans-serif;
            padding-top: 10px;
            padding-bottom: 10px;
            border-radius: 10px 10px 10px 10px;
            padding-left: 5px;
            padding-right: 5px;
        }

        .paginate_button active {
            background-color: red i !important;
        }

        .active {
            margin-left: 10px;
            margin-right: 10px;
            color: #FFF;
            background-color: #f15733;
            border-color: #f15733;
            font-family: 'Oswald', sans-serif;
            padding-top: 10px;
            padding-bottom: 10px;
            border-radius: 10px 10px 10px 10px;
            padding-left: 17px;
            padding-right: 17px;
        }

        #example3_next {
            color: #FFF;
            background-color: #f15733;
            border-color: #f15733;
            font-family: 'Oswald', sans-serif;
            padding-top: 10px;
            padding-bottom: 10px;
            border-radius: 10px 10px 10px 10px;
            padding-left: 15px;
            padding-right: 15px;
        }

        #example_next a {
            background-color: red;
        }

        #example3_filter {
            margin-bottom: 10px;
        }

        .pagination>li {
            display: inline-block;
        }
    </style>



    <div class="space"></div>

    <div class="recent_order">
        <a href="my-order.html">
            <div class="recent_order_left">
                <h1>Coupon</h1>
            </div>
        </a>
        <div class="recent_order_right">
            <div class="recent_order_right_left">
                <input type="text" class="search-place" placeholder="Search">
            </div>
            <div class="recent_order_right_right"><a href="#"><img src="images/search.png" alt=""></a></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>





    <div class="item_edit">

        <div class="items_display2">





            <div class="items_display2_firstline">
                <ul>
                    <li>
                        <div class="items_display2_firstline_inn">
                            <label>1 point value<span>*</span></label>
                            <input type="text" id="onepointvalue" name="filename" value="<?= isset($settings) ? $settings->one_point_value : ''; ?>" placeholder="1 point value" required="">
                            <span id="#12" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>

                    <li>
                        <div class="items_display2_firstline_inn">
                            <div class="menu_edit_btn">
                                <ul>
                                    <li>

                                        <input class="submit_btn_onepointvalue" name="txtName" type="submit" id="#34" required="">
                                        <span id="#35" class="spn_Error" style="display:none;"></span>
                                    </li>

                                </ul>
                                <div class="clear"></div>
                            </div>

                        </div>
                    </li>

                </ul>

                <div class="clear"></div>
            </div>

            <div class="items_display2_firstline">
                <ul>
                    <li>
                        <div class="items_display2_firstline_inn">
                            <label>Minimum Point to generate coupon code<span>*</span></label>
                            <input type="text" id="minimumpnt" value="<?= isset($settings) ? $settings->minimum_points_required : ''; ?>" id="myFile" name="filename" required="">
                            <span id="#12" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>

                    <li>
                        <div class="items_display2_firstline_inn">
                            <div class="menu_edit_btn">
                                <ul>
                                    <li>

                                        <input class="submit_btnminimumpnt" name="txtName" type="submit" id="#34" required="">
                                        <span id="#35" class="spn_Error" style="display:none;"></span>
                                    </li>

                                </ul>
                                <div class="clear"></div>
                            </div>

                        </div>
                    </li>

                </ul>

                <div class="clear"></div>
            </div>
            <div class="items_display2_firstline">
                <ul>
                    <li>
                        <div class="items_display2_firstline_inn">
                            <label>Minimum order amount required<span>*</span></label>
                            <input value="<?= isset($settings) ? $settings->minimun_amount_required : ''; ?>" type="text" id="orderamount" name="filename" required="">
                            <span id="#12" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>

                    <li>
                        <div class="items_display2_firstline_inn">
                            <div class="menu_edit_btn">
                                <ul>
                                    <li>

                                        <input class="submit_btn_orderamount" name="txtName" type="submit" id="#34" required="">
                                        <span id="#35" class="spn_Error" style="display:none;"></span>
                                    </li>

                                </ul>
                                <div class="clear"></div>
                            </div>

                        </div>
                    </li>

                </ul>

                <div class="clear"></div>
            </div>


        </div>

    </div>


    <div id="pagination-container"></div>


</div>

<div class="item_edit">

    <div class="items_display2">

        <div class="items_display2_firstline">
            <ul>
                <input type="hidden" value="" class="viewsid">
                <li>
                    <div class="items_display2_firstline_inn">
                        <label>Views From <span>*</span></label>
                        <input type="text" class="from" id="myFile" name="from" placeholder="VIEWS FROM" required="">
                        <span id="#12" class="spn_Error" style="display:none;"></span>
                    </div>
                </li>
                <li>
                    <div class="items_display2_firstline_inn">
                        <label>Views To<span>*</span></label>
                        <input type="text" id="myFile" class="to" name="to" placeholder="VIEWS TO" required="">
                        <span id="#12" class="spn_Error" style="display:none;"></span>
                    </div>
                </li>
                <li>
                    <div class="items_display2_firstline_inn">
                        <label>Points<span>*</span></label>
                        <input type="text" class="Points" id="myFile" name="Points" placeholder="Points" required="">
                        <span id="#12" class="spn_Error" style="display:none;"></span>
                    </div>
                </li>
                <li>
                    <div class="items_display2_firstline_inn">
                        <div class="menu_edit_btn">
                            <ul>
                                <li>

                                    <input class="submit_btn_points" name="txtName" type="submit" id="#34" required="">
                                    <span id="#35" class="spn_Error" style="display:none;"></span>
                                </li>

                            </ul>
                            <div class="clear"></div>
                        </div>

                    </div>
                </li>
            </ul>


            <div class="clear"></div>
        </div>

        <div class="list-item">

            <div class="our_menu_data">

                <table class="dataTable" id="example31">
                    <thead>
                        <tr>
                            <th>SL no</th>

                            <th>View from </th>
                            <th> View to</th>
                            <th> Points </th>
                            <th> Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php if ($allPoints) {
                            $i = 1;
                            foreach ($allPoints as $key => $cats) { ?>
                                <tr>
                                    <td class=""><?= $i++; ?></td>
                                    <td class="v_from_<?= $cats->coupon_values_id; ?>"><?= $cats->view_from ?></td>
                                    <td class="v_to_<?= $cats->coupon_values_id; ?>"><?= $cats->view_to ?></td>
                                    <td class="v_point_<?= $cats->coupon_values_id; ?>"><?= $cats->points ?></td>


                                    <td>
                                        <div class="data_butn">
                                            <ul>

                                                <li>
                                                    <div class="edit_btn">
                                                        <a class="pntEdit" href="#" data-pntEdit="<?= $cats->coupon_values_id  ?>"> <i class="fas fa-pencil-alt"></i></a>


                                                        <a class="pntDelete" href="#" data-pntDelete="<?= $cats->coupon_values_id  ?>"><i class="fas fa-trash-alt"></i></a>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="clear"></div>
                                        </div>
                                    </td>
                                </tr>
                        <?php }
                        } ?>
                    <tbody>


                </table>

            </div>

        </div>











    </div>

</div>







</div>















<div class="clear"></div>
<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>resources/js/popup.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {

        $('#example3').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });

        $('#example31').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });


    });
</script>


<script>
    $('.submit_btn_points').click(function(e) {
        e.preventDefault();
        var _url = "<?= base_url() ?>";
        var _vId = $('.viewsid').val();
        var _froms = $('.from').val();
        var _tos = $('.to').val();
        var _pnts = $('.Points').val();

        if ((_froms != '') && (_tos != '') && (_pnts != '')) {
            $.ajax({

                url: _url + 'admin/coupon/addpointsval',
                type: 'post',
                data: {
                    '_froms': _froms,
                    '_tos': _tos,
                    '_pnts': _pnts,
                    '_vId': _vId,
                },
                dataType: 'json',
                // enctype: 'multipart/form-data',

                success: function(data) {
                    if (data != '') {
                        alert('Ponits added');
                        location.reload(true);

                    } else {
                        alert('Failed to add Points');
                    }
                }
            });
        }


    });
    $('.pntEdit').click(function(e) {
        e.preventDefault();
        var _url = "<?= base_url() ?>";
        var _id = $(this).attr('data-pntEdit');
        $.ajax({

            url: _url + 'admin/coupon/geteditdata',
            type: 'post',
            data: {
                'pId': _id

            },
            dataType: 'json',
            // enctype: 'multipart/form-data',

            success: function(data) {
                if (data != '') {
                    $('.viewsid').val(data.coupon_values_id);
                    $('.from').val(data.view_from);
                    $('.to').val(data.view_to);
                    $('.Points').val(data.points);

                } else {

                }
            }
        });
    });

    $('.pntDelete').click(function(e) {
        e.preventDefault();
        var _id = $(this).attr('data-pntDelete');
        var _vId = _id;
        var _url = "<?= base_url() ?>";
        if (confirm('Delete this point')) {
            if ((_vId != '')) {
                $.ajax({

                    url: _url + 'admin/coupon/deletepoints',
                    type: 'post',
                    data: {
                        '_vId': _vId,

                    },
                    dataType: 'json',
                    // enctype: 'multipart/form-data',

                    success: function(data) {
                        if (data != '') {
                            alert('Ponit deleted');
                            location.reload(true);

                        } else {
                            alert('Failed to delete Point');
                        }
                    }
                });
            }
        }
    });


    $('.submit_btn_onepointvalue').click(function(e) {
        e.preventDefault();
        var _url = "<?= base_url() ?>";
        var _pntval = $("#onepointvalue").val();
        if (confirm('Update this value')) {
            if ((_pntval != '')) {
                $.ajax({

                    url: _url + 'admin/coupon/updatepointvalue',
                    type: 'post',
                    data: {
                        '_vId': _pntval,

                    },
                    dataType: 'json',
                    // enctype: 'multipart/form-data',

                    success: function(data) {
                        if (data != '') {
                            alert('value updated');
                            location.reload(true);

                        } else {
                            alert('Failed to updated value');
                        }
                    }
                });
            }
        }
    });


    $('.submit_btnminimumpnt').click(function(e) {
        e.preventDefault();
        var _url = "<?= base_url() ?>";
        var _pntval = $("#minimumpnt").val();
        if (confirm('Update this value')) {
            if ((_pntval != '')) {
                $.ajax({

                    url: _url + 'admin/coupon/updateminimumpnt',
                    type: 'post',
                    data: {
                        '_vId': _pntval,

                    },
                    dataType: 'json',
                    // enctype: 'multipart/form-data',

                    success: function(data) {
                        if (data != '') {
                            alert('value updated');
                            location.reload(true);

                        } else {
                            alert('Failed to updated value');
                        }
                    }
                });
            }
        }
    });

    $('.submit_btn_orderamount').click(function(e) {
        e.preventDefault();
        var _url = "<?= base_url() ?>";
        var _pntval = $("#orderamount").val();
        if (confirm('Update this value')) {
            if ((_pntval != '')) {
                $.ajax({

                    url: _url + 'admin/coupon/updateorderamount',
                    type: 'post',
                    data: {
                        '_vId': _pntval,

                    },
                    dataType: 'json',
                    // enctype: 'multipart/form-data',

                    success: function(data) {
                        if (data != '') {
                            alert('value updated');
                            location.reload(true);

                        } else {
                            alert('Failed to updated value');
                        }
                    }
                });
            }
        }
    });
</script>