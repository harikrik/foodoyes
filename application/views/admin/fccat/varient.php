<style>
    .dataTables_filter {
        width: 50%;
        float: right;
        text-align: right;
    }

    .pagination {
        float: center;
        text-align: center;


        margin-top: 10px;

    }

    .paginate_button {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
        margin-right: 10px;
    }

    #example3_previous {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .paginate_button active {
        background-color: red i !important;
    }

    .active {
        margin-left: 10px;
        margin-right: 10px;
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 17px;
        padding-right: 17px;
    }

    #example3_next {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
    }

    #example_next a {
        background-color: red;
    }

    #example3_filter {
        margin-bottom: 10px;
    }

    .pagination>li {
        display: inline-block;
    }
</style>

<div class="quote">

    <div class="popup_main">
        <div class="obscure">
            <div class="popup animationClose">

                <div class="contact_form">
                    <form action="<?= base_url() ?>" method="post" id="catform" enctype="multipart/form-data">

                        <div class="items_display">
                            <div class="items_display2_firstline">
                                <ul>
                                    <li><label>Food Varient Name<span>*</span></label>
                                        <input name="catname" class="catname" type="text" id="#8" placeholder="Food Varient Name" required />
                                        <span id="#9" class="spn_Error" style="display:none;"></span>
                                    </li>
                                    <!-- 
                                    <li><label>Varient Icon<span>*</span></label>
                                        <input name="caticon" class="caticon" type="file" id="#8" placeholder="Food Varient Name" required />
                                        <span id="#9" class="spn_Error" style="display:none;"></span>
                                    </li> -->



                                </ul>


                                <div class="clear"></div>
                            </div>







                            <div class="add_food_btn">
                                <div class="add_food_btn_left"><a href="#"><i class="fas fa-check"></i></a></div>
                                <div class="add_food_btn_right"><a href="#"><i class="fas fa-times"></i></a></div>
                                <div class="clear"></div>
                            </div>
                        </div>





                    </form>
                </div>

                <a class="closeBtn" href="#"></a>
            </div>
        </div>
        <a class="openBtn" href="#">Add food varients</a>
    </div>
</div>

<div class="space"></div>

<div class="recent_order">
    <a href="#">
        <div class="recent_order_left2">
            <h1>Food Varient </h1>
        </div>
    </a>

    <div class="clear"></div>
</div>





<div class="list-wrapper">
    <div class="list-item">

        <div class="our_menu_data">


            <table class="dataTable" id="example31">
                <thead>
                    <tr>
                        <th>SL no</th>
                        <!-- <th>Icon</th> -->
                        <th>Varient Name</th>
                        <th> actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($fudvarnts) {
                        $i = 1;
                        foreach ($fudvarnts as $key => $cats) { ?>
                            <tr>
                                <td><?= $i++; ?></td>

                                <!-- <td><img style="height:50px" src="<?= base_url() ?><?= $cats->food_variant_icon ?>" alt="FOODOYES APPS"></td> -->
                                <td><?= $cats->food_variant_name ?></td>


                                <td>
                                    <div class="data_butn">
                                        <ul>
                                            <!-- <li>
                                                <div class="edit_btn"><a href="#"><i class="fas fa-pencil-alt"></i></a></div>
                                            </li> -->
                                            <li>
                                                <div class="edit_btn"><a href="#"><i class="fas fa-trash-alt dltvar" data-dltvar="<?= $cats->food_variant_id; ?>"></i></a></div>
                                            </li>
                                        </ul>
                                        <div class="clear"></div>
                                    </div>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                </tbody>



            </table>

        </div>

    </div>
    <div class="list-item">


    </div>





</div>
<div id="pagination-container"></div>
</div>

<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>resources/js/popup.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {

        $('#example3').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });

        $('#example31').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });


    });
</script>


<script>
    $('.add_food_btn_left').click(function() {
        $('.add_food_btn_left').hide();
        var _vname = $('.catname').val();
        if (_vname != '') {
            var _frm = $('#catform').serialize();
            var formData = new FormData($('#catform')[0]);
            var _url = "<?= base_url() ?>";
            $.ajax({

                url: _url + 'admin/food_category/newvarient',
                type: 'post',
                data: formData,
                dataType: 'json',
                // enctype: 'multipart/form-data',
                cache: false,
                processData: false,
                contentType: false,
                success: function(data) {
                    if (data != '') {
                        alert('Varient added');
                        location.reload(true);

                    } else {
                        alert('Failed to add Varient');
                    }
                }
            });
        } else {
            alert('Varient name required');
        }
    });
    // url: 'url',
    // data: data,
    // enctype: 'multipart/form-data',



    $('.dltvar').click(function(e) {
        e.preventDefault();
        var _url = "<?= base_url() ?>";
        var _id = $(this).data('dltvar');
        if (confirm('delete this varient')) {
            $.ajax({
                url: _url + 'admin/food_category/deletevarient',
                type: 'post',
                data: {
                    id: _id
                },
                dataType: 'json',
                // enctype: 'multipart/form-data',

                success: function(data) {
                    if (data != '') {
                        alert('Varient deleted');
                        location.reload(true);

                    } else {
                        alert('Failed to delete Varient');
                    }

                }


            });
        }
    });
</script>