<div class="slide_bar">
    <a href="<?= base_url() ?>home">
        <div class="slide_bar_inner_top"><img src="<?= base_url() ?>resources/images/logo.jpg" alt="" /></div>
    </a>
    <div class="slide_bar_inner">
        <div class="slide_bar_inner_in">
            <ul>

                <a href="<?= base_url() ?>home">
                    <li class="active"><i class="fas fa-th"></i>
                        <h1>Dashboard</h1>
                    </li>
                </a>

                <a href="<?= base_url() ?>addcity">
                    <li><i class="fas fa-building"></i>
                        <h1>Add city</h1>
                    </li>
                </a>

                <a href="<?= base_url() ?>recentpost">
                    <li><i class="fas fa-photo-video"></i>
                        <h1>Recent post</h1>
                    </li>
                </a>

                <a href="total-orders.html">
                    <li><i class="fas fa-shopping-bag"></i>
                        <h1>total orders</h1>
                    </li>
                </a>

                <a href="<?= base_url() ?>restuarant">
                    <li><i class="fas fa-utensils"></i>
                        <h1>restaurants</h1>
                    </li>
                </a>

                <a href="<?= base_url() ?>food_category">
                    <li><i class="fas fa-hamburger"></i>
                        <h1>Food category</h1>
                    </li>
                </a>

                <a href="<?= base_url() ?>varient">
                    <li><i class="fas fa-egg"></i>
                        <h1>Food Varients</h1>
                    </li>
                </a>

                <a href="<?= base_url() ?>adpromotion">
                    <li><i class="fas fa-clock"></i>
                        <h1>Add promotion</h1>
                    </li>
                </a>

                <a href="<?= base_url() ?>delivery_partner">
                    <li><i class="fas fa-map-marker-alt"></i>
                        <h1>delivery partners</h1>
                    </li>

                    <a href="<?= base_url() ?>coupon">
                        <li><i class="fas fa-percentage"></i>
                            <h1>Coupns</h1>
                        </li>
                    </a>

                    <a href="<?= base_url() ?>admin/hashtags">
                        <li><i class="fas fa-hashtag"></i>
                            <h1>Tags</h1>
                        </li>
                    </a>

                    <a href="user-list.html">
                        <li><i class="fas fa-user"></i>
                            <h1>Users</h1>
                        </li>
                    </a>



            </ul>
            <div class="clear"></div>
        </div>


    </div>
</div>

<div class="dashboard_right">

    <div class="head">
        <div class="profile_name">
            <div class="profile_name_inn">
                <ul>
                    <li><a href="<?= base_url() ?>admin/login/logout">
                            <div class="profile_name_user">
                                <div class="profile_name_user_left">
                                    <p>Log Out</p>
                                </div>

                        </a></li>
                    <li><a href="#">
                            <div class="profile_name_user">
                                <div class="profile_name_user_left">
                                    <p>super admin</p>
                                </div>
                                <div class="profile_name_user_right">
                                    <div class="profile_name_user_right_in_img"><img src="resources/images/logo-top.png" alt="" /></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </a></li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>