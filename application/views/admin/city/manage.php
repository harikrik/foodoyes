<style>
    .dataTables_filter {
        width: 50%;
        float: right;
        text-align: right;
    }

    .pagination {
        float: center;
        text-align: center;


        margin-top: 10px;

    }

    .paginate_button {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
        margin-right: 10px;
    }

    #example3_previous {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .paginate_button active {
        background-color: red i !important;
    }

    .active {
        margin-left: 10px;
        margin-right: 10px;
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 17px;
        padding-right: 17px;
    }

    #example3_next {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
    }

    #example_next a {
        background-color: red;
    }

    #example3_filter {
        margin-bottom: 10px;
    }

    .pagination>li {
        display: inline-block;
    }
</style>

<?php if ($this->session->flashdata('success')) { ?>
    <center>
        <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
    </center>
<?php } ?>

<?php if ($this->session->flashdata('danger')) { ?>
    <center>
        <div class="alert alert-danger"> <?= $this->session->flashdata('danger') ?> </div>
    </center>
<?php } ?>
<?php echo validation_errors(); ?>

<div class="space">
    <div class="varient_item2">

        <!-- <div class="items_display_firstline">
            <ul>

                <li>
                    <div class="items_display_firstline_inn">
                        <select id="#123" class="items_display_firstline_dropdown">
                            <option value="Select tax" selected>Country</option>
                            <option value="gst0">India</option>
                            <option value="gst18">England</option>

                        </select>
                    </div>
                </li>

                <li>
                    <div class="items_display_firstline_inn">
                        <select id="#123" class="items_display_firstline_dropdown">
                            <option value="Select tax" selected>state</option>
                            <option value="gst0">state 1</option>
                            <option value="gst18">state 2</option>

                        </select>
                    </div>
                </li>
                <li>
                    <div class="items_display_firstline_inn">
                        <select id="#123" class="items_display_firstline_dropdown">
                            <option value="Select tax" selected>district</option>
                            <option value="gst0">district 1</option>
                            <option value="gst5">district 2</option>

                        </select>
                    </div>
                </li>
                <li>
                    <div class="items_display_firstline_inn">
                        <select id="#123" class="items_display_firstline_dropdown">
                            <option value="Select tax" selected>city</option>
                            <option value="gst0">city 1</option>
                            <option value="gst5">city 2</option>
                            <option value="gst12">city 3</option>


                        </select>
                    </div>
                </li>

                <li>
                    <div class="items_display2_firstline_inn">
                        <div class="menu_edit_btn_new">
                            <ul>
                                <li>

                                    <input class="submit_btn" name="txtName" type="submit" id="#34" required />
                                    <span id="#35" class="spn_Error" style="display:none;"></span>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>

                    </div>
                </li>



            </ul>


            <div class="clear"></div>
        </div> -->



        <div class="clear"></div>
    </div>








</div>




<div class="recent_order">

    <a href="#">
        <div class="recent_order_left2">
            <h1>Add country</h1>
        </div>
    </a>

    <div class="clear"></div>


</div>
<div class="items_display2">
    <form method="post" action="<?= base_url() ?>admin/city/newcountry">
        <div class="items_display2_firstline">
            <ul>
                <li>
                    <div class="items_display2_firstline_inn">
                        <label>Enter Country Name<span>*</span></label>
                        <input name="country_name" type="text" id="#" placeholder="Country Name" required />
                        <span id="#1" class="spn_Error" style="display:none;"></span>
                    </div>
                </li>

                <li>
                    <div class="items_display2_firstline_inn">
                        <div class="menu_edit_btn">
                            <ul>
                                <li>


                                    <button class="submit_btn" type="submit" style="width: 200px  !important;">Add Country</button>
                                    <span id="#35" class="spn_Error" style="display:none;"></span>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>

                    </div>
                </li>



            </ul>


            <div class="clear"></div>
        </div>

    </form>






</div>
<div class="list-wrapper">
    <div class="list-item">

        <div class="our_menu_data">


            <table class="dataTable" id="example3">
                <thead>
                    <tr>
                        <th>SL NO</th>
                        <th>Country name</th>
                        <th>actions</th>
                    </tr>

                </thead>
                <tbody>

                    <?php if ($country) {
                        $i = 1;
                        foreach ($country as $key => $cntry) { ?>
                            <tr>
                                <td><?= $i++; ?></td>
                                <td><?= $cntry->country_name ?></td>
                                <td>
                                    <div class="data_butn">
                                        <ul>
                                            <li>
                                                <div class="edit_btn"><a href="<?= base_url() ?>admin/city/deletecntry/<?= $cntry->country_id; ?>" onclick="return confirm('Are you sure delete this country?')"><i class="fas fa-trash-alt"></i></a></div>
                                            </li>
                                        </ul>
                                        <div class="clear"></div>
                                    </div>
                                </td>
                            </tr>
                    <?php }
                    } ?>




                </tbody>

            </table>

        </div>

    </div>




</div>





<div class="recent_order">

    <a href="#">
        <div class="recent_order_left2">
            <h1>Add State</h1>
        </div>
    </a>

    <div class="clear"></div>


</div>

<div class="list-wrapper">


    <!-- new div end -->

    <!-- <div class="list-item">

        <div class="our_menu_data">
            <select>

                <?php if ($country) {
                ?> <option value="" selected>Select Country</option> <?php
                                                                        foreach ($country as $key => $cntry) { ?>
                        <option value="<?= $cntry->country_id; ?>"><?= $cntry->country_name; ?></option>
                    <?php }
                                                                    } else { ?> <option value="">Country not found</option> <?php } ?>




            </select>

            <table class="dataTable" id="example31">
                <thead>
                    <tr>
                        <th>SL NO</th>
                        <th>Country name</th>
                        <th>State name</th>
                        <th>actions</th>
                    </tr>

                </thead>
                <tbody>

                    <?php if ($alstates) {
                        $i = 1;
                        foreach ($alstates as $key => $cntry) { ?>
                            <tr>
                                <td><?= $i++; ?></td>
                                <td><?= $cntry->country_name ?></td>
                                <td><?= $cntry->state_name ?></td>
                                <td>
                                    <div class="data_butn">
                                        <ul>
                                            <li>
                                                <div class="edit_btn"><a href="<?= base_url() ?>admin/city/deletestate/<?= $cntry->state_id; ?>"><i class="fas fa-trash-alt"></i></a></div>
                                            </li>
                                        </ul>
                                        <div class="clear"></div>
                                    </div>
                                </td>
                            </tr>
                    <?php }
                    } ?>




                </tbody>

            </table>

        </div>

    </div> -->




</div>

<div class="items_display2 recent_order_of_newstates">
    <div class="items_display2_firstline">
        <form method="post" action="<?= base_url() ?>admin/city/newstate">
            <ul>
                <li>
                    <div class="items_display2_firstline_inn">
                        <label>Country <span>*</span></label>
                        <select id="#123" name="countryId" class="items_display_firstline2_dropdown">

                            <?php if ($country) {
                            ?> <option value="" selected>Select Country</option> <?php
                                                                                    foreach ($country as $key => $cntry) { ?>
                                    <option value="<?= $cntry->country_id; ?>"><?= $cntry->country_name; ?></option>
                                <?php }
                                                                                } else { ?> <option value="">Country not found</option> <?php } ?>




                        </select>
                    </div>
                </li>
                <li>
                    <div class="items_display2_firstline_inn">
                        <label>Enter State Name<span>*</span></label>
                        <input name="stname" type="text" id="#" placeholder="State Name" required />
                        <span id="#1" class="spn_Error" style="display:none;"></span>
                    </div>
                </li>

                <li>
                    <div class="items_display2_firstline_inn">
                        <div class="menu_edit_btn">
                            <ul>
                                <li>

                                    <button class="submit_btn" type="submit" style="width: 200px  !important;">Add State</button>
                                    <span id="#35" class="spn_Error" style="display:none;"></span>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>

                    </div>
                </li>



            </ul>
        </form>

        <div class="clear"></div>
    </div>








</div>
<!-- new div start -->

<div class="items_display2">
    <form method="post" action="<?= base_url() ?>admin/city/searchstatebycountry">
        <div class="items_display2_firstline">
            <ul>
                <li>
                    <div class="items_display2_firstline_inn">
                        <label>Country <span>*</span></label>
                        <select id="#123search" name="countryIdsearch" class="items_display_firstline2_dropdown" required>

                            <?php if ($country) {
                            ?> <option value="" selected>Select Country</option> <?php
                                                                                    foreach ($country as $key => $cntry) { ?>
                                    <option value="<?= $cntry->country_id; ?>"><?= $cntry->country_name; ?></option>
                                <?php }
                                                                                } else { ?> <option value="">Country not found</option> <?php } ?>




                        </select>
                    </div>
                </li>


                <li>
                    <div class="items_display2_firstline_inn">
                        <div class="menu_edit_btn">
                            <ul>
                                <li>


                                    <button style="font-size:12px" class="submit_btn" type="submit" id="#34">Search State</button>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>

                    </div>
                </li>



            </ul>


            <div class="clear"></div>
        </div>

    </form>






</div>


<div class="recent_order">

    <a href="#">
        <div class="recent_order_left2">
            <h1>Add District</h1>
        </div>
    </a>

    <div class="clear"></div>


</div>

<div class="items_display2">
    <div class="items_display2_firstline">
        <form method="post" action="<?= base_url() ?>admin/city/newdistrict">
            <ul>
                <li>
                    <div class="items_display2_firstline_inn">
                        <label>Country <span>*</span></label>
                        <select id="#123" name="countryId" class="items_display_firstline2_dropdown countryId">

                            <?php if ($country) {
                            ?> <option value="" selected>Select Country</option> <?php
                                                                                    foreach ($country as $key => $cntry) { ?>
                                    <option value="<?= $cntry->country_id; ?>"><?= $cntry->country_name; ?></option>
                                <?php }
                                                                                } else { ?> <option value="">Country not found</option> <?php } ?>




                        </select>
                    </div>
                </li>
                <li>
                    <div class="items_display2_firstline_inn">
                        <label>State <span>*</span></label>
                        <select name="stateId" id="#123" class="items_display_firstline2_dropdown new_state">
                            <option class="rmv_state" value="" selected>Select State</option>



                        </select>
                    </div>
                </li>

                <li>
                    <div class="items_display2_firstline_inn">
                        <label>Enter District Name<span>*</span></label>
                        <input name="district" type="text" id="#" placeholder="District Name" required />
                        <span id="#1" class="spn_Error" style="display:none;"></span>
                    </div>
                </li>



            </ul>


            <div class="clear"></div>

    </div>



    <div class="items_display2_firstline">
        <ul>



            <li>
                <div class="items_display2_firstline_inn">
                    <div class="menu_edit_btn">
                        <ul>
                            <li>


                                <button class="submit_btn" type="submit" style="width: 200px  !important;">Add District</button>
                                <span id="#35" class="spn_Error" style="display:none;"></span>
                            </li>
                        </ul>
                        <div class="clear"></div>
                    </div>

                </div>
            </li>



        </ul>
        </form>

        <div class="clear"></div>
    </div>




</div>




<div class="recent_order">

    <a href="#">
        <div class="recent_order_left2">
            <h1>Add Location / City</h1>
        </div>
    </a>

    <div class="clear"></div>


</div>

<div class="items_display2">
    <form method="post" action="<?= base_url() ?>admin/city/newdcity">
        <div class="items_display2_firstline">
            <ul>
                <li>
                    <div class="items_display2_firstline_inn">
                        <label>Country <span>*</span></label>
                        <select id="#123" name="countryId" class="items_display_firstline2_dropdown city_countryId">

                            <?php if ($country) {
                            ?> <option value="" selected>Select Country</option> <?php
                                                                                    foreach ($country as $key => $cntry) { ?>
                                    <option value="<?= $cntry->country_id; ?>"><?= $cntry->country_name; ?></option>
                                <?php }
                                                                                } else { ?> <option value="">Country not found</option> <?php } ?>




                        </select>
                    </div>
                </li>
                <li>
                    <div class="items_display2_firstline_inn">
                        <label>State <span>*</span></label>
                        <select name="stateId" id="#123" class="items_display_firstline2_dropdown city_new_state">
                            <option class="rmv_state" value="" selected>Select State</option>



                        </select>
                    </div>
                </li>

                <li>
                    <div class="items_display2_firstline_inn">
                        <label>District <span>*</span></label>
                        <select id="#123" name="districtId" class="items_display_firstline2_dropdown city_new_destrict">
                            <option value="" selected>Select District</option>



                        </select>
                    </div>
                </li>



            </ul>


            <div class="clear"></div>
        </div>



        <div class="items_display2_firstline">
            <ul>

                <li>
                    <div class="items_display2_firstline_inn">
                        <label>Enter Location/City Name<span>*</span></label>
                        <input name="cityname" type="text" id="#" placeholder="Location/City" required />
                        <span id="#1" class="spn_Error" style="display:none;"></span>
                    </div>
                </li>


                <li>
                    <div class="items_display2_firstline_inn">
                        <div class="menu_edit_btn">
                            <ul>
                                <li>

                                    <button class="submit_btn" type="submit" style="width: 200px  !important;">Add City</button>
                                    <span id="#35" class="spn_Error" style="display:none;"></span>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>

                    </div>
                </li>



            </ul>


            <div class="clear"></div>
        </div>


    </form>

</div>


</div>






<div class="clear"></div>




<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>


<script>
    $(document).ready(function() {

        $('#example3').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });

        $('#example31').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });


    });
</script>
<script>
    $(function() {
        $('.countryId').change(function() {
            var _url = "<?= base_url() ?>";
            var _cntryId = $(this).val();
            if (_cntryId != '') {
                $.ajax({
                    method: 'post',
                    url: _url + 'admin/city/getstatebyid',
                    data: {
                        'countryid': _cntryId
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {
                            var html = '';
                            $.each(data, function(key, val) {
                                html += '<option value="' + val.state_id + '">' + val.state_name + '</option>';
                            });
                            $('.new_state').html(html);
                        } else {
                            alert('no state found');
                        }
                    }
                });
            }
        });
        // city ajax request
        $('.city_countryId').change(function() {
            var _url = "<?= base_url() ?>";
            var _cntryId = $(this).val();
            if (_cntryId != '') {
                $.ajax({
                    method: 'post',
                    url: _url + 'admin/city/getstatebyid',
                    data: {
                        'countryid': _cntryId
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {
                            var html = '';
                            html += '<option value = "" > Select State </option>';
                            $.each(data, function(key, val) {

                                html += '<option value="' + val.state_id + '">' + val.state_name + '</option>';
                            });

                            $('.city_new_state').html(html);
                            $('.city_new_destrict').html('<option value="" >Select District</option>');
                        } else {
                            alert('no state found');
                        }
                    }
                });
            }
        });

        // get district by state changes
        $('.city_new_state').change(function() {
            var _url = "<?= base_url() ?>";
            var _cityId = $(this).val();
            if (_cityId != '') {
                $.ajax({
                    method: 'post',
                    url: _url + 'admin/city/getdistrictbyid',
                    data: {
                        'cityid': _cityId
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {
                            var html = '';

                            $.each(data, function(key, val) {
                                html += '<option value="' + val.district_id + '">' + val.district_name + '</option>';
                            });
                            $('.city_new_destrict').html(html);

                        } else {
                            alert('no district found');
                        }
                    }
                });
            }
        });
    });
</script>
<script>
    $(function() {
        var _sdivs = "<?= $scroltodiv; ?>";
        console.log(_sdivs);
        $('html, body').animate({
            scrollTop: $('.recent_order_of_newstates').offset().top
        }, 2000);
    });
</script>

<?php if ($scroltodiv) { ?>
    <script>
        $(function() {
            var _sdivs = "<?= $scroltodiv; ?>";
            console.log(_sdivs);
            $('html, body').animate({
                scrollTop: $('.recent_order_of_newstates').offset().top
            }, 2000);
        });
    </script>
<?php } ?>