<style>
    .dataTables_filter {
        width: 50%;
        float: right;
        text-align: right;
    }

    .pagination {
        float: center;
        text-align: center;


        margin-top: 10px;

    }

    .paginate_button {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
        margin-right: 10px;
    }

    #example3_previous {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .paginate_button active {
        background-color: red i !important;
    }

    .active {
        margin-left: 10px;
        margin-right: 10px;
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 17px;
        padding-right: 17px;
    }

    #example3_next {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
    }

    #example_next a {
        background-color: red;
    }

    #example3_filter {
        margin-bottom: 10px;
    }

    .pagination>li {
        display: inline-block;
    }
</style>


<div class="quote">

    <div class="popup_main">
        <div class="obscure">
            <div class="popup animationClose">

                <div class="contact_form">
                    <form action="mailsend.php" method="post" id="reasturant_form">

                        <div class="items_display">
                            <div class="items_display2_firstline">
                                <ul>
                                    <li><label>Restaurant Name <span>*</span></label>
                                        <input class="reasturant_name" name="reasturant_name" type="text" id="" placeholder="Restaurant name" required />
                                        <span id="#1" class="spn_Error" style="display:none;"></span>
                                    </li>

                                    <li><label>Country <span>*</span></label>

                                        <select id="#123" name="country_name" class="items_display_firstline2_dropdown country_name">

                                            <?php if ($country) {
                                            ?> <option value="" selected>Select Country</option> <?php
                                                                                                    foreach ($country as $key => $cntry) { ?>
                                                    <option value="<?= $cntry->country_id; ?>"><?= $cntry->country_name; ?></option>
                                                <?php }
                                                                                                } else { ?> <option value="">Country not found</option> <?php } ?>




                                        </select>

                                    </li>
                                    <li><label>State <span>*</span></label>
                                        <select id="#123" name="state_name" class="items_display_firstline2_dropdown state_name">
                                            <option value="">Select State</option>
                                        </select>
                                    </li>
                                </ul>


                                <div class="clear"></div>
                            </div>


                            <div class="items_display2_firstline">
                                <ul>
                                    <li><label>District <span>*</span></label>
                                        <select id="#123" name="district_name" class="items_display_firstline2_dropdown district_name">
                                            <option value="">District State</option>

                                        </select>
                                    </li>
                                    <li><label>City <span>*</span></label>
                                        <select id="#123" name="city_name" class="items_display_firstline2_dropdown city_name">
                                            <option value="" selected>Select City</option>

                                        </select>
                                    </li>

                                    <li><label>Restaurant category<span>*</span></label>
                                        <!-- <input name="res_cat" type="text" id="#8" placeholder="Restaurant Category" class="res_cat" required /> -->
                                        <select name="res_cat" class="res_cat">
                                            <option value="">Select</option>
                                            <option value="veg">Veg</option>
                                            <option value="non_veg">Non-veg</option>
                                            <option value="veg_non_veg">Veg&non veg</option>
                                        </select>
                                        <span id="#9" class="spn_Error" style="display:none;"></span>
                                    </li>

                                </ul>


                                <div class="clear"></div>
                            </div>

                            <div class="items_display2_firstline">
                                <ul>
                                    <li><label>contact no <span>*</span></label>
                                        <input name="contact_no" type="number" id="#6" placeholder="Contact No" class="contact_no" required />
                                        <span id="#7" class="spn_Error" style="display:none;"></span>
                                    </li>
                                    <li><label>pin code<span>*</span></label>
                                        <input name="pin_code" type="text" id="#8" placeholder="Pin Code" class="pin_code" required />
                                        <span id="#9" class="spn_Error" style="display:none;"></span>
                                    </li>

                                    <li><label>latitude<span>*</span></label>
                                        <input name="r_latitude" type="text" id="#8" placeholder="Latitude" class="r_latitude" required />
                                        <span id="#9" class="spn_Error" style="display:none;"></span>
                                    </li>
                                    <li><label>longitude<span>*</span></label>
                                        <input name="r_longitude" type="text" id="#8" placeholder="Longitude" class="r_longitude" required />
                                        <span id="#9" class="spn_Error" style="display:none;"></span>
                                    </li>
                                    <li><label>User Name<span>*</span></label>
                                        <input name="r_uname" type="text" id="#8" placeholder="login name" class="r_uname" required />
                                        <span id="#9" class="spn_Error" style="display:none;"></span>
                                    </li>
                                    <li><label>Password<span>*</span></label>
                                        <input name="r_password" type="text" id="#8" placeholder="login name" class="r_password" required />
                                        <span id="#9" class="spn_Error" style="display:none;"></span>
                                    </li>
                                    <li><label>Restaurant Image 1 <span>*</span></label>
                                        <input class="reasturant_image1" name="reasturant_image1" type="file" id="" placeholder="Restaurant name" required />
                                        <span id="#1" class="spn_Error" style="display:none;"></span>
                                    </li>
                                    <li><label>Restaurant Image 2 <span>*</span></label>
                                        <input class="reasturant_image2" name="reasturant_image2" type="file" id="" placeholder="Restaurant name" required />
                                        <span id="#1" class="spn_Error" style="display:none;"></span>
                                    </li>
                                    <li><label>Restaurant Image 3 <span>*</span></label>
                                        <input class="reasturant_image3" name="reasturant_image3" type="file" id="" placeholder="Restaurant name" required />
                                        <span id="#1" class="spn_Error" style="display:none;"></span>
                                    </li>

                                </ul>

                                <div class="clear"></div>
                            </div>


                            <div class="add_food_btn">
                                <div class="add_food_btn_left"><a href="#"><i class="fas fa-check"></i></a></div>
                                <div class="add_food_btn_right"><a href="#"><i class="fas fa-redo-alt"></i></a></div>
                                <div class="clear"></div>
                            </div>
                        </div>





                    </form>
                </div>

                <a class="closeBtn" href="#"></a>
            </div>
        </div>
        <a class="openBtn" href="#">add new restaurant</a>
    </div>
</div>



<div class="clear"></div>
</div>










<div class="space"></div>

<div class="recent_order">
    <a href="#">
        <div class="recent_order_left">
            <h1>Restaurants</h1>
        </div>
    </a>
    <div class="recent_order_right">
        <div class="recent_order_right_left">
            <input type="text" class="search-place" placeholder="Search">
        </div>
        <div class="recent_order_right_right"><a href="#"><img src="images/search.png" alt="" /></a></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>





<div class="list-wrapper">

    <div class="list-item">

        <div class="our_menu_data">


            <table class="dataTable" id="example31">
                <thead>
                    <tr>
                        <th>SL no</th>
                        <th>restaurant name</th>
                        <th>Contact Number</th>
                        <th>restaurant image</th>
                        <th>District/city</th>
                        <th>restaurant off-on</th>
                        <th> actions</th>
                    </tr>
                </thead>
                <tbody>

                    <?php if ($restuarants) {

                        $i = 1;
                        foreach ($restuarants as $key => $res) { ?>
                            <tr>
                                <td>
                                    <?= $i++; ?></td>
                                <td> <?= $res->restaurant_name; ?></td>
                                <td> <?= $res->contact_no; ?> </td>
                                <td> <img style="height: 100px;" src="<?= base_url() ?><?= $res->reasturant_image_one; ?>"> </td>
                                <td> <?= $res->district_name; ?> / <?= $res->city_name; ?> </td>
                                <td>
                                    <ul>
                                        <li>
                                            <div class="data_butn2_left">
                                                <p>off</p>
                                            </div>
                                            <label class="switch3">
                                                <input type="checkbox" class="res_manage" data-resid="<?= $res->restaurant_id; ?>" <?php if ($res->status == 1) { ?>checked <?php } else { ?> <?php } ?>>
                                                <span class="slider round"></span>
                                            </label>
                                            <div class="data_butn2_right">
                                                <p>on</p>
                                            </div>
                                        </li>
                                    </ul>
                                </td>
                                <td>
                                    <ul>
                                        <li>
                                            <div class="edit_btn"><a href="<?= base_url() ?>admin/restaurant/edit/<?= base64_encode($res->restaurant_id); ?>"><i class="fas fa-pencil-alt"></i></a></div>
                                        </li>
                                        <li>
                                            <div class="edit_btn"><a href="#" class="deletemyres" data-myresId="<?= $res->restaurant_id; ?>"><i class="fas fa-trash-alt"></i></a></div>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                    <?php }
                    }  ?>

                </tbody>
            </table>

        </div>

    </div>





</div>
<div id="pagination-container"></div>
</div>








</div>
<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>resources/js/popup.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {

        $('#example3').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });

        $('#example31').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });


    });
</script>
<script>
    // change country

    $(function() {
        $('.country_name').change(function() {
            var _url = "<?= base_url() ?>";
            var _cntryId = $(this).val();
            if (_cntryId != '') {
                $.ajax({
                    method: 'post',
                    url: _url + 'admin/restaurant/getstatebyid',
                    data: {
                        'countryid': _cntryId
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {
                            var html = '';
                            html += '<option value="">Select State</option>';
                            $.each(data, function(key, val) {
                                html += '<option value="' + val.state_id + '">' + val.state_name + '</option>';
                            });
                            $('.state_name').html(html);
                            $('.city_name').html('<option value="">Select City</option>');
                            $('.district_name').html('<option value="">Select District</option>');

                        } else {
                            alert('no state found');
                        }
                    }
                });
            }
        });


        // get district by state changes
        $('.state_name').change(function() {
            var _url = "<?= base_url() ?>";
            var _cityId = $(this).val();
            if (_cityId != '') {
                $.ajax({
                    method: 'post',
                    url: _url + 'admin/restaurant/getdistrictbyid',
                    data: {
                        'cityid': _cityId
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {
                            var html = '';
                            html += '<option value="">Select District</option>';
                            $.each(data, function(key, val) {
                                html += '<option value="' + val.district_id + '">' + val.district_name + '</option>';
                            });
                            $('.district_name').html(html);
                            $('.city_name').html('<option value="">Select City</option>');

                        } else {
                            alert('no district found');
                        }
                    }
                });
            }
        });
        // get cities by district id
        $('.district_name').change(function() {
            var _url = "<?= base_url() ?>";
            var _cityId = $(this).val();
            if (_cityId != '') {
                $.ajax({
                    method: 'post',
                    url: _url + 'admin/restaurant/getcitiesbydid',
                    data: {
                        'cityid': _cityId
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {
                            var html = '';
                            html += '<option value="">Select City</option>';
                            $.each(data, function(key, val) {
                                html += '<option value="' + val.city_id + '">' + val.city_name + '</option>';
                            });
                            $('.city_name').html(html);

                        } else {
                            alert('no city found');
                        }
                    }
                });
            }
        });
    });
    $('.add_food_btn_left').click(function() {
        var _rname = $('.reasturant_name').val();
        var _country_name = $('.country_name').val();
        var _state_name = $('.state_name').val();
        var _district_name = $('.district_name').val();
        var _city_name = $('.city_name').val();
        var _res_cat = $('.res_cat').val();
        var _contact_no = $('.contact_no').val();
        var _pin_code = $('.pin_code').val();
        var _r_longitude = $('.r_longitude').val();
        var _r_latitude = $('.r_latitude').val();
        var _r_uname = $('.r_uname').val();
        var _r_password = $('.r_password').val();

        if (_rname == '') {
            alert('RESTAURANT name required');
        }
        if (_country_name == '') {
            alert('COUNTRY  required');
        }
        if (_state_name == '') {
            alert('STATE  required');
        }
        if (_district_name == '') {
            alert('DISTRICT   required');
        }
        if (_city_name == '') {
            alert('CITY  name required');
        }
        if (_res_cat == '') {
            alert('RESTAURANT CATEGORY required');
        }
        if (_contact_no == '') {
            alert('CONTACT NO required');
        }
        if (_pin_code == '') {
            alert('PIN CODE required');
        }
        if (_r_longitude == '') {
            alert('LONGITUDE required');
        }
        if (_r_latitude == '') {
            alert('LATITUDE required');
        }
        if (_r_uname == '') {
            alert('USER NAME required');
        }
        if (_r_password == '') {
            alert('PASSWORD required');
        }

        if ((_rname) && (_r_password) && (_r_uname) && (_r_latitude) && (_r_longitude) && (_pin_code) && (_contact_no) && (_res_cat) && (_city_name) && (_district_name) && (_state_name) && (_country_name)) {
            // $("#reasturant_form")
            $('.add_food_btn_left').hide();
            var formData = new FormData($('#reasturant_form')[0]);
            $('.add_food_btn_left').attr('readonly', true);
            var _url = "<?= base_url() ?>";
            $.ajax({

                url: _url + 'admin/restaurant/addnewrestaurant',
                type: 'post',
                data: formData,
                dataType: 'json',
                // enctype: 'multipart/form-data',
                cache: false,
                processData: false,
                contentType: false,
                success: function(data) {
                    if (data != '') {
                        alert('Restuarant Created  ');
                        location.reload(true);

                    } else {
                        alert('Failed to create restaurant');
                    }
                }
            });

        }

    });


    // change restaurant status
    $('.res_manage').click(function() {
        var _sts = $(this).data('resid');
        var _url = "<?= base_url() ?>";

        $.ajax({
            method: 'post',
            url: _url + 'admin/restaurant/changereststs',
            data: {
                'status': _sts,

            },
            success: function(data) {
                alert('Restuarant Status changed Successfully');
                // $('.closeBtn').click();
                location.reload(true);
            },
            error: function(data) {
                alert('Failed to changed status');
                $('.add_food_btn_left').click('readonly', false);
            },
        });
    });

    // delete restaurant

    $('.deletemyres').click(function(e) {
        e.preventDefault();
        var _deleteId = $(this).data('myresid');
        if (confirm("Are you sure you want to delete this?")) {
            var _url = "<?= base_url() ?>";

            $.ajax({

                url: _url + 'admin/restaurant/deleterestaurant',
                type: 'post',
                data: {
                    'id': _deleteId
                },
                dataType: 'json',
                // enctype: 'multipart/form-data',

                success: function(data) {
                    if (data != '') {
                        alert('Restaurant deleted');
                        location.reload(true);

                    } else {
                        alert('Failed to delete Restaurant');
                    }
                }
            });
        }
    });
</script>