<style>
    .dataTables_filter {
        width: 50%;
        float: right;
        text-align: right;
    }

    .pagination {
        float: center;
        text-align: center;


        margin-top: 10px;

    }

    .paginate_button {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
        margin-right: 10px;
    }

    #example3_previous {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .paginate_button active {
        background-color: red i !important;
    }

    .active {
        margin-left: 10px;
        margin-right: 10px;
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 17px;
        padding-right: 17px;
    }

    #example3_next {
        color: #FFF;
        background-color: #f15733;
        border-color: #f15733;
        font-family: 'Oswald', sans-serif;
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 10px 10px 10px 10px;
        padding-left: 15px;
        padding-right: 15px;
    }

    #example_next a {
        background-color: red;
    }

    #example3_filter {
        margin-bottom: 10px;
    }

    .pagination>li {
        display: inline-block;
    }
</style>

<div class="popup_main">
    <div class="obscure">
        <div class="popup animationClose">

            <div class="contact_form">
                <form action="<?= base_url() ?>" method="post" id="catform" enctype="multipart/form-data">

                    <div class="items_display">
                        <div class="items_display2_firstline">
                            <ul>
                                <li><label>Food Category Name<span>*</span></label>
                                    <input name="catname" class="catname" type="text" id="#8" placeholder="Food Category Name" required />
                                    <span id="#9" class="spn_Error" style="display:none;"></span>
                                </li>

                                <li><label>Category Icon<span>*</span></label>
                                    <input name="caticon" class="caticon" type="file" id="#8" placeholder="Food Category Name" required />
                                    <span id="#9" class="spn_Error" style="display:none;"></span>
                                </li>



                            </ul>


                            <div class="clear"></div>
                        </div>







                        <div class="add_food_btn">
                            <div class="add_food_btn_left"><a href="#"><i class="fas fa-check"></i></a></div>
                            <div class="add_food_btn_right"><a href="#"><i class="fas fa-times"></i></a></div>
                            <div class="clear"></div>
                        </div>
                    </div>





                </form>
            </div>

            <a class="closeBtn" href="#"></a>
        </div>
    </div>
    <!-- <a class="openBtn" href="#">Add food category</a> -->
    <div class="list-wrapper">
        <div class="list-item">
            <form action="<?= base_url() ?>" method="post" id="catform" enctype="multipart/form-data">

                <div class="items_display">
                    <div class="items_display2_firstline">
                        <ul>
                            <li><label>Hash tag<span>*</span></label>
                                <input name="catname" class="catnamezz" type="text" id="#8" placeholder="Hashtag" required />
                                <span id="#9" class="spn_Error" style="display:none;"></span>
                            </li>

                            <li>
                                <div class="menu_edit_btn">
                                    <ul>
                                        <li>

                                            <button class="submit_btn addhsh" type="submit" style="width: 200px  !important;">Add Tag</button>
                                            <span id="#35" class="spn_Error" style="display:none;"></span>
                                        </li>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                            </li>



                        </ul>


                        <div class="clear"></div>
                    </div>
                    <!-- <button class="btn btn-primary addhsh">Add hashtag</button> -->
                    <!-- <a class="openBtn addhsh" style="    margin-left: 371px;
    margin-top: 39px;
" href="#">Add hashtag</a> -->
                </div>



        </div>


    </div>





    </form>
</div>
</div>

<div class="space"></div>

<div class="recent_order" style="margin-top: 100px;">
    <a href="#">
        <div class="recent_order_left">
            <h1>hashtag</h1>
        </div>
    </a>
    <div class="recent_order_right">
        <div class="recent_order_right_left">
            <input type="text" class="search-place" placeholder="Search">
        </div>
        <div class="recent_order_right_right"><a href="#"><img src="images/search.png" alt="" /></a></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>





<div class="list-wrapper">
    <div class="list-item">

        <div class="our_menu_data">

            <table class="dataTable" id="example31">
                <thead>
                    <tr>
                        <th>SL no</th>

                        <th>hashtag </th>
                        <th> actions</th>
                    </tr>
                </thead>
                <tbody>

                    <?php if ($allhashes) {
                        $i = 1;
                        foreach ($allhashes as $key => $cats) { ?>
                            <tr>
                                <td><?= $i++; ?></td>
                                <td><?= $cats->hash_tag_name ?></td>


                                <td>
                                    <div class="data_butn">
                                        <ul>

                                            <li>
                                                <div class="edit_btn"><a class="trid" href="#" data-htag="<?= $cats->hash_tag_id  ?>"><i class="fas fa-trash-alt"></i></a></div>
                                            </li>
                                        </ul>
                                        <div class="clear"></div>
                                    </div>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                <tbody>


            </table>

        </div>

    </div>






</div>
<div id="pagination-container"></div>
</div>








<div class="clear"></div>
<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>resources/js/popup.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {

        $('#example3').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });

        $('#example31').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });


    });
</script>


<script>
    $('.addhsh').click(function(e) {
        e.preventDefault();
        var _hashtag = $('.catnamezz').val();
        var _url = "<?= base_url() ?>";
        if (_hashtag != '') {
            $.ajax({

                url: _url + 'admin/hashtags/addnewhashtag',
                type: 'post',
                data: {
                    'htag': _hashtag
                },
                dataType: 'json',
                // enctype: 'multipart/form-data',

                success: function(data) {
                    if (data != '') {
                        alert('Hashtag added');
                        location.reload(true);

                    } else {
                        alert('Failed to add Hasgtag');
                    }
                }
            });
        }

    });
    $('.trid').click(function() {
        var _did = $(this).data('htag');
        if (confirm("Are you sure you want to delete this?")) {

            var _url = "<?= base_url() ?>";

            $.ajax({

                url: _url + 'admin/hashtags/deletehash',
                type: 'post',
                data: {
                    'htag': _did
                },
                dataType: 'json',
                // enctype: 'multipart/form-data',

                success: function(data) {
                    if (data != '') {
                        alert('Hashtag deleted');
                        location.reload(true);

                    } else {
                        alert('Failed to delete Hasgtag');
                    }
                }
            });


        }
    });
</script>