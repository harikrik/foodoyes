<div class="popup_main">
    <div class="obscure">
        <div class="popup2 animationClose">
            <form action="mailsend.php" method="post" id="distamcefoodform">
                <div class="food_time">
                    <h1>Input Timeslot</h1>
                    <div class="food_time_in">
                        <label>Start<span>*</span></label>
                        <input name="newpincode" class="minimum_distance_cls" type="time" id="#18" placeholder="Pincode" required />

                        <span id="#19" class="spn_Error" style="display:none;"></span>
                        <label>End<span>*</span></label>
                        <input name="newpincodeend" class="minimum_distance_cls" type="time" id="#18" placeholder="Pincode" required />

                        <span id="#19" class="spn_Error" style="display:none;"></span>
                    </div>



                    <div class="food_time_in2">
                        <input class="submit_btn popbtns" name="txtName" type="submit" id="#22" required />
                        <span id="#23" class="spn_Error" style="display:none;"></span>
                    </div>


                </div>
            </form>
            <a class="closeBtn" href="#"></a>
        </div>
    </div>
    <a class="openBtn" href="#">add new Time Slot</a>
</div>
</div>

</div>



<div class="recent_order">
    <div class="recent_order_left2">
        <h1>Available Time slots </h1>
    </div>

    <div class="clear"></div>
</div>



<div class="list-wrapper">

    <div class="list-item">
        <div class="list_order_inner">
            <ul>
                <?php if ($list_distances) {
                    $i = 1;
                    foreach ($list_distances as $ks => $lstvl) { ?>
                        <li>
                            <form action="mailsend.php" method="post" class="distamcefoodform_<?= $lstvl->time_slots_id; ?>">
                                <input type="hidden" value="<?= $lstvl->time_slots_id; ?>" name="mastersid">
                                <div class="food_time">
                                    <h1>Time slot <?= $i++; ?></h1><a href="" class="deletepin" data-delid="<?= $lstvl->time_slots_id; ?>" style="color: red;">Delete</a>
                                    <div class="food_time_in">
                                        <label>Start Time<span></span></label>
                                        <input value="<?= isset($lstvl->time_slots_name) ? $lstvl->time_slots_name : '' ?>" name="minimum_distance" class="minimum_distance_cls_<?= $lstvl->time_slots_id; ?>" type="time" id="#18" placeholder="Minimum Distance (Km)" required />

                                        <span id="#19" class="spn_Error" style="display:none;"></span>
                                        <label>End Time<span></span></label>
                                        <input value="<?= isset($lstvl->time_slots_name_end) ? $lstvl->time_slots_name_end : '' ?>" name="minimum_distance" class="minimum_distance_cls_end<?= $lstvl->time_slots_id; ?>" type="time" id="#18" placeholder="Minimum Distance (Km)" required />

                                        <span id="#19" class="spn_Error" style="display:none;"></span>
                                    </div>





                                    <div class="food_time_in2">
                                        <input class="submit_btn popbtns_update" data-upid="<?= $lstvl->time_slots_id; ?>" name="txtName" type="submit" id="#22" required />
                                        <span id="#23" class="spn_Error" style="display:none;"></span>
                                    </div>


                                </div>
                            </form>
                        </li>

                <?php }
                } ?>


            </ul>
        </div>
    </div>

</div>
<div id="pagination-container"></div>
</div>



<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>res_resources/js/popup.js" type="text/javascript"></script>


<script>
    $('.popbtns').click(function(e) {
        e.preventDefault();
        var formData = new FormData($('#distamcefoodform')[0]);
        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/newtimeslot',
            type: 'post',
            data: formData,
            dataType: 'json',
            // enctype: 'multipart/form-data',
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data != '') {
                    alert('time slot added');
                    location.reload(true);

                } else {
                    alert('Failed to add time slot');
                }
            }
        });
    });
</script>

<script>
    $('.popbtns_update').click(function(e) {
        e.preventDefault();
        var _mianId = $(this).data('upid');
        var _min_dis = $('.minimum_distance_cls_' + _mianId).val();
        var _min_dis_end = $('.minimum_distance_cls_end' + _mianId).val();


        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/timeslotsupdate',
            type: 'post',
            data: {
                _mianId: _mianId,
                pincode: _min_dis,
                endtime: _min_dis_end,

            },
            dataType: 'json',
            // enctype: 'multipart/form-data',

            success: function(data) {
                if (data != '') {
                    alert('Time slot updated');
                    location.reload(true);

                } else {
                    alert('Failed to update');
                }
            }
        });
    });

    $('.deletepin').click(function(e) {
        e.preventDefault();
        var _id = $(this).data('delid');

        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/deletetimeslot',
            type: 'post',
            data: {

                pincode: _id,

            },
            dataType: 'json',
            // enctype: 'multipart/form-data',

            success: function(data) {
                if (data != '') {
                    alert('time slot deleted');
                    location.reload(true);

                } else {
                    alert('Failed to delete ');
                }
            }
        });
    });
</script>