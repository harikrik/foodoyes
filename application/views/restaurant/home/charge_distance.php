	<div class="popup_main">
		<div class="obscure">
			<div class="popup2 animationClose">
				<form action="mailsend.php" method="post" id="distamcefoodform">
					<div class="food_time">
						<h1>delivery details</h1>
						<div class="food_time_in">
							<label>minimum distance<span>*</span></label>
							<input name="minimum_distance" class="minimum_distance_cls" type="location" id="#18" placeholder="Minimum Distance (Km)" required />

							<span id="#19" class="spn_Error" style="display:none;"></span>
						</div>

						<div class="food_time_in">
							<label>maximum distance <span>*</span></label>
							<input name="maximum_distance" class="maximum_distance_cls" type="location" id="#20" placeholder="Maximum Distance (Km)" required />
							<span id="#21" class="spn_Error" style="display:none;"></span>
						</div>

						<div class="food_time_in">
							<label>delivery charge <span>*</span></label>
							<input name="delivery_charge" class="delivery_charge_cls" type="map" id="#27" placeholder="Enter Delivery Charge " required />
							<span id="#26" class="spn_Error" style="display:none;"></span>
						</div>

						<div class="food_time_in2">
							<input class="submit_btn popbtns" name="txtName" type="submit" id="#22" required />
							<span id="#23" class="spn_Error" style="display:none;"></span>
						</div>


					</div>
				</form>
				<a class="closeBtn" href="#"></a>
			</div>
		</div>
		<a class="openBtn" href="#">add delivery details</a>
	</div>
	</div>

	</div>



	<div class="recent_order">
		<div class="recent_order_left2">
			<h1>delivery charge & Distance</h1>
		</div>

		<div class="clear"></div>
	</div>



	<div class="list-wrapper">

		<div class="list-item">
			<div class="list_order_inner">
				<ul>
					<?php if ($list_distances) {
						foreach ($list_distances as $ks => $lstvl) { ?>
							<li>
								<form action="mailsend.php" method="post" class="distamcefoodform_<?= $lstvl->id; ?>">
									<input type="hidden" value="<?= $lstvl->id; ?>" name="mastersid">
									<div class="food_time">
										<h1>delivery details</h1><a href="" class="deletepin" data-delid="<?= $lstvl->id; ?>" style="color: red;">Delete</a>
										<div class="food_time_in">
											<label>minimum distance<span>*</span></label>
											<input value="<?= isset($lstvl->minimum_distance) ? $lstvl->minimum_distance : '' ?>" name="minimum_distance" class="minimum_distance_cls_<?= $lstvl->id; ?>" type="location" id="#18" placeholder="Minimum Distance (Km)" required />

											<span id="#19" class="spn_Error" style="display:none;"></span>
										</div>

										<div class="food_time_in">
											<label>maximum distance <span>*</span></label>
											<input value="<?= isset($lstvl->maximum_distance) ? $lstvl->maximum_distance : '' ?>" name="maximum_distance" class="maximum_distance_cls_<?= $lstvl->id; ?>" type="location" id="#20" placeholder="Maximum Distance (Km)" required />
											<span id="#21" class="spn_Error" style="display:none;"></span>
										</div>

										<div class="food_time_in">
											<label>delivery charge <span>*</span></label>
											<input value="<?= isset($lstvl->delivery_charge) ? $lstvl->delivery_charge : '' ?>" name="delivery_charge" class="delivery_charge_cls_<?= $lstvl->id; ?>" type="map" id="#27" placeholder="Enter Delivery Charge " required />
											<span id="#26" class="spn_Error" style="display:none;"></span>
										</div>

										<div class="food_time_in2">
											<input class="submit_btn popbtns_update" data-upid="<?= $lstvl->id; ?>" name="txtName" type="submit" id="#22" required />
											<span id="#23" class="spn_Error" style="display:none;"></span>
										</div>


									</div>
								</form>
							</li>

					<?php }
					} ?>


				</ul>
			</div>
		</div>

	</div>
	<div id="pagination-container"></div>
	</div>



	<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
	<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
	<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

	<script src="<?= base_url() ?>res_resources/js/popup.js" type="text/javascript"></script>


	<script>
		$('.popbtns').click(function(e) {
			e.preventDefault();
			var formData = new FormData($('#distamcefoodform')[0]);
			var _url = "<?= base_url() ?>";
			$.ajax({

				url: _url + 'restaurant/home/newdistance',
				type: 'post',
				data: formData,
				dataType: 'json',
				// enctype: 'multipart/form-data',
				cache: false,
				processData: false,
				contentType: false,
				success: function(data) {
					if (data != '') {
						alert('distance added');
						location.reload(true);

					} else {
						alert('Failed to add distance');
					}
				}
			});
		});
	</script>

	<script>
		$('.popbtns_update').click(function(e) {
			e.preventDefault();
			var _mianId = $(this).data('upid');
			var _min_dis = $('.minimum_distance_cls_' + _mianId).val();
			var _max_dis = $('.maximum_distance_cls_' + _mianId).val();
			var _del_charge = $('.delivery_charge_cls_' + _mianId).val();

			var _url = "<?= base_url() ?>";
			$.ajax({

				url: _url + 'restaurant/home/newdistanceupdate',
				type: 'post',
				data: {
					_mianId: _mianId,
					_min_dis: _min_dis,
					_max_dis: _max_dis,
					_del_charge: _del_charge,
				},
				dataType: 'json',
				// enctype: 'multipart/form-data',

				success: function(data) {
					if (data != '') {
						alert('distance updated');
						location.reload(true);

					} else {
						alert('Failed to update distance');
					}
				}
			});
		});


		$('.deletepin').click(function(e) {
			e.preventDefault();
			var _id = $(this).data('delid');

			var _url = "<?= base_url() ?>";
			$.ajax({

				url: _url + 'restaurant/home/deletedistance',
				type: 'post',
				data: {

					pincode: _id,

				},
				dataType: 'json',
				// enctype: 'multipart/form-data',

				success: function(data) {
					if (data != '') {
						alert('distance deleted');
						location.reload(true);

					} else {
						alert('Failed to delete distance');
					}
				}
			});
		});
	</script>