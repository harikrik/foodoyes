<div class="item_edit">
    <div class="recent_order">
        <div class="recent_order_left2">
            <h1>settings</h1>
        </div>
        <div class="recent_order_right2">
            <div class="recent_order_right2_left"><label>restaurant status</label></div>
            <div class="recent_order_right2_right">
                <label class="switch2">
                    <input class="changeresstatus" type="checkbox" <?php if ($ressettings_online->is_online == 1) {
                                                                        echo 'checked';
                                                                    } ?>>
                    <span class="slider round"></span>
                </label>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="items_display2">
        <form action="mailsend.php" method="post" id="settingsfoodform">
            <input type="hidden" name="smainid" value="<?= isset($ressettings->settings_id) ? $ressettings->settings_id : ''; ?>">
            <div class="items_display_firstline2">
                <ul>
                    <li>
                        <div class="items_display_firstline_inn2">
                            <label>restaurant opening time <span>*</span></label>
                            <input value="<?= isset($ressettings->opening_time) ? $ressettings->opening_time : ''; ?>" name="opentime" class="opentime_cls" type="time" id="#" placeholder="Product Variant Name " required />
                            <span id="#1" class="spn_Error" style="display:none;"></span>
                        </div>
                        <div class="items_display_firstline_inn2">
                            <label>Maximum Delivery Km<span>*</span></label>
                            <input value="<?= isset($ressettings->maximum_delivery_distance) ? $ressettings->maximum_delivery_distance : ''; ?>" name="max_del" class="max_del_cls" type="text" id="#" placeholder="Product Variant Name " required />
                            <span id="#1" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>

                    <li>
                        <div class="items_display_firstline_inn2">
                            <label>restaurant closing time <span>*</span></label>
                            <input value="<?= isset($ressettings->closing_time) ? $ressettings->closing_time : ''; ?>" name="closingtiem" class="closingtiem_cls" type="time" id="#" placeholder="Product Variant Name " required />
                            <span id="#1" class="spn_Error" style="display:none;"></span>
                        </div>
                        <div class="items_display_firstline_inn2">
                            <label>Currency symbol<span>*</span></label>
                            <input value="<?= isset($ressettings->currency_symbol) ? $ressettings->currency_symbol : ''; ?>" name="currencysymbol" class="opentime_cls" type="text" id="#" placeholder="Currency symbol" required />
                            <span id="#1" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>
                    <li>
                        <div class="items_display_firstline_inn2">
                            <label>minimum order amount <span>*</span></label>
                            <input value="<?= isset($ressettings->minimum_order_amount) ? $ressettings->minimum_order_amount : ''; ?>" name="minimumorderamt" class="minimumorderamt_cls" type="text" id="#2" placeholder="Minimum Order Amount" required />
                            <span id="#3" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>

                    <li>

                    <li>
                        <div class="items_display_firstline_inn2">
                            <label>custom delivery message <span>*</span></label>
                            <div class="message_box"><textarea name="custm_del_msg" class="custm_del_msg_cls" id="w3review" name="w3review" rows="4" cols="50" placeholder="Custom Delivery Message "><?= isset($ressettings->custom_delivery_message) ? $ressettings->custom_delivery_message : ''; ?></textarea> </div>
                        </div>
                    </li>


                    <li>
                        <div class="items_display_firstline_inn2">
                            <div class="menu_edit_btn">
                                <ul>
                                    <li>

                                        <a class="openBtn openBtnsvcgs" href="#">save changes</a>
                                    </li>

                                </ul>
                                <div class="clear"></div>
                            </div>

                        </div>
                    </li>

                </ul>


                <div class="clear"></div>
            </div>

        </form>






    </div>

</div>


<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>res_resources/js/popup.js" type="text/javascript"></script>


<script>
    $('.openBtnsvcgs').click(function(e) {
        e.preventDefault();
        var formData = new FormData($('#settingsfoodform')[0]);
        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/newsettings',
            type: 'post',
            data: formData,
            dataType: 'json',
            // enctype: 'multipart/form-data',
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data != '') {
                    alert(data);
                    location.reload(true);

                } else {
                    alert(data);
                }
            }
        });
    });
</script>

<script>
    $('.changeresstatus').click(function(e) {

        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/deactivaterest',
            type: 'post',
            dataType: 'json',

            cache: false,

            success: function(data) {
                if (data != '') {
                    alert(data);
                    location.reload(true);

                } else {
                    alert(data);
                }
            }
        });
    });
</script>