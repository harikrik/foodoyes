<div class="varient_item">

    <form action="mailsend.php" method="post" id="foodform">

        <div class="items_display">
            <div class="items_display_firstline">
                <ul>
                    <li>
                        <input type="hidden" name="mainId" value="<?= $itembyid->master_id; ?>">
                        <div class="items_display_firstline_inn">
                            <label>item name <span>*</span></label>
                            <input name="itemName" value="<?= isset($itembyid->item_name) ? $itembyid->item_name : '' ?>" class="itemName_cls" type="text" id="#" placeholder="Enter Item Name" required />
                            <span id="#1" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Discription <span>*</span></label>
                            <input value="<?= isset($itembyid->description) ? $itembyid->description : '' ?>" name="description" class="description_cls" type="text" id="#2" placeholder="Enter Discription" required />
                            <span id="#3" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>category <span>*</span></label>
                            <select name="fudcats" id="#123" class="items_display_firstline_dropdown fudcats_cls">
                                <option value="">Select Category</option>
                                <?php if ($listresults['food_category']) {
                                    foreach ($listresults['food_category'] as $key => $cats) {
                                ?>
                                        <option <?php if ($itembyid->food_cat_id == $cats->food_category_id) {
                                                    echo 'selected';
                                                } ?> value="<?= $cats->food_category_id ?>"><?= $cats->food_category_name ?></option>
                                <?php }
                                } ?>

                            </select>
                        </div>
                    </li>

                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Tax <span>*</span></label>
                            <select name="fudtax" id="#123" class="items_display_firstline_dropdown fudtax_cls">
                                <option value="">Select Category</option>
                                <?php if ($listresults['tax']) {
                                    foreach ($listresults['tax'] as $key => $cats) {
                                ?>
                                        <option <?php if ($itembyid->total_tax == $cats->tax_amt) {
                                                    echo 'selected';
                                                } ?> value="<?= $cats->tax_amt ?>"><?= $cats->tax_amt ?></option>
                                <?php }
                                } ?>

                            </select>

                        </div>
                    </li>

                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Type <span>*</span></label>
                            <select name="fud_types" id="#121" class="items_display_firstline_dropdown fud_types_cls">
                                <option value="" selected>Select Type</option>
                                <option <?php if ($itembyid->food_type == 'veg') {
                                            echo 'selected';
                                        } ?> value="veg">Veg</option>
                                <option <?php if ($itembyid->food_type == 'non-veg') {
                                            echo 'selected';
                                        } ?> value="non-veg">Non-Veg</option>

                            </select>

                        </div>
                    </li>


                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Group <span>*</span></label>
                            <select name="fudgrp" id="#123" class="items_display_firstline_dropdown fudgrp_cls">
                                <option value="">Select Category</option>
                                <?php if ($listresults['food_groups']) {
                                    foreach ($listresults['food_groups'] as $key => $cats) {
                                ?>
                                        <option <?php if ($itembyid->food_group_id == $cats->foodgroupId) {
                                                    echo 'selected';
                                                } ?> value="<?= $cats->foodgroupId ?>"><?= $cats->food_group_name ?></option>
                                <?php }
                                } ?>

                            </select>


                        </div>
                    </li>

                    <li>
                        <div class="items_display_firstline_inn">
                            <label>image <span>*</span></label>
                            <input type="file" class="myfiles_cls" id="myFile" name="myfiles" required />


                        </div>
                    </li>
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>thumb <span>*</span></label>
                            <input type="file" id="myFile2" class="thumpfile_cls" name="thumpfile" required />
                            <span id="#13" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>custom message <span>*</span></label>
                            <label class="switch">
                                <input type="checkbox" name="chngecusmsg" <?php if ($itembyid->custom_message == 1) {
                                                                                echo 'checked';
                                                                            } ?> value="0" class="chngecusmsg_cls">
                                <!-- checked -->
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </li>


                </ul>


                <div class="clear"></div>
            </div>


            <div class="add_food_btn">
                <div class="add_food_btn_left"><a href="#"><i class="fas fa-check"></i></a></div>
                <div class="add_food_btn_right"><a href="#"><i class="fas fa-times"></i></a></div>
                <div class="clear"></div>
            </div>
        </div>





    </form>



    <div class="clear"></div>
</div>




<div class="list-wrapper">

    <div class="list-item">
        <div class="our_menu_head">
            <div class="our_menu_head_left">
                <h1>edit product</h1>
            </div>
            <div class="our_menu_head_right">
                <h1><?= isset($itembyid->item_name) ? $itembyid->item_name : '' ?></h1>
            </div>
            <div class="clear"></div>
        </div>
        <div class="our_menu_data">


            <table>
                <thead>
                    <tr>
                        <th>varient name</th>
                        <th> display price</th>
                        <th>offer price</th>
                        <th>Max qty </th>
                        <th>status</th>
                        <th>Change status</th>
                        <th>actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($fdvarients) {
                        foreach ($fdvarients as $key => $kvl) { ?>
                            <tr>
                                <td style="text-align: left;"><?= isset($kvl->variant_name) ? $kvl->variant_name : 0 ?></td>

                                <td><?= isset($kvl->display_price) ? $kvl->display_price : 0 ?> rs</td>

                                <td><?= isset($kvl->offer_price) ? $kvl->offer_price : 0 ?> rs</td>
                                <td><?= isset($kvl->max_prod_quantity) ? $kvl->max_prod_quantity : 0 ?></td>
                                <td> <?php if ($kvl->status == 1) { ?> Active <?php } else { ?>
                                        Inactive<?php } ?>
                                </td>
                                <td>
                                    <div class="data_butn">
                                        <ul>
                                            <li>
                                                <?php if ($kvl->status == 1) { ?> <div data-masterid="<?= $kvl->unit_id; ?>" data-variant_sts="<?= $kvl->status; ?>" class="inactive_btn variant_sts"><a href="#">make inactive</a></div> <?php } else { ?>
                                                    <div data-masterid="<?= $kvl->unit_id; ?>" data-variant_sts="<?= $kvl->status; ?>" class="active_btn variant_sts"><a href="#">Make active</a></div> <?php } ?>



                                            </li>
                                            <li>
                                                <?php if ($kvl->activate_offer == 1) { ?> <div data-masterid="<?= $kvl->unit_id; ?>" data-offer_sts="<?= $kvl->activate_offer; ?>" class="inactive_btn offer_sts"><a href="#">Deactivte offer</a></div> <?php } else if (($kvl->activate_offer == 2)) { ?> <div data-masterid="<?= $kvl->unit_id; ?>" data-offer_sts="<?= $kvl->activate_offer; ?>" class="active_btn offer_sts"><a href="#">Activate offer</a></div> <?php } ?>
                                            </li>
                                        </ul>
                                        <div class="clear"></div>
                                    </div>
                                </td>
                                <td>
                                    <div class="data_butn">
                                        <ul>
                                            <li>
                                                <div class="edit_btn edit_btn_update" data-editunitid="<?= $kvl->unit_id; ?>"><a href="#"><i class="fas fa-pencil-alt"></i></a></div>
                                            </li>
                                            <li>
                                                <div class="edit_btn delete_deletebtnId" data-deleteid="<?= $kvl->unit_id; ?>"><a href="#"><i class="fas fa-trash-alt"></i></a></div>
                                            </li>
                                        </ul>
                                        <div class="clear"></div>
                                    </div>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                </tbody>


            </table>

        </div>

    </div>





</div>
<div class="item_edit">
    <div class="recent_order">
        <div class="recent_order_left2">
            <h1>Food variant</h1>
        </div>

        <div class="clear"></div>
    </div>
    <form action="mailsend.php" method="post" id="unitfoodform">
        <input type="hidden" name="mainIdmaster" value="<?= $itembyid->master_id; ?>">
        <div class="items_display2">
            <div class="items_display_firstline">
                <ul>
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Product Variant Name <span>*</span></label>
                            <input name="variant_name" type="text" id="#" class="variant_name_cls" placeholder="Product Variant Name " required />
                            <span id="#1" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>
                    <li>
                        <div class="items_display_firstline_inn removenclick">
                            <label>Unit <span>*</span></label>
                            <select name="unitz" id="#123" class="items_display_firstline_dropdown unitz_cls">
                                <option value="">Select Unit</option>
                                <?php if ($unittypes) {
                                    foreach ($unittypes as $key => $cats) {
                                ?>
                                        <option value="<?= $cats->unit_types ?>"><?= $cats->unit_types ?></option>
                                <?php }
                                } ?>

                            </select>
                        </div>
                        <div class=" addnewdivhere"></div>
                    </li>

                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Display Price <span>*</span></label>
                            <input name="display_price" class="display_price_cls" type="text" id="#2" placeholder="Display Price" required />
                            <span id="#3" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>

                </ul>


                <div class="clear"></div>
            </div>




            <div class="items_display_firstline">
                <ul>
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Offer Price <span>*</span></label>
                            <input type="text" id="myFile" name="ofr_price" class="ofr_price_cls" required />
                            <span id="#12" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Max product quantity <span>*</span></label>
                            <input type="text" name="max_qty" class="max_qty" required />
                            <span id="#12" class="spn_Error" style="display:none;"></span>
                        </div>
                    </li>
                    <li>
                        <div class="items_display_firstline_inn">
                            <div class="menu_edit_btn">
                                <ul>
                                    <li>

                                        <input class="submit_btn_units" name="txtName" type="submit" id="#34" required />
                                        <div class="newbutton"></div>
                                        <span id="#35" class="spn_Error" style="display:none;"></span>
                                    </li>
                                    <!-- <li><input class="submit_btn" name="txtName" type="reset" id="#34" required />
                                    <span id="#35" class="spn_Error" style="display:none;"></span>
                                </li> -->
                                </ul>
                                <div class="clear"></div>
                            </div>

                        </div>
                    </li>

                </ul>

                <div class="clear"></div>
            </div>



        </div>
    </form>
</div>

</div>


<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>res_resources/js/popup.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {

        $('#example3').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });

        $('#example31').dataTable({

            "bLengthChange": false, //thought this line could hide the LengthMenu
            "bInfo": false,
            "lengthMenu": [
                [5]
            ]
        });


    });
</script>

<script>
    $('.chngecusmsg_cls').click(function() {
        var _sts = $(this).val();
        if (_sts == 0) {
            $(this).val(1);
        } else {
            $(this).val(0);
        }
    });


    $('.add_food_btn_left').click(function() {

        var formData = new FormData($('#foodform')[0]);
        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/editfud',
            type: 'post',
            data: formData,
            dataType: 'json',
            // enctype: 'multipart/form-data',
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data != '') {
                    alert('Food updated');
                    location.reload(true);

                } else {
                    alert('Failed to update food');
                }
            }
        });
    });



    $('.submit_btn_units').click(function(e) {
        e.preventDefault();
        var formData = new FormData($('#unitfoodform')[0]);
        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/addnewunit',
            type: 'post',
            data: formData,
            dataType: 'json',
            // enctype: 'multipart/form-data',
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data != '') {
                    alert('unit added');
                    location.reload(true);

                } else {
                    alert('Failed to add unit');
                }
            }
        });
    });
</script>

<script>
    $('.offer_sts').click(function(e) {
        e.preventDefault();
        var _ofrsts = $(this).data('offer_sts');
        var _masterid = $(this).data('masterid');

        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/updateofferstatus',
            type: 'post',
            data: {
                'masterId': _masterid,
                'offerstatus': _ofrsts
            },
            dataType: 'json',


            success: function(data) {
                if (data != '') {
                    alert('offer status updated');
                    location.reload(true);

                } else {
                    alert('Failed to Update offer status');
                }
            }
        });
    });
</script>

<script>
    $('.variant_sts').click(function(e) {
        e.preventDefault();
        var _ofrsts = $(this).data('variant_sts');
        var _masterid = $(this).data('masterid');

        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/updateunitsstatus',
            type: 'post',
            data: {
                'masterId': _masterid,
                'offerstatus': _ofrsts
            },
            dataType: 'json',


            success: function(data) {
                if (data != '') {
                    alert('status status updated');
                    location.reload(true);

                } else {
                    alert('Failed to Update  status');
                }
            }
        });
    });
</script>

<script>
    $('.edit_btn_update').click(function(e) {

        e.preventDefault();

        var _editid = $(this).data('editunitid');
        $('.removenclick').remove();
        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/getuntstoedit',
            type: 'post',
            data: {
                'masterId': _editid,

            },
            dataType: 'json',


            success: function(data) {
                if (data != '') {
                    var html = '';
                    html += '<div class="items_display_firstline_inn ">';
                    html += '<input type="hidden" value="' + data['singlerow'].unit_id + '" name="masterofupdate" >';
                    html += ' <label>Unit <span>*</span></label>';
                    html += ' <select name="unitz" id="#123" class="items_display_firstline_dropdown unitz_cls">';
                    var _sel = data['singlerow'].unit;
                    var _selected = '';

                    $.each(data['result'], function(k, val) {
                        if (_sel == val.unit_types) {
                            _selected = 'selected';
                        } else {
                            _selected = '';
                        }
                        html += ' <option value="">Select Unit</option>';

                        html += ' <option ' + _selected + ' value="' + val.unit_types + '">' + val.unit_types + '</option>';


                    });

                    html += '  </select>';
                    html += ' </div>';
                    $('.addnewdivhere').html(html);
                    $('.variant_name_cls').val(data['singlerow'].variant_name);
                    $('.display_price_cls').val(data['singlerow'].display_price);
                    $('.ofr_price_cls').val(data['singlerow'].offer_price);
                    $('.submit_btn_units').remove();
                    // $('.newbutton').html('<button class="updatedbtnz" type="button">Update</button>');
                    $('.newbutton').html(' <input class="updatedbtnz" name="txtName" type="submit" id="#34" required />');
                } else {
                    alert('Failed to Update  status');
                }
            }
        });
        e.preventDefault();
    });
</script>

<script>
    $(document).on('click', '.updatedbtnz', function(e) {
        e.preventDefault();


        var formData = new FormData($('#unitfoodform')[0]);
        var _url = "<?= base_url() ?>";
        $.ajax({

            url: _url + 'restaurant/home/updateoldunit',
            type: 'post',
            data: formData,
            dataType: 'json',
            // enctype: 'multipart/form-data',
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data != '') {
                    alert('unit updated');
                    location.reload(true);

                } else {
                    alert('Failed to update unit');
                }
            }
        });
    });
    e.preventDefault();
</script>

<script>
    $('.delete_deletebtnId').click(function(e) {
        e.preventDefault();
        if (confirm('Are yo sure to delete this variant')) {
            var _id = $(this).data('deleteid');
            var _url = "<?= base_url() ?>";
            $.ajax({

                url: _url + 'restaurant/home/deleteunits',
                type: 'post',
                data: {
                    'masterId': _id,

                },
                dataType: 'json',


                success: function(data) {
                    if (data != '') {
                        alert('unit deleted');
                        location.reload(true);

                    } else {
                        alert('Failed to delete unit');
                    }
                }
            });
        }
    });
</script>