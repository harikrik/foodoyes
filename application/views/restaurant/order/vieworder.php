<style>
    /* pop up style */

    .cd-nugget-info {
        text-align: center;
        position: absolute;
        width: 100%;
        height: 50px;
        line-height: 50px;
        bottom: 0;
        left: 0;
    }

    .cd-nugget-info a {
        position: relative;
        font-size: 14px;
        color: #5e6e8d;
        -webkit-transition: all 0.2s;
        -moz-transition: all 0.2s;
        transition: all 0.2s;
    }

    .no-touch .cd-nugget-info a:hover {
        opacity: .8;
    }

    .cd-nugget-info span {
        vertical-align: middle;
        display: inline-block;
    }

    .cd-nugget-info span svg {
        display: block;
    }

    .cd-nugget-info .cd-nugget-info-arrow {
        fill: #5e6e8d;
    }

    .cd-popup-trigger {
        display: block;
        line-height: 50px;
        margin: 0 auto;
        padding: 10px 10px;
        text-align: center;
        color: #FFF;
        font-size: 14px;
        font-weight: bold;
        text-transform: uppercase;
        border-radius: 10%;
        background: #f15733;
        box-shadow: 0 3px 0 rgba(0, 0, 0, 0.07);
    }

    .cd-popup-trigger .fa {
        font-size: 15px;

    }




    @media only screen and (min-width: 1170px) {
        .cd-popup-trigger {
            /* margin: 6em auto; */
            margin: 1% auto;
        }
    }

    /* -------------------------------- 

xpopup 

-------------------------------- */
    .cd-popup {
        position: fixed;
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        background-color: rgba(94, 110, 141, 0.9);
        opacity: 0;
        visibility: hidden;
        -webkit-transition: opacity 0.3s 0s, visibility 0s 0.3s;
        -moz-transition: opacity 0.3s 0s, visibility 0s 0.3s;
        transition: opacity 0.3s 0s, visibility 0s 0.3s;
    }

    .cd-popup.is-visible {
        opacity: 1;
        visibility: visible;
        -webkit-transition: opacity 0.3s 0s, visibility 0s 0s;
        -moz-transition: opacity 0.3s 0s, visibility 0s 0s;
        transition: opacity 0.3s 0s, visibility 0s 0s;
    }

    .cd-popup-container {
        position: relative;
        width: 90%;
        max-width: 400px;
        margin: 4em auto;
        background: #FFF;
        border-radius: .25em .25em .4em .4em;
        text-align: center;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
        -webkit-transform: translateY(-40px);
        -moz-transform: translateY(-40px);
        -ms-transform: translateY(-40px);
        -o-transform: translateY(-40px);
        transform: translateY(-40px);
        /* Force Hardware Acceleration in WebKit */
        -webkit-backface-visibility: hidden;
        -webkit-transition-property: -webkit-transform;
        -moz-transition-property: -moz-transform;
        transition-property: transform;
        -webkit-transition-duration: 0.3s;
        -moz-transition-duration: 0.3s;
        transition-duration: 0.3s;
    }

    .cd-popup-container p {
        padding: 3em 1em;
    }

    .cd-popup-container .cd-buttons:after {
        content: "";
        display: table;
        clear: both;
    }

    .cd-popup-container .cd-buttons li {
        float: left;
        width: 50%;
        list-style: none;
    }

    .cd-popup-container .cd-buttons a {
        display: block;
        height: 60px;
        line-height: 60px;
        text-transform: uppercase;
        color: #FFF;
        -webkit-transition: background-color 0.2s;
        -moz-transition: background-color 0.2s;
        transition: background-color 0.2s;
    }

    .cd-popup-container .cd-buttons li:first-child a {
        background: #fc7169;
        border-radius: 0 0 0 .25em;
    }

    .no-touch .cd-popup-container .cd-buttons li:first-child a:hover {
        background-color: #fc8982;
    }

    .cd-popup-container .cd-buttons li:last-child a {
        background: #b6bece;
        border-radius: 0 0 .25em 0;
    }

    .no-touch .cd-popup-container .cd-buttons li:last-child a:hover {
        background-color: #c5ccd8;
    }

    .cd-popup-container .cd-popup-close {
        position: absolute;
        top: 8px;
        right: 8px;
        width: 30px;
        height: 30px;
    }

    .cd-popup-container .cd-popup-close::before,
    .cd-popup-container .cd-popup-close::after {
        content: '';
        position: absolute;
        top: 12px;
        width: 14px;
        height: 3px;
        background-color: #8f9cb5;
    }

    .cd-popup-container .cd-popup-close::before {
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        transform: rotate(45deg);
        left: 8px;
    }

    .cd-popup-container .cd-popup-close::after {
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        -ms-transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        transform: rotate(-45deg);
        right: 8px;
    }

    .is-visible .cd-popup-container {
        -webkit-transform: translateY(0);
        -moz-transform: translateY(0);
        -ms-transform: translateY(0);
        -o-transform: translateY(0);
        transform: translateY(0);
    }

    @media only screen and (min-width: 1170px) {
        .cd-popup-container {
            margin: 8em auto;
        }
    }





    /* pop up style end */
</style>


<div class="">


    <div class="space"></div>
    <div class="recent_order">
        <div class="recent_order_left">
            <h1>order details</h1>
        </div>
        <div class="recent_order_right">
            <div class="recent_order_right_left">
                <input type="text" class="search-place" placeholder="Search">
            </div>
            <div class="recent_order_right_right"><a href="#"><img src="images/search.png" alt="" /></a></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>


    <div class="order_details">
        <div class="items_display2">
            <div class="items_display_firstline">
                <ul>
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Order Number<span></span></label>

                            <br>
                            <h2><?= $orders['orderdetails']->order_master_id; ?></h2>
                        </div>
                    </li>

                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Customer name<span>*</span></label>
                            <br>
                            <h2><?= $orders['orderdetails']->first_name; ?></h2>
                        </div>
                    </li>
                    <!-- <li>
	                        <div class="items_display_firstline_inn">
	                            <label>Order Date & Time<span>*</span></label>
                                <br>
	                            <h2><?= $orders['orderdetails']->first_name; ?></h2>
	                        </div>
	                    </li> -->
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Delivery Address<span>*</span></label>
                            <br>
                            <h2><?= $orders['del_address']->address; ?></h2>
                        </div>
                    </li>

                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Location<span>*</span></label>
                            <br>
                            <h2><?= $orders['del_address']->location_address; ?></h2>
                        </div>
                    </li>

                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Landmark<span>*</span></label>
                            <br>
                            <h2><?= $orders['del_address']->landmark; ?></h2>
                        </div>
                    </li>
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Phone Number<span>*</span></label>
                            <br>
                            <h2><?= $orders['del_address']->phone_number; ?></h2>
                        </div>
                    </li>
                    <!-- <li>
	                        <div class="items_display_firstline_inn">
	                            <label>Sub Total<span>*</span></label>
	                            <br>
	                            <h2><?= $orders['orderdetails']->order_amount; ?></h2>
	                        </div>
	                    </li> -->
                    <!-- <li>
	                        <div class="items_display_firstline_inn">
	                            <label>Packing Charges<span>*</span></label>
	                            <input name="txtName" type="text" id="#2" placeholder="Packing Charges" required />
	                            <span id="#3" class="spn_Error" style="display:none;"></span>
	                        </div>
	                    </li> -->

                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Delivery Charges<span>*</span></label>
                            <br>
                            <h2><?= $orders['orderdetails']->delivery_charge; ?></h2>
                        </div>
                    </li>



                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Grand Total<span>*</span></label>
                            <br>
                            <h2><?= $orders['orderdetails']->order_amount; ?></h2>
                        </div>
                    </li>
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Total Tax <span>*</span></label>
                            <br>
                            <h2><?= $orders['orderdetails']->tax_amount; ?></h2>
                        </div>
                    </li>
                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Delivery Mode<span>*</span></label>
                            <br>
                            <h2><?= $orders['orderdetails']->payment_method; ?></h2>
                        </div>
                    </li>


                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Delivery Date<span>*</span></label>
                            <br>
                            <h2><?= $orders['orderdetails']->delivery_date; ?></h2>
                        </div>
                    </li>
                    <?php if ($bankdetails) { ?>
                        <li>
                            <div class="items_display_firstline_inn">
                                <label>Delivery Mode<span>*</span></label>
                                <br>
                                <h2><?= $bankdetails['del_mode']; ?></h2>
                            </div>
                        </li>
                        <li>
                            <div class="items_display_firstline_inn">
                                <label>Time From<span>*</span></label>
                                <br>
                                <h2><?= $bankdetails['starttime']; ?></h2>
                            </div>
                        </li>
                        <li>
                            <div class="items_display_firstline_inn">
                                <label>Time To<span>*</span></label>
                                <br>
                                <h2><?= $bankdetails['endtime']; ?></h2>
                            </div>
                        </li>
                    <?php } ?>
                    <!-- <li>
                        <div class="items_display_firstline_inn">
                            <label>Status<span>*</span></label>
                            <br>
                            <h2><?php if ($orders['orderdetails']->status == 'Delivered') {
                                    echo 'Delivered';
                                } else {
                                    echo 'Undelivered';
                                } ?></h2>
                        </div>
                    </li> -->
                    <!-- <?php if (($orders['orderdetails']->payment_method) && ($orders['orderdetails']->payment_method == 'Bank')) { ?>
                        <li>
                            <div class="items_display_firstline_inn">
                                <label>Time Slot<span>*</span></label>
                                <input name="txtName" type="text" id="#2" placeholder="Grand Total" required />
                                <span id="#3" class="spn_Error" style="display:none;"></span>
                            </div>
                        </li>

                    <?php } ?> -->



                    <!-- <li>
	                        <div class="items_display_firstline_inn">
	                            <label>Payment mode<span>*</span></label>
	                            <select id="#123" class="items_display_firstline_dropdown">
	                                <option value="Select tax" selected>Select Payment Mode</option>
	                                <option value="gst0">Cash On Delivery</option>
	                                <option value="gst5">Online</option>



	                            </select>
	                        </div>
	                    </li> -->


                    <!-- <li>
	                        <div class="items_display_firstline_inn">
	                            <label>Delivery Partner<span>*</span></label>
	                            <input name="txtName" type="text" id="#2" placeholder="Delivery Partner" required />
	                            <span id="#3" class="spn_Error" style="display:none;"></span>
	                        </div>
	                    </li> -->

                    <li>
                        <div class="items_display_firstline_inn">
                            <label>Update Order Status<span>*</span></label>
                            <select class="orderstatus">
                                <option value="">Select</option>
                                <?php if ($allStatus) {
                                    foreach ($allStatus as $keys => $stsvl) {
                                        if ($stsvl->fud_sts_master_id >= $orders['orderdetails']->order_status) {  ?>
                                            <option <?php if ($stsvl->fud_sts_master_id == $orders['orderdetails']->order_status) {
                                                        echo 'selected';
                                                    } ?> value="<?= $stsvl->fud_sts_master_id; ?>"><?= $stsvl->fud_status; ?></option>
                                <?php }
                                    }
                                } ?>


                            </select>
                        </div>
                    </li>



                </ul>
                <div class="clear"></div>
            </div>

        </div>

    </div>
    <div class="recent_order">
        <div class="recent_order_left2">
            <h1>List Order Details</h1>
        </div>

        <div class="clear"></div>
    </div>
    <div class="order_details_new">

        <div class="our_menu_data">


            <table>
                <tr>
                    <th>item name</th>
                    <th>Unit</th>
                    <th>Qty</th>
                    <th>price</th>
                    <th> Custom Message</th>
                    <th>tax</th>
                    <th>Sub total</th>



                </tr>
                <?php foreach ($orders['orderItems'] as $key => $orderItems) { ?>
                    <tr>
                        <td style="text-align: left;"><?= $orderItems->item_name ?></td>
                        <td><?= $orderItems->food_name; ?></td>
                        <td><?= $orderItems->quantity; ?></td>
                        <td>Rs <?php if ($orderItems->quantity > 0) {
                                    $qty = $orderItems->quantity;
                                } else {
                                    $qty = 1;
                                }
                                echo  $orderItems->selling_price / $qty; ?></td>
                        <td><?= $orderItems->message; ?></td>
                        <td><?= $orderItems->total_tax; ?></td>

                        <td>

                            <?php $pieces = explode(" ", $orderItems->total_tax);
                            $tax = ($orderItems->selling_price * $pieces[1]) / 100;

                            $subtotal = $orderItems->selling_price  + $tax;
                            echo 'Rs ' . $subtotal;
                            ?>


                        </td>

                    </tr>
                <?php } ?>

            </table>

        </div>

    </div>
</div>


<script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url() ?>res_resources/js/popup.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.6/jquery.simplePagination.js"></script>
<script src="js/pagination.js"></script>


<script>
    $('.orderstatus').change(function(e) {
        e.preventDefault();
        var _sts = $(this).val();
        var _id = "<?= $orders['orderdetails']->order_master_id; ?>";

        if (_sts != '') {

            var _url = "<?= base_url() ?>";
            $.ajax({

                url: _url + 'restaurant/order/updateorderstatus',
                type: 'post',
                data: {
                    'orderid': _id,
                    'status': _sts
                },
                dataType: 'json',
                // enctype: 'multipart/form-data',

                success: function(data) {
                    if (data > 0) {
                        alert('Status updated');
                        location.reload(true);

                    } else {
                        alert('Failed to update unit');
                    }
                }
            });
        }
    });

    $(document).ready(function() {
        //open popup

        $(document).on('click', '.cd-popup-trigger', function(event) {
            event.preventDefault();
            $('.cd-popup').addClass('is-visible');
        });
    });
</script>