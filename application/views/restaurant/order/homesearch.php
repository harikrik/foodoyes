<style>
    .button_reject_new {
        padding: 1em 30px;
        text-align: center;
        width: auto;
        margin: 0 auto;
        display: block;
        -webkit-transition: all linear 0.15s;
        transition: all linear 0.15s;
        border-radius: 5px;
        background: red;
        font-size: 17px;
        text-decoration: none;
        border: none;
        letter-spacing: 1px;
        text-transform: uppercase;
        color: #FFFFFF !important;
        font-family: 'Oswald', sans-serif;
        font-weight: bold !important;
    }

    /* pop up style */

    .box_new_popup {
        width: auto;
        margin: 0 auto;
        background: red;
        padding: 35px;
        border: 2px solid red;
        text-align: center;
    }

    .button_new_popup {
        font-size: 1em;
        padding: 10px;
        color: #fff;
        border: 2px solid #06D85F;
        border-radius: 20px/50px;
        text-decoration: none;
        cursor: pointer;
        transition: all 0.3s ease-out;
    }

    .button_new_popup:hover {
        background: green;
    }

    .overlay_new_popup {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;
    }

    .overlay_new_popup:target {
        visibility: visible;
        opacity: 1;
    }

    .popup_new_popup {
        margin: 70px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 30%;
        position: relative;
        transition: all 5s ease-in-out;
    }

    .popup_new_popup h2 {
        margin-top: 0;
        color: #333;
        font-family: Tahoma, Arial, sans-serif;
    }

    .popup_new_popup .close {
        position: absolute;
        top: 20px;
        right: 30px;
        transition: all 200ms;
        font-size: 30px;
        font-weight: bold;
        text-decoration: none;
        color: #333;
    }

    .popup_new_popup .close:hover {
        color: #06D85F;
    }

    .popup_new_popup .content {
        max-height: 30%;
        overflow: auto;
    }

    @media screen and (max-width: 700px) {
        .box_new_popup {
            width: 70%;
        }

        .button_new_popup {
            font-size: 25px;
            color: red;
            padding: 25%;
            border: red solid 1px;
            border-radius: 6px;
            overflow: hidden;
        }
    }
</style>



<div class="dasboard_main">




    <div class="dasboard_main">



        <div class="">
            <div class="">
                <form method="post" action="#">
                    <div class="recent_order">
                        <div class="recent_order_left">
                            <h1>Search Results</h1>
                        </div>
                        <!-- <div class="recent_order_right">
                            <div class="recent_order_right_left">
                                <input type="text" name="fudname" class="search-place srchhere" placeholder="Search">
                            </div>

                            <div class="recent_order_right_right"><button type="submit"><img src="https://app.foodoyes.com/dev/assets/search.png" alt=""></button></div>

                            <div class="clear"></div>
                        </div> -->
                        <div class="clear"></div>
                    </div>
                </form>





                <div class="list-wrapper">

                    <div class="list-item">

                        <div class="our_menu_data">
                            <div id="dialog"></div>

                            <table>
                                <thead>
                                    <tr>
                                        <th>Sl no</th>
                                        <th>Order No</th>
                                        <th>Date time</th>
                                        <th>Customer Name</th>
                                        <th>Contact No</th>
                                        <th>Order Status</th>
                                        <th> Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if ($listorders) {
                                        $i = 1;
                                        foreach ($listorders as $lorder) { ?>

                                            <tr>
                                                <td style="text-align: left;"><?= $i++; ?> </td>
                                                <td><?= $lorder->orderId ?></td>
                                                <td><?= $lorder->createdDate ?></td>
                                                <td><?= $lorder->customer->first_name ?></td>
                                                <td><?= $lorder->customer->mobile ?></td>
                                                <td><?= $lorder->order_status ?></td>
                                                <td>
                                                    <div class="data_butn">
                                                        <ul>
                                                            <li>
                                                                <div class="view_btn"><a href="<?= base_url() ?>restaurant/vieworder/<?= base64_encode($lorder->orderId) ?>">view</a></div>
                                                            </li>
                                                            <?php if ($lorder->order_status_val == 1) { ?> <li>
                                                                    <div class="view_btn"><a class="acceptorder" data-orderId="<?= $lorder->orderId ?>" data-orderStatus="<?= $lorder->order_status_val ?>" href="#">Accept</a></div>
                                                                </li>
                                                                <li>
                                                                    <div class="view_btn"><a class="rejorder" data-orderId="<?= $lorder->orderId ?>" data-orderStatus="<?= $lorder->order_status_val ?>" href="#">Reject</i></a></div>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                        <div class="clear"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                    <?php }
                                    } ?>

                                </tbody>


                            </table>

                        </div>

                    </div>




                </div>

                <div id="pagination-container"></div>
            </div>








            <div class="clear"></div>


        </div>

        <div class="clear"></div>



    </div>



    <script src="<?= base_url() ?>resources/js/main_jQuery.js" type="text/javascript"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

    <script src="<?= base_url() ?>res_resources/js/popup.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.6/jquery.simplePagination.js"></script>
    <script src="js/pagination.js"></script>

    <script>
        $(document).ready(function() {

            $('#example3').dataTable({

                "bLengthChange": false, //thought this line could hide the LengthMenu
                "bInfo": false,
                "lengthMenu": [
                    [5]
                ]
            });

            $('#example31').dataTable({

                "bLengthChange": false, //thought this line could hide the LengthMenu
                "bInfo": false,
                "lengthMenu": [
                    [5]
                ]
            });


        });


        // Accept Order

        $('.acceptorder').click(function(e) {
            e.preventDefault();
            var _id = $(this).attr('data-orderId');
            var _sts = $(this).attr('data-orderStatus');
            if (confirm('Accept this order ?')) {
                var _url = "<?= base_url() ?>";
                $.ajax({

                    url: _url + 'restaurant/order/updateorderstatus',
                    type: 'post',
                    data: {
                        'orderid': _id,
                        'status': 2
                    },
                    dataType: 'json',
                    // enctype: 'multipart/form-data',

                    success: function(data) {
                        if (data > 0) {
                            alert('Status updated');
                            location.reload(true);

                        } else {
                            alert('Failed to update status');
                        }
                    }
                });
            }
        });


        $('.rejorder').click(function(e) {
            e.preventDefault();
            var _id = $(this).attr('data-orderId');
            var _sts = $(this).attr('data-orderStatus');
            if (confirm('Reject  this order ?')) {
                showDialog1(_id);

            }
        });

        function showDialog1(_id) {
            $("#dialog").html("<textarea class='reject_reason' cols='100' rows='5'></textarea><button class='cancelorder' data-cnId='" + _id + "'>Proceed</button>");
            $("#dialog").dialog("option", "title", "Loading....").dialog("open");
            $("span.ui-dialog-title").text('title here');
            $("#dialog").dialog({
                autoOpen: false,
                resizable: true,
                width: "350",
                height: 300,
                modal: true,
                buttons: {
                    "Close": function() {
                        $(this).dialog("close");
                    }
                }
            });
        }

        $(function() {
            $(document).on('click', '.cancelorder', function(e) {
                e.preventDefault();
                var _did = $(this).attr('data-cnId');
                var resn = $('.reject_reason').val();
                var _url = "<?= base_url() ?>";
                if (resn != '') {
                    $.ajax({

                        url: _url + 'restaurant/order/cancelorder',
                        type: 'post',
                        data: {
                            'orderid': _did,
                            'status': 6,
                            'reason': resn,
                        },
                        dataType: 'json',
                        // enctype: 'multipart/form-data',

                        success: function(data) {
                            if (data > 0) {
                                alert('Status updated');
                                location.reload(true);

                            } else {
                                alert('Failed to update status');
                            }
                        }
                    });
                } else {
                    alert('Reason required');
                }
            });

        });
    </script>