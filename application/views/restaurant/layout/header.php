<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <title>Foodoyes Dashboard</title>
    <link rel="shortcut icon" type="<?= base_url() ?>res_resources/image/png" href="images/favicon.png" />
    <link href="<?= base_url() ?>res_resources/css/foodoyes.css" rel="stylesheet" type="text/css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400&family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="<?= base_url() ?>res_resources/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>res_resources/css/owl.css" rel="stylesheet" type="text/css" />
    <meta name="description" content="foodoyes">
    <meta name="keywords" content="foodoyes">
    <link href="<?= base_url() ?>res_resources/fonts/fontawesome-free-5.12.0-web/css/all.css" rel="stylesheet" />

</head>

<body>

    <!--INDEX SLIDER-->

    <div class="dasboard_main">