<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('res/order_model', 'omodel');
        $this->load->model('res/home_model', 'hmodel');
        $this->is_res_session_exist();
    }

    public function index($offset = 0)
    {
        $data = array();
        $data = array();
        $this->load->library('pagination');
        $config['base_url'] = base_url('restaurant/home/index');
        $config['total_rows'] = $this->omodel->getnumOfRecords();
        $config['per_page'] = 10;
        //pagination styles start
        $config['full_tag_open'] = "<ul class='pagination newclasstart'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="nearby_page">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active active_pagination"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li class="test7">';
        $config['prev_tag_open'] = '<div class="previous newclasstart">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li class="test 2">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="test 1">';
        $config['last_tag_close'] = '</li>';



        $config['prev_link'] = '<i class="fa fa-long-arrow-left previouspagilink"></i>Previous Page';
        $config['prev_tag_open'] = '<li class="first_page">';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right nextpagilink"></i>';
        $config['next_tag_open'] = '<li class="nxt_pge">';
        $config['next_tag_close'] = '</div>';
        $config['next_tag_close'] = '</li>';

        // pagination style end
        $this->pagination->initialize($config);
        $data = array();
        $data['listorders'] =   $this->omodel->getAllOrders($config['per_page'], $offset);

        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/home/home', $data);
        $this->load->view('restaurant/layout/footer');
        // $this->res_is_loggedin();
    }

    public function newfud()
    {

        $cmsg = $this->input->post('chngecusmsg');
        $fud['restaurant_id'] =  $_SESSION['uid'];
        $fud['item_name'] =  $this->input->post('itemName');
        $fud['description'] = $this->input->post('description');
        $fud['food_cat_id'] = $this->input->post('fudcats');
        $fud['total_tax'] = $this->input->post('fudtax');
        $fud['food_type'] = $this->input->post('fud_types');
        $fud['food_group_id'] = $this->input->post('fudgrp');
        $fud['custom_message'] = isset($cmsg)  ? $cmsg : 0;


        // print_r($_FILES['thumpfile']);
        $folder = 'assets/foodimage/' . $_SESSION['uid'] . '_fudimage/';
        if (!file_exists($folder)) {

            mkdir($folder, 0755, TRUE);
        }
        // image
        $tmp_name = $_FILES["newfoodimage"]["tmp_name"];
        $name = basename($_FILES["newfoodimage"]["name"]);
        move_uploaded_file($tmp_name, "$folder/$name");
        $fud['food_image'] =  $folder  . $name;


        // thump image
        $folderth = 'assets/foodimages/' . $_SESSION['uid'] . '_fudimage/thump/';
        if (!file_exists($folderth)) {

            mkdir($folderth, 0755, TRUE);
        }
        $tmp_name = $_FILES["thumpfile"]["tmp_name"];
        $name = basename($_FILES["thumpfile"]["name"]);
        move_uploaded_file($tmp_name, "$folderth/$name");
        $fud['thump_image'] = $folderth  . $name;


        $result =  $this->hmodel->adNewFudItem($fud);
        echo json_encode($result);
    }

    public function listFoods($offset = 0)
    {
        $this->load->library('pagination');
        $config['base_url'] = base_url('restaurant/home/listFoods');
        $config['total_rows'] = $this->hmodel->getnumOfRecords();
        $config['per_page'] = 3;
        //pagination styles start
        $config['full_tag_open'] = "<ul class='pagination newclasstart'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="nearby_page">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active active_pagination"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li class="test7">';
        $config['prev_tag_open'] = '<div class="previous newclasstart">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li class="test 2">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="test 1">';
        $config['last_tag_close'] = '</li>';



        $config['prev_link'] = '<i class="fa fa-long-arrow-left previouspagilink"></i>Previous Page';
        $config['prev_tag_open'] = '<li class="first_page">';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right nextpagilink"></i>';
        $config['next_tag_open'] = '<li class="nxt_pge">';
        $config['next_tag_close'] = '</div>';
        $config['next_tag_close'] = '</li>';

        // pagination style end
        $this->pagination->initialize($config);
        $data = array();
        $data['listresults'] =   $this->hmodel->getAllItemStoAddNewFood();
        $data['menuitems'] =   $this->hmodel->getMenuItems($config['per_page'], $offset);

        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/home/listfud', $data);
        // $this->load->view('restaurant/layout/footer');
    }

    public function editfudlist($id)
    {

        $id = base64_decode($id);

        $data = array();
        $data['listresults'] =   $this->hmodel->getAllItemStoAddNewFood();
        $data['unittypes'] =   $this->hmodel->getUnitTypes($id);
        $data['itembyid'] =   $this->hmodel->getfudsById($id);
        $data['menuitems'] =   $this->hmodel->getMenuItems(1, 1);
        $data['fdvarients'] =   $this->hmodel->getFoodvariants($id);
        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/home/editfudlist', $data);
    }
    public function editfud()
    {
        $mainId = $this->input->post('mainId');
        $cmsg = $this->input->post('chngecusmsg');
        $fud['restaurant_id'] =  $_SESSION['uid'];
        $fud['item_name'] =  $this->input->post('itemName');
        $fud['description'] = $this->input->post('description');
        $fud['food_cat_id'] = $this->input->post('fudcats');
        $fud['total_tax'] = $this->input->post('fudtax');
        $fud['food_type'] = $this->input->post('fud_types');
        $fud['food_group_id'] = $this->input->post('fudgrp');
        $fud['custom_message'] = isset($cmsg)  ? $cmsg : 0;



        if (isset($_FILES['newfoodimage'])) {
            $folder = 'assets/foodimage/' . $_SESSION['uid'] . '_fudimage/';
            if (!file_exists($folder)) {

                mkdir($folder, 777, true);
            }
            $tmp_name = $_FILES["newfoodimage"]["tmp_name"];
            $name = basename($_FILES["newfoodimage"]["name"]);
            move_uploaded_file($tmp_name, "$folder/$name");
            $fud['food_image'] =  $folder  . $name;
        }
        $folderth = 'assets/foodimages/' . $_SESSION['uid'] . '_fudimage/thump/';
        if (!file_exists($folderth)) {

            mkdir($folderth, 777, true);
        }
        if ($_FILES['thumpfile']) {
            $tmp_name = $_FILES["thumpfile"]["tmp_name"];
            $name = basename($_FILES["thumpfile"]["name"]);
            move_uploaded_file($tmp_name, "$folderth/$name");
            $fud['thump_image'] = $folderth  . $name;
            $result =  $this->hmodel->updateFudItems($fud, $mainId);
            echo json_encode($result);
        }
    }
    public function addnewunit()
    {
        $ofprc =  $this->input->post('ofr_price');
        if (($ofprc != '') && ($ofprc > 0)) {
            $unt['activate_offer']  = 1;
        } else {
            $unt['activate_offer']  = 0;
        }
        $unt['variant_name'] = $this->input->post('variant_name');
        $unt['max_prod_quantity'] = $this->input->post('max_qty');
        $unt['unit'] = $this->input->post('unitz');
        $unt['display_price'] = $this->input->post('display_price');
        $unt['offer_price'] = $this->input->post('ofr_price');
        $unt['master_id'] = $this->input->post('mainIdmaster');
        $unt['restaurant_id'] = $_SESSION['uid'];

        $unt['status'] = 0;

        $result =  $this->hmodel->addnewunits($unt);
        echo json_encode($result);
    }

    public function updateofferstatus()
    {
        $masterId = $this->input->post('masterId');
        $offerstatus = $this->input->post('offerstatus');
        $result =  $this->hmodel->updateofferstatus($masterId, $offerstatus);
        echo json_encode($result);
    }
    public function updateunitsstatus()
    {
        $masterId = $this->input->post('masterId');
        $offerstatus = $this->input->post('offerstatus');
        $result =  $this->hmodel->updatevariantstatus($masterId, $offerstatus);
        echo json_encode($result);
    }
    public function getuntstoedit()
    {
        $masterId = $this->input->post('masterId');

        $result =  $this->hmodel->getuntstoedit($masterId);
        echo json_encode($result);
    }
    public function updateoldunit()
    {
        $this->input->post('mainIdmaster');
        $dta['variant_name'] = $this->input->post('variant_name');
        $mainId =  $this->input->post('masterofupdate');
        $dta['unit'] = $this->input->post('unitz');
        $dta['max_prod_quantity'] = $this->input->post('max_qty');
        $dta['display_price'] = $this->input->post('display_price');
        $dta['offer_price'] = $this->input->post('ofr_price');
        if ($this->input->post('ofr_price') > 0) {
            $dta['activate_offer']  = 1;
        } else {
            $dta['activate_offer']  = 0;
        }
        $result =  $this->hmodel->updateoldunit($dta, $mainId);
        echo json_encode($result);
    }
    public function deleteunits()
    {
        $mainId =  $this->input->post('masterId');
        $result =  $this->hmodel->deleteunits($mainId);
        echo json_encode($result);
    }
    public function changerestfusstatus()
    {
        $mainId =  $this->input->post('restaurant_id');
        $result =  $this->hmodel->changerestfusstatus($mainId);
        echo json_encode($result);
    }
    public function foodtime()
    {
        $data = array();
        $data['listresults'] =   $this->hmodel->getAllFudCats();
        // $data['menuitems'] =   $this->hmodel->getMenuItems();
        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/home/fudtime', $data);
        // $this->load->view('restaurant/layout/footer');
    }
    public function newtiming()
    {
        $mainId =  $this->input->post('catid');
        $_st_time =  $this->input->post('_st_time');
        $_end_time =  $this->input->post('_end_time');
        $result =  $this->hmodel->updateresttimingStatus($mainId, $_SESSION['uid'], $_st_time, $_end_time);
        echo json_encode($result);
    }
    public function charge_distance()
    {
        $data = array();
        $data['list_distances'] =   $this->hmodel->getAllDistances();
        // $data['menuitems'] =   $this->hmodel->getMenuItems();
        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/home/charge_distance', $data);
        // $this->load->view('restaurant/layout/footer');
    }

    public function newdistance()
    {

        $dis['minimum_distance'] = $this->input->post('minimum_distance');
        $dis['maximum_distance'] = $this->input->post('maximum_distance');
        $dis['delivery_charge'] = $this->input->post('delivery_charge');
        $dis['restaurant_id'] = $_SESSION['uid'];
        $result =  $this->hmodel->addNewDistances($dis);
        echo json_encode($result);
    }

    public function newdistanceupdate()
    {
        $_mianId = $this->input->post('_mianId');
        $dis['minimum_distance'] = $this->input->post('_min_dis');
        $dis['maximum_distance'] = $this->input->post('_max_dis');
        $dis['delivery_charge'] = $this->input->post('_del_charge');
        $dis['restaurant_id'] = $_SESSION['uid'];
        $result =  $this->hmodel->updateNewDistances($dis, $_mianId);
        echo json_encode($result);
    }
    public function settings()
    {
        $data = array();
        $data['reststuas'] =   $this->hmodel->getResSts();
        $data['ressettings'] =   $this->hmodel->getRestSettings();
        $data['ressettings_online'] =    $result =  $this->hmodel->getResOnline();
        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/home/settings', $data);
        // $this->load->view('restaurant/layout/footer');
    }
    public function newsettings()
    {

        $smainid =   $this->input->post('smainid');
        $settings['opening_time'] =  $this->input->post('opentime');
        $settings['maximum_delivery_distance'] =  $this->input->post('max_del');
        $settings['closing_time'] =  $this->input->post('closingtiem');
        $settings['minimum_order_amount'] =  $this->input->post('minimumorderamt');
        $settings['custom_delivery_message'] =  $this->input->post('custm_del_msg');
        $settings['currency_symbol'] =  $this->input->post('currencysymbol');
        $settings['restaurant_id'] =  $_SESSION['uid'];
        $result =  $this->hmodel->newsettingsAdding($smainid, $settings);

        echo json_encode($result);
    }

    public function myarea()
    {
        $data = array();
        $data['list_distances'] =   $this->hmodel->getAllpincodes();
        $data['ressettings'] =   $this->hmodel->getRestSettings();
        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/home/myarea', $data);
        // $this->load->view('restaurant/layout/footer');
    }
    public function deactivaterest()
    {
        $result =   $this->hmodel->updateRestaurantStatus($_SESSION['uid']);
        $rslt = "restaurant status updated";
        echo json_encode($rslt);
    }
    public function newpincode()
    {
        $pcode = $this->input->post('newpincode');
        $result =   $this->hmodel->newpincode($_SESSION['uid'], $pcode);
        $rslt = "Pincode Added";
        echo json_encode($rslt);
    }
    public function pincodesupdate()
    {
        $_mianId = $this->input->post('_mianId');
        $pcode = $this->input->post('pincode');
        $result =   $this->hmodel->pincodesupdate($_mianId, $pcode);

        echo json_encode($result);
    }
    public function timeslotsupdate()
    {
        $_mianId = $this->input->post('_mianId');
        $pcode = $this->input->post('pincode');
        $endtime = $this->input->post('endtime');
        $result =   $this->hmodel->timeslotsupdate($_mianId, $pcode, $endtime);

        echo json_encode($result);
    }
    public function deletepincode()
    {

        $pcode = $this->input->post('pincode');
        $result =   $this->hmodel->pincodedelete($pcode);

        echo json_encode($result);
    }

    public function deletetimeslot()
    {
        $pcode = $this->input->post('pincode');
        $result =   $this->hmodel->deletetimeslot($pcode);

        echo json_encode($result);
    }

    public function deletedistance()
    {
        $pcode = $this->input->post('pincode');
        $result =   $this->hmodel->deletedistance($pcode);

        echo json_encode($result);
    }
    public function deletefoodmaster()
    {
        $masterId = $this->input->post('deleteid');
        $result =   $this->hmodel->deletefoodmaster($masterId);

        echo json_encode($result);
    }
    public function timeslot()
    {
        $data = array();
        $data['list_distances'] =   $this->hmodel->getAlltimeSlots();
        $data['ressettings'] =   $this->hmodel->getRestSettings();
        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/home/timeslot', $data);
    }

    public function newtimeslot()
    {

        $pcode = $this->input->post('newpincode');
        $newpincodeend = $this->input->post('newpincodeend');
        $result =   $this->hmodel->addNewTimeSlot($_SESSION['uid'], $pcode, $newpincodeend);
        $rslt = "Time Slot Added";
        echo json_encode($rslt);
    }
    public function token()
    {
        $tokenId = $this->input->post('tokenId');
        $result =   $this->hmodel->addNewToken($_SESSION['uid'], $tokenId);
        echo json_encode($result);
    }

    public function ourMenuSearch($offset = 0)
    {

        $fudname = $this->input->post('fudname');
        $this->load->library('pagination');
        $config['base_url'] = base_url('restaurant/home/listFoods');
        $config['total_rows'] = $this->hmodel->getnumOfRecordsSearch($fudname);
        $config['per_page'] = 3;
        //pagination styles start
        $config['full_tag_open'] = "<ul class='pagination newclasstart'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="nearby_page">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active active_pagination"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li class="test7">';
        $config['prev_tag_open'] = '<div class="previous newclasstart">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li class="test 2">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="test 1">';
        $config['last_tag_close'] = '</li>';



        $config['prev_link'] = '<i class="fa fa-long-arrow-left previouspagilink"></i>Previous Page';
        $config['prev_tag_open'] = '<li class="first_page">';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right nextpagilink"></i>';
        $config['next_tag_open'] = '<li class="nxt_pge">';
        $config['next_tag_close'] = '</div>';
        $config['next_tag_close'] = '</li>';

        // pagination style end
        $this->pagination->initialize($config);

        $data['menuitems'] =  $this->hmodel->ourMenuSearch($_SESSION['uid'], $fudname, $config['per_page'], $offset);
        $this->load->view('restaurant/layout/header');
        $this->load->view('restaurant/common/sidebar');
        $this->load->view('restaurant/home/listfudsearch', $data);
    }
}
