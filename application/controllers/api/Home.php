<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Alloe-Origin: *');
        $this->load->model('home_model');
        $this->load->library('Extended');
    }
    public function index()
    {
        echo "hello Foodo";
    }
    public function uploadUserFiles()
    {
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        $myvideo = isset($_FILES['myvideo']) ? ($_FILES['myvideo']) : '';
        $hasgTag = isset($_POST['hashTag']) ? trim($_POST['hashTag']) : '';
        $postlocation = isset($_POST['postlocation']) ? trim($_POST['postlocation']) : '';
        $post_title = isset($_POST['post_title']) ? trim($_POST['post_title']) : '';

        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$myvideo) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'File is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$postlocation) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'postlocation is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$post_title) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'post_title is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($myvideo) && ($postlocation) && ($post_title)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $allowed_vid_ext = ['mov', 'mpeg', 'mp3', 'avi', 'mp4'];
                $fileExt = pathinfo($_FILES["myvideo"]["name"], PATHINFO_EXTENSION);
                $file_name = $_FILES['myvideo']['name'];
                $file_name = preg_replace("/[^a-zA-Z0-9.]/", "", $file_name);
                if (in_array($fileExt, $allowed_vid_ext)) {
                    $configVideo['file_name'] = $file_name;
                    $configVideo['upload_path'] = 'assets/uservideo'; # check path is correct
                    // $configVideo['max_size'] = '10240000';
                    $configVideo['allowed_types'] = 'mov|mpeg|mp3|avi|mp4'; # add video extenstion on here
                    // $configVideo['overwrite'] = FALSE;
                    $configVideo['remove_spaces'] = TRUE;
                    // $video_name = 'midhu';
                    // $configVideo['file_name'] = $video_name;
                    // $file = $_FILES['upload'];
                    $upload_path = './assets';
                    // $config[' allowed_types '] = ' Mov|mpeg|mp3|avi|mp4 ';
                    // $config[' max_size '] = ' 2250000 ';
                    // $config [' max_width '] = ';
                    // $config [' max_height '] = ';
                    // Get the original file name from $_FILES
                    $file_name = $_FILES['myvideo']['name'];
                    // Remove any characters you don't want
                    // The below code will remove anything that is not a-z, 0-9 or a dot.
                    $file_name = preg_replace("/[^a-zA-Z0-9.]/", "", $file_name);
                    $this->load->library('upload', $configVideo);
                    $this->upload->initialize($configVideo);
                    if (!$this->upload->do_upload('myvideo')) {
                        // If there is any error
                        $err_msgs = ' Error in uploading ' . $this->upload->display_errors() . ' <br/> ';
                        $message = ['status' => false, 'statusCode' => Bad_Request, 'message' =>  $err_msgs];
                        $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                        $video_path = $data['upload_data']['file_name'];
                        $directory_path = $data['upload_data']['file_path'];
                        $directory_path_full = $data['upload_data']['full_path'];
                        $file_name = $data['upload_data']['raw_name'];
                        $var = new Extended();
                        $respsne =  $var->addWaterMarkAndText($directory_path_full, $video_path);
                        if ($respsne) {
                            $userDatas = array(
                                'userid' => $userId,
                                'file' => 'assets/userdata/' . $file_name . '.mp4',
                                'hashtag' => $hasgTag,
                                'post_title' => $post_title,
                                'user_post_location' => $postlocation,
                                'is_active' => 0,
                                'file_type' => 1,
                                'file_location' =>  $respsne
                            );
                            $insId =   $this->home_model->addnNewUserData($userDatas);
                            unlink($directory_path_full);
                            $message = ['status' => true, 'statusCode' => Successfull, 'message' => 'File uploaded'];
                            $this->json_output(Successfull, array('status' => Successfull, 'message' =>  $message));
                        } else {
                            unlink($directory_path_full);
                            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'Failed to upload'];
                            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
                        }
                    }
                } else {
                    //Image upload
                    $config['file_name'] = $file_name;
                    $config['upload_path'] = 'assets/uservideo';
                    $config['allowed_types'] = 'gif|jpeg|png|jpg';
                    // $config['max_height'] = '1000';
                    // $config['max_width'] = '2048';
                    // $config['max_size'] = '2048';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('myvideo')) {
                        $error = array('error' => $this->upload->display_errors());
                        $message = ['status' => false, 'statusCode' => Bad_Request, 'message' =>  $error];
                        $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
                    } else {
                        $dataUpload = $this->upload->data();
                        $data = array('upload_data' => $this->upload->data());
                        $image_path = $data['upload_data']['file_name'];
                        $directory_path = $data['upload_data']['file_path'];
                        $directory_path_full = $data['upload_data']['full_path'];
                        $file_name = $data['upload_data']['raw_name'];
                        //  'assets/userdata/' . $file_name . '.jpg';
                        $var = new Extended();
                        $respsne =  $var->addWaterMarkToImage($directory_path_full, $image_path);
                        if ($respsne) {
                            $userDatas = array(
                                'userid' => $userId,
                                'file' => 'assets/userdata/' . $file_name . '.jpg',
                                'hashtag' => $hasgTag,
                                'post_title' => $post_title,
                                'user_post_location' => $postlocation,
                                'is_active' => 0,
                                'file_type' => 2,
                                'file_location' =>  $respsne
                            );
                            $insId =   $this->home_model->addnNewUserData($userDatas);
                            unlink($directory_path_full);
                            $message = ['status' => true, 'statusCode' => Successfull, 'message' => 'File uploaded'];
                            $this->json_output(Successfull, array('status' => Successfull, 'message' =>  $message));
                        } else {
                            unlink($directory_path_full);
                            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'Failed to upload'];
                            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
                        }
                    }
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
    // get uploaded user files 
    public function getUserFiles()
    {
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $response =   $this->home_model->getAllFilesByUserId($userId);
                if ($response) {
                    $this->json_output(Successfull, array('status' => Successfull, 'data' =>  $response));
                } else {
                    $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' => 'Result not found'));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
    public function deleteUserFileById()
    {
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        $filesId = isset($_POST['filesId']) ? trim($_POST['filesId']) : '';
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$filesId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'filesId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($filesId)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $response =   $this->home_model->deleteUserFileById($userId, $filesId);
                if ($response > 0) {
                    $this->json_output(Successfull, array('status' => Successfull, 'message' =>  'File deleted successfully'));
                } else {
                    $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' => 'Failed to delete '));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
    public function homeData()
    {
        $latitude = isset($_POST['latitude']) ? trim($_POST['latitude']) : '';
        $longitude = isset($_POST['longitude']) ? ($_POST['longitude']) : '';
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        $postalcode = isset($_POST['postalcode']) ? trim($_POST['postalcode']) : '';
        $districtName = isset($_POST['districtName']) ? trim($_POST['districtName']) : '';
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$latitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'latitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$longitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'longitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$districtName) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'districtName is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($latitude) && ($longitude) && ($districtName)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $trendingVideo =  $this->home_model->getTrendingVideo($offfet = 0);
                $homesdatas =   $this->home_model->getHomdeDatas($userId, $latitude, $longitude, $postalcode, $districtName);
                $banners =  $this->home_model->getAdpromotions($districtName);
                $foobiesPost =  $this->home_model->getfoobiesPost($offfet = 0);
                if (($homesdatas) || ($trendingVideo) || ($banners) || ($foobiesPost)) {
                    $this->json_output(Successfull, array('status' => Successfull, 'result' => array('trendingVideo' => $trendingVideo, 'nearByRestaurant' => $homesdatas, 'foobiesPost' => $foobiesPost), 'ad' => $banners,));
                } else {
                    $this->json_output(Not_Found, array('status' => Not_Found, 'message' => 'no restaurants found'));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
    public function getHashtags()
    {
        $homesdatas =   $this->home_model->getHashtags();
        if ($homesdatas) {
            $this->json_output(Successfull, array('status' => Successfull, 'message' => 'success', 'data' => $homesdatas));
        } else {
            $this->json_output(Not_Found, array('status' => Not_Found, 'message' => 'no hashtags found'));
        }
    }

    public function userDeliveryAddress()
    {
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        $address_type = isset($_POST['address_type']) ? trim($_POST['address_type']) : '';
        $latitude = isset($_POST['latitude']) ? trim($_POST['latitude']) : '';
        $longitude = isset($_POST['longitude']) ? trim($_POST['longitude']) : '';
        $landmark = isset($_POST['landmark']) ? trim($_POST['landmark']) : '';
        $location_address = isset($_POST['location_address']) ? trim($_POST['location_address']) : '';
        $address = isset($_POST['address']) ? trim($_POST['address']) : '';
        $state = isset($_POST['state']) ? trim($_POST['state']) : '';
        $city = isset($_POST['city']) ? trim($_POST['city']) : '';
        $country = isset($_POST['country']) ? trim($_POST['country']) : '';
        $pincode = isset($_POST['pincode']) ? trim($_POST['pincode']) : '';
        $is_default = isset($_POST['is_default']) ? trim($_POST['is_default']) : '';
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        } else {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $response =   $this->home_model->getuserDeliveryAddress($userId);
                if ($response) {
                    $this->json_output(Successfull, array('status' => Successfull, 'results' =>  $response));
                } else {
                    $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' => 'Result not found/Deafult address not found'));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
    public function trendingVideos($offfet = 0)
    {
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        $exst = $this->checkUserExist($userId);
        if ($exst != 'null') {
            $tvideos =   $this->home_model->getTrendingVideo($offfet);
            if (($tvideos)) {
                $this->json_output(Successfull, array('status' => Successfull, 'result' => $tvideos));
            } else {
                $this->json_output(Not_Found, array('status' => Not_Found, 'message' => 'no trending videos found'));
            }
        } else {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
    }

    public function getfoobiesPost($offfet = 0)
    {
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        $exst = $this->checkUserExist($userId);
        if ($exst != 'null') {
            $fbspost =   $this->home_model->getfoobiesPost($offfet);
            if (($fbspost)) {
                $this->json_output(Successfull, array('status' => Successfull, 'result' => $fbspost));
            } else {
                return [];
            }
        } else {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
    }

    public function addAddress()
    {
        $userId =   $this->input->post('userId');
        $pincode = $this->input->post('pincode');
        $longitude =  $this->input->post('longitude');
        $latitude = $this->input->post('latitude');
        $location_address = $this->input->post('location_address');
        $address = $this->input->post('address');
        $phone_number = $this->input->post('phoneNumber');
        $landmark = $this->input->post('landmark');

        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$pincode) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'pincode is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$latitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'latitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$longitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'longitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$location_address) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'location_address is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$address) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'address is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$phone_number) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'phone_number is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$landmark) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'landmark is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (($userId) && ($pincode) && ($latitude) && ($longitude) && ($location_address) && ($address) && ($phone_number) && ($landmark)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $uData = $this->home_model->addNewAddress($userId, $pincode, $latitude, $longitude, $location_address, $address, $phone_number, $landmark);
                if (($uData)) {
                    $this->json_output(Successfull, array('status' => Successfull, 'message' => 'Address added'));
                } else {
                    $this->json_output(Not_Found, array('status' => Not_Found, 'message' => 'Failed to add address'));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
    public function editAddress()
    {
        $addressId =   $this->input->post('addressId');
        $userId =   $this->input->post('userId');
        $pincode = $this->input->post('pincode');
        $longitude =  $this->input->post('longitude');
        $latitude = $this->input->post('latitude');
        $location_address = $this->input->post('location_address');
        $address = $this->input->post('address');
        $phone_number = $this->input->post('phoneNumber');
        $landmark = $this->input->post('landmark');


        if (!$addressId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'addressId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($addressId)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $uData = $this->home_model->updateAddress($userId, $addressId, $pincode, $latitude, $longitude, $location_address, $address, $phone_number, $landmark);
                if (($uData)) {
                    $this->json_output(Successfull, array('status' => Successfull, 'message' => 'updated successfully'));
                } else {
                    $this->json_output(Not_Found, array('status' => Not_Found, 'message' => 'Failed to update address'));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
    public function deleteAddress()
    {
        $addressId =   $this->input->post('addressId');
        $userId =   $this->input->post('userId');
        if (!$addressId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'addressId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($addressId)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $uData = $this->home_model->deleteAddress($userId, $addressId);
                if (($uData)) {
                    $this->json_output(Successfull, array('status' => Successfull, 'message' => 'deleted successfully'));
                } else {
                    $this->json_output(Not_Found, array('status' => Not_Found, 'message' => 'Failed to delete address'));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    public function videoListing($offfet = 0)
    {
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        $exst = $this->checkUserExist($userId);
        if ($exst != 'null') {
            $tvideos =   $this->home_model->videoListing($offfet);
            if (($tvideos)) {
                $this->json_output(Successfull, array('status' => Successfull, 'result' => $tvideos));
            } else {
                $this->json_output(Not_Found, array('status' => Not_Found, 'message' => 'no  videos found'));
            }
        } else {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
    }

    public function  addVideoViews()
    {
        $userId = isset($_POST['postownerId']) ? trim($_POST['postownerId']) : '';
        $postId = isset($_POST['postId']) ? trim($_POST['postId']) : '';
        $viewduserId = isset($_POST['viewduserId']) ? trim($_POST['viewduserId']) : '';

        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$postId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'postId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$viewduserId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'viewduserId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (($userId) && ($postId) && ($viewduserId)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $tvideos =   $this->home_model->addVideoViews($userId, $postId, $viewduserId);
                if (($tvideos)) {
                    $this->json_output(Successfull, array('status' => Successfull, 'message' => 'View added'));
                } else {
                    $this->json_output(Not_Found, array('status' => Not_Found, 'message' => 'failed to add view'));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
}
