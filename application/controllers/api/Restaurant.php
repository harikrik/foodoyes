<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Restaurant extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Alloe-Origin: *');
        $this->load->model('restaurant_model');
        $this->load->library('Extended');
    }
    public function index()
    {
        echo "hello Restaurant";
    }

    public function restaurants()
    {

        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        $restaurantId = isset($_POST['restaurantId']) ? trim($_POST['restaurantId']) : '';
        $restaurantType = isset($_POST['restaurantType']) ? trim($_POST['restaurantType']) : '';
        $latitude = isset($_POST['latitude']) ? trim($_POST['latitude']) : '';
        $longitude = isset($_POST['longitude']) ? trim($_POST['longitude']) : '';
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$restaurantId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$restaurantType) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantType is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$latitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'latitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$longitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'longitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (($userId) && ($restaurantId) && ($restaurantType) && ($latitude) && ($longitude)) {

            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {


                $restaurantdats = $this->restaurant_model->getSelectedRestaurant($restaurantId, $restaurantType, $latitude, $longitude);
                $foodswithoffers = $this->restaurant_model->foodswithoffers($restaurantId);
                if (($restaurantdats)) {
                    $this->json_output(Successfull, array('status' => Successfull, 'result' => array('restaurnat' => $restaurantdats, 'offer' => $foodswithoffers)));
                } else {
                    $this->json_output(Not_Found, array('status' => Not_Found, 'message' => 'food items not found'));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    public function listCategory()
    {
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        $restaurantId = isset($_POST['restaurantId']) ? trim($_POST['restaurantId']) : '';
        $restaurantType = isset($_POST['restaurantType']) ? trim($_POST['restaurantType']) : '';
        $offset = isset($_POST['pagenumber']) ? trim($_POST['pagenumber']) : '';


        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$restaurantId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$restaurantType) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantType is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }



        if (($userId) && ($restaurantId) && ($restaurantType)) {

            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {


                $catg = $this->restaurant_model->listCategory($restaurantId, $restaurantType, $offset);
                // $foodswithoffers = $this->restaurant_model->foodswithoffers($restaurantId);
                if (($catg)) {
                    $this->json_output(Successfull, array('status' => Successfull, 'result' => array('category' => $catg)));
                } else {
                    $this->json_output(Not_Found, array());
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    public function listCategoryById()
    {
        $userId = isset($_POST['userId']) ? trim($_POST['userId']) : '';
        $restaurantId = isset($_POST['restaurantId']) ? trim($_POST['restaurantId']) : '';
        $restaurantType = isset($_POST['restaurantType']) ? trim($_POST['restaurantType']) : '';
        $categoryId = isset($_POST['categoryId']) ? trim($_POST['categoryId']) : '';
        $offset = isset($_POST['pagenumber']) ? trim($_POST['pagenumber']) : '';



        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$restaurantId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$restaurantType) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantType is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        // if (!$categoryId) {
        //     $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'categoryId is blank'];
        //     $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        // }

        if (($userId) && ($restaurantId) && ($restaurantType)) {

            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {


                $catg = $this->restaurant_model->listFudsById($restaurantId, $restaurantType, $offset, $categoryId);
                // $foodswithoffers = $this->restaurant_model->foodswithoffers($restaurantId);
                if (($catg)) {
                    $this->json_output(Successfull, array('status' => Successfull, 'result' => array('foods' => $catg)));
                } else {
                    $this->json_output(Not_Found, array('status' => Not_Found, 'result' => []));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
}
