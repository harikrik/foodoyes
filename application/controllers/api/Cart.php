<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Cart extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:https://app.foodoyes.com:');
        $this->load->model('cart_model');
    }
    public function index()
    {
        echo "hello cart";
    }

    public function cart()
    {
        $userId = $this->input->post('userId');
        $restaurantId =  $this->input->post('restaurantId');
        $cart =  $this->input->post('cart');


        $cart = json_decode($cart);


        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$restaurantId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$cart) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'cart is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }


        if (($userId) && ($restaurantId) && ($cart)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {

                $amt = 0;
                $totlTax = 0;
                $i = 0;
                foreach ($cart as $keys => $cval) {
                    $i++;
                    $foodamt =  $this->cart_model->getTotalAmount($cval->variantId, $cval->quantity);
                    $cheKTaxAmt = $this->cart_model->getTotalAmountForTax($cval->variantId, $cval->quantity);
                    $totlTax += $cheKTaxAmt;
                    $amt += $foodamt;
                }
                $mnmn =  $this->cart_model->getMinimumOrderAmount($restaurantId);


                $gstAmount = $amt - $totlTax;
                $payableAmount = $amt;

                $totlAmt = $amt - $gstAmount;
                if ($totlAmt > $mnmn->minimum_order_amount) {
                    $value = $this->is_decimal($payableAmount);
                    $masterId =   $this->cart_model->addNewcartMaster($restaurantId, $userId, $value, $gstAmount, $totlAmt);
                    if ($masterId) :
                        $response =  $this->cart_model->addCartItems($cart, $masterId);
                        if ($response) {
                            $data = array('cartId' => $masterId, 'payableAmount' => number_format($value));
                            $this->json_output(Successfull, array('status' => Successfull, 'result' =>  $data));
                        }
                    endif;
                } else {
                    $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'Minimun order amount' . $mnmn->minimum_order_amount];
                    $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    function is_decimal($val)
    {

        return number_format((float)$val, 2, '.', '');
    }

    public function cod()
    {
        $userId = $this->input->post('userId');
        $restaurantId = $this->input->post('restaurantId');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $cartId =  $this->input->post('cartId');
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$longitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'longitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$latitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'latitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$cartId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'cartId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$restaurantId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($restaurantId) && ($cartId) && ($latitude) && ($longitude)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {

                $result = $this->cart_model->getCodCheckOutPage($userId, $cartId, $latitude, $longitude, $restaurantId);

                if ($result) {
                    $this->json_output(Successfull, array('status' => Successfull, 'result' =>  $result));
                } else {
                    $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $result));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    public function bank()
    {
        $restaurantId = $this->input->post('restaurantId');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $userId = $this->input->post('userId');
        $cartId =  $this->input->post('cartId');


        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$restaurantId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$longitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'longitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$latitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'latitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$cartId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'cartId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($restaurantId) && ($cartId) && ($latitude) && ($longitude)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $result = $this->cart_model->getBankCheckOutPage($userId, $cartId, $latitude, $longitude, $restaurantId);

                $timeslots = $this->cart_model->getDeliveryMode($restaurantId);

                $data = array('payableData' => array($result), 'timeslot' => $timeslots);
                $this->json_output(Successfull, array('status' => Successfull, 'result' =>  $data));
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    public function codCheckOut()
    {
        $cartId =  $this->input->post('cartId');
        $userId = $this->input->post('userId');
        $token = $this->input->post('token');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');

        if (!$longitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'longitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$latitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'latitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$token) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'token is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$cartId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'cartId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($cartId) && ($token) && ($longitude) && ($latitude)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $status = $this->cart_model->updateCodCheckOut($cartId, $userId, $token, $longitude, $latitude);
                if ($status > 0) {
                    $this->json_output(Successfull, array('status' => Successfull, 'message' =>  'Order placed successfully'));
                } else {
                    $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  'Failed to place order'));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    public function bankCheckOut()
    {
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');

        $cartId =  $this->input->post('cartId');
        $restaurantId =  $this->input->post('restaurantId');

        $userId = $this->input->post('userId');
        $deliveryModeId = $this->input->post('deliveryModeId');
        $timeSlotId = $this->input->post('timeSlotId');
        $date = $this->input->post('date');
        $token = $this->input->post('token');

        if (!$timeSlotId) {
            $timeSlotId = 1;
        }


        if (!$longitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'longitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$latitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'latitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$token) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'token is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$cartId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'cartId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$timeSlotId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'timeSlotId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$date) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'date is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$restaurantId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$deliveryModeId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'deliveryModeId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }



        if (($userId) && ($cartId) && ($date) && ($token) && ($longitude) && ($latitude) && ($restaurantId)  && ($deliveryModeId)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $rslt = $this->cart_model->bankCheckOut($userId, $cartId, $latitude, $longitude, $restaurantId, $date, $timeSlotId, $token, $deliveryModeId);
                $this->json_output(Successfull, array('status' => Successfull, 'result' =>  $rslt));
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    public function generateCoupon()
    {
        $oamt =  $this->input->post('orderAmount');
        $userId = $this->input->post('userId');
        if (!$oamt) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'orderAmount is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($oamt)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $rslt = $this->cart_model->generateCoupon($oamt, $userId);
                $this->json_output(Successfull, $rslt);
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
    public function applyCoupon()
    {
        $cartId =  $this->input->post('cartId');
        $userId = $this->input->post('userId');
        $couponcode = $this->input->post('couponcode');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $restaurantId = $this->input->post('restaurantId');

        if (!$longitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'longitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$latitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'latitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$couponcode) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'couponcode is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$cartId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'cartId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$restaurantId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }


        if (($userId) && ($cartId) && ($couponcode) && ($longitude) && ($latitude) && ($restaurantId)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $rslt = $this->cart_model->applyCoupon($userId, $cartId, $latitude, $longitude, $restaurantId, $couponcode);
                $this->json_output(Successfull, array('status' => Successfull, 'result' =>  $rslt));
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
    public function removeCoupon()
    {
        $cartId =  $this->input->post('cartId');
        $userId = $this->input->post('userId');
        $couponcode = $this->input->post('couponcode');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $restaurantId = $this->input->post('restaurantId');

        if (!$longitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'longitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$latitude) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'latitude is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$couponcode) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'couponcode is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$cartId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'cartId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$restaurantId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }


        if (($userId) && ($cartId) && ($couponcode) && ($longitude) && ($latitude) && ($restaurantId)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $rslt = $this->cart_model->removeCoupon($userId, $cartId, $latitude, $longitude, $restaurantId, $couponcode);
                $this->json_output(Successfull, array('status' => Successfull, 'result' =>  $rslt));
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    public function orderHistory()
    {
        $userId = $this->input->post('userId');
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if ($userId) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $histry =  $this->cart_model->orderHistory($userId);
                $this->json_output(Successfull, array('status' => Successfull, 'result' =>  $histry));
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    public function orderDetails()
    {
        $userId = $this->input->post('userId');
        $orderId = $this->input->post('orderId');
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$orderId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($orderId)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $histry =  $this->cart_model->orderDetails($userId, $orderId);
                $this->json_output(Successfull, array('status' => Successfull, 'result' =>  $histry));
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }

    public function trackMyOrder()
    {
        $userId = $this->input->post('userId');
        $orderId = $this->input->post('orderId');
        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (!$orderId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($orderId)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $histry =  $this->cart_model->trackMyOrder($userId, $orderId);
                $this->json_output(Successfull, array('status' => Successfull, 'result' =>  $histry));
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
    public function razorresponse()
    {


        $payment_id = $this->input->post('razorpay_payment_id');
        $razorpay_order_id =   $this->input->post('razorpay_order_id');
        $signature = $this->input->post('razorpay_signature');
        $order_id = $this->input->post('order_id');
        $response =  $this->cart_model->razorresponse($payment_id, $razorpay_order_id, $signature, $order_id);
        if ($response) {
            $message = ['status' => false, 'statusCode' => Successfull, 'message' => 'Payment successful'];
            $this->json_output(Successfull, array('status' => Successfull, 'message' =>  $message));
        } else {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'Payment unsuccessful'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
    }

    public function checkCustomerDistance()
    {
        $restaurantId =  $this->input->post('restaurantId');
        $userId = $this->input->post('userId');
        if (!$restaurantId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'restaurantId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }

        if (!$userId) {
            $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'userId is blank'];
            $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        }
        if (($userId) && ($restaurantId)) {
            $exst = $this->checkUserExist($userId);
            if ($exst != 'null') {
                $rslt = $this->cart_model->checkCustomerDistance($restaurantId, $userId);

                if ($rslt > 0) {

                    $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  'Delivery accepted below ' . $rslt . ' km'));
                } else {
                    $this->json_output(Successfull, array('status' => Successfull, 'message' =>  'Deliverable area '));
                }
            } else {
                $message = ['status' => false, 'statusCode' => Bad_Request, 'message' => 'User Not Exist'];
                $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
            }
        }
    }
}
