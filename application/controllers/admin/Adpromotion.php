<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdPromotion extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->is_session_exist();
        $this->load->model('admin/adpromotion_model', 'apmodel');
    }

    public function index()
    {
        $data = [];
        $data['country'] = loadcountry();
        $data['promotions'] = $this->apmodel->getAllPromotions();

        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/promotion/manage', $data);
        // $this->load->view('admin/layout/footer');
    }
    public function varient()
    {
        $data = [];

        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/fccat/varient', $data);
        $this->load->view('admin/layout/footer');
    }
    public function getstatebyid()
    {
        $cntryid =  $this->input->post('countryid');
        $resp =  getStateNameByCntryId($cntryid);
        echo json_encode($resp);
    }
    public function getdistrictbyid()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   getCountryNameById($cityid);
        echo json_encode($resp);
    }
    public function getcitiesbydid()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   getCityNameByDistrictId($cityid);
        echo json_encode($resp);
    }
    public function newpromotion()
    {
        $rslr = '';
        $bnr['country_id'] = $this->input->post('countryId');
        $bnr['state_id'] = $this->input->post('stateid');
        $bnr['district_id'] = $this->input->post('districtId');
        $bnr['city_id'] = $this->input->post('districtId');
        $bnr['starting_date'] = $this->input->post('stdate');
        $bnr['ending_date'] = $this->input->post('enddate');
        $bnr['restaurant_id'] = $this->input->post('cityId');
        $this->res = $this->apmodel->newBannerData($bnr);
        if ($this->res) {
            $this->load->library('upload');

            $files = $_FILES;
            $count = count($_FILES['bannerimage']['name']);
            for ($i = 0; $i < $count; $i++) {
                $_FILES['bannerimage']['name'] = $files['bannerimage']['name'][$i];
                $_FILES['bannerimage']['type'] = $files['bannerimage']['type'][$i];
                $_FILES['bannerimage']['tmp_name'] = $files['bannerimage']['tmp_name'][$i];
                $_FILES['bannerimage']['error'] = $files['bannerimage']['error'][$i];
                $_FILES['bannerimage']['size'] = $files['bannerimage']['size'][$i];

                $this->upload->initialize($this->set_upload_options()); //function defination below
                $this->upload->do_upload('bannerimage');
                $upload_data = $this->upload->data();
                $name_array[] = $upload_data['file_name'];
                $fileName = $upload_data['file_name'];
                $brnimgs['ad_images'] = 'assets/sadmin/banner/' . $fileName;
                $brnimgs['ad_promotion_id'] = $this->res;
                $rslr =  $this->result = $this->apmodel->newBannerImages($brnimgs);
            }
            echo json_encode($rslr);
        }
    }

    function set_upload_options()
    {
        // upload an image options
        $config = array();
        //    $config['upload_path'] = LARGEPATH; //give the path to upload the image in folder
        $config['remove_spaces'] = TRUE;
        $config['upload_path'] = 'assets/sadmin/banner/';
        $config['allowed_types'] = 'gif|jpeg|png|jpg';
        $config['allowed_types'] = 'gif|jpg|png';

        return $config;
    }
    public function changereststs()
    {
        $sts = $this->input->post('id');
        $resp =  $this->apmodel->changeresSts($sts);
        echo json_encode($resp);
    }
    public function deletepromo()
    {
        $id = $this->input->post('id');
        $resp =  $this->apmodel->deletepromo($id);
        echo json_encode($resp);
    }
    public function getcountryrestaurants()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   $this->apmodel->getcountryrestaurants($cityid);
        echo json_encode($resp);
    }

    public function getdistrictsrestaurants()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   $this->apmodel->getdistrictsrestaurants($cityid);
        echo json_encode($resp);
    }
    public function getdisres()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   $this->apmodel->getdisres($cityid);
        echo json_encode($resp);
    }
}
