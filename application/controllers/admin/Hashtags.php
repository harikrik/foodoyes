<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hashtags extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->is_session_exist();
        $this->load->model('admin/Hashtag_model', 'hmodel');
    }
    public function index()
    {
        $data = [];
        $data['country'] = loadcountry();
        $data['allhashes'] = $this->hmodel->getAllHash();
        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/hashtag/manage', $data);
        // $this->load->view('admin/layout/footer');
    }
    public function addNewHashTag()
    {
        $ht['hash_tag_name'] =  $this->input->post('htag');
        $resp =   $this->hmodel->addNewHasTag($ht);
        echo json_encode($resp);
    }
    public function deletehash()
    {
        $delId =  $this->input->post('htag');
        $resp =   $this->hmodel->deleteNewHasTag($delId);
        echo json_encode($resp);
    }
}
