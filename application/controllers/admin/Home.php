<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{



    public function __construct()
    {

        parent::__construct();
        $this->is_session_exist();
        $this->load->model('admin/home_model', 'hmodel');
    }

    public function index()
    {
        $data = [];
        $data['country'] = loadcountry();
        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/home/manage', $data);
        $this->load->view('admin/layout/footer');
    }
    public function getstatebyid()
    {
        $cntryid =  $this->input->post('countryid');
        $resp =  getStateNameByCntryId($cntryid);
        echo json_encode($resp);
    }
    public function getdistrictbyid()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   getCountryNameById($cityid);
        echo json_encode($resp);
    }
    public function getcitiesbydid()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   getCityNameByDistrictId($cityid);
        echo json_encode($resp);
    }
    public function addNewRestaurant()
    {
        $resturant['restaurant_name'] = $this->input->post('restuarant_name');
        $resturant['country'] = $this->input->post('country');
        $resturant['state'] = $this->input->post('state');
        $resturant['district'] = $this->input->post('district');
        $resturant['city'] = $this->input->post('city');
        $resturant['restaurant_category'] = $this->input->post('restaurant_cat');
        $resturant['contact_no'] = $this->input->post('contact_no');
        $resturant['pincode'] = $this->input->post('pincode');
        $resturant['longitude'] = $this->input->post('lon');
        $resturant['latitude'] = $this->input->post('lat');
        $uname = $this->input->post('uname');
        $password = $this->input->post('pasword');
        $lon = $this->input->post('lon');
        $lat = $this->input->post('lat');


        $folder = 'assets/sadmin/restaurant/';
        if (!file_exists($folder)) {

            mkdir($folder, 777, true);
        }

        if (isset($_FILES['reasturant_image1'])) {

            $tmp_name = $_FILES["reasturant_image1"]["tmp_name"];
            $name = basename($_FILES["reasturant_image1"]["name"]);
            move_uploaded_file($tmp_name, "$folder/$name");
            $resturant['reasturant_image_one'] =  $folder  . $name;
        }

        if (isset($_FILES['reasturant_image2'])) {

            $tmp_name = $_FILES["reasturant_image2"]["tmp_name"];
            $name = basename($_FILES["reasturant_image2"]["name"]);
            move_uploaded_file($tmp_name, "$folder/$name");
            $resturant['reasturant_image_two'] =  $folder  . $name;
        }

        if (isset($_FILES['reasturant_image3'])) {

            $tmp_name = $_FILES["reasturant_image3"]["tmp_name"];
            $name = basename($_FILES["reasturant_image3"]["name"]);
            move_uploaded_file($tmp_name, "$folder/$name");
            $resturant['reasturant_image_three'] =  $folder  . $name;
        }



        $response =   $this->hmodel->addNewresturant($resturant, $uname, $password, $lon, $lat);
        echo json_encode($response);
    }
}
