<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Food_Category extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->is_session_exist();
        $this->load->model('admin/foodcat_model', 'fcmodel');
    }

    public function index()
    {
        $data = [];
        $data['fudcats'] = $this->fcmodel->getAllFudCats();
        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/fccat/manage', $data);
        // $this->load->view('admin/layout/footer');
    }
    public function varient()
    {
        $data = [];
        $data['fudvarnts'] = $this->fcmodel->getAllFudVarnts();
        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/fccat/varient', $data);
        // $this->load->view('admin/layout/footer');
    }
    // public function getstatebyid()
    // {
    //     $cntryid =  $this->input->post('countryid');
    //     $resp =  getStateNameByCntryId($cntryid);
    //     echo json_encode($resp);
    // }
    // public function getdistrictbyid()
    // {
    //     $cityid =  $this->input->post('cityid');
    //     $resp =   getCountryNameById($cityid);
    //     echo json_encode($resp);
    // }
    // public function getcitiesbydid()
    // {
    //     $cityid =  $this->input->post('cityid');
    //     $resp =   getCityNameByDistrictId($cityid);
    //     echo json_encode($resp);
    // }
    public function newcategory()
    {
        $catarr['food_category_name'] = $this->input->post('catname');
        $config['upload_path'] = 'assets/sadmin/';
        $config['allowed_types'] = 'gif|jpeg|png|jpg';
        // $config['max_height'] = '1000';
        // $config['max_width'] = '2048';
        // $config['max_size'] = '2048';
        $this->load->library('upload', $config);


        $this->upload->initialize($config);

        if ($this->upload->do_upload('caticon')) {
            $data['upload'] =  $this->upload->data();
            $fname = $data['upload']['file_name'];
            $catarr['food_category_icon'] = 'assets/sadmin/' . $fname;
        }
        $resp =   $this->fcmodel->newFoodcategory($catarr);
        echo json_encode($resp);
    }

    public function newvarient()
    {
        $catarr['food_variant_name'] = $this->input->post('catname');
        $config['upload_path'] = 'assets/sadmin/varient';
        $config['allowed_types'] = 'gif|jpeg|png|jpg';
        // $config['max_height'] = '1000';
        // $config['max_width'] = '2048';
        // $config['max_size'] = '2048';
        // $this->load->library('upload', $config);


        // $this->upload->initialize($config);

        // if (!$this->upload->do_upload('caticon')) {
        //     $error = array('error' => $this->upload->display_errors());
        //     $message = ['status' => false, 'statusCode' => Bad_Request, 'message' =>  $error];
        //     $this->json_output(Bad_Request, array('status' => Bad_Request, 'message' =>  $message));
        // } else {
        //     $data['upload'] =  $this->upload->data();
        //     $fname = $data['upload']['file_name'];
        //     $catarr['food_variant_icon'] = 'assets/sadmin/varient/' . $fname;
        // }
        $resp =   $this->fcmodel->newFoodVariant($catarr);
        echo json_encode($resp);
    }

    public function deletevarient()
    {
        $vid = $this->input->post('id');
        $resp =   $this->fcmodel->deletevarient($vid);
        echo json_encode($resp);
    }
    public function deletecategory()
    {
        $vid = $this->input->post('id');
        $resp =   $this->fcmodel->deletecategory($vid);
        echo json_encode($resp);
    }
}
