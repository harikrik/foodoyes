<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Restaurant extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->is_session_exist();
        $this->load->model('admin/restaurant_model', 'rmodel');
    }

    public function index()
    {
        $data = [];
        $data['country'] = loadcountry();
        $data['restuarants'] = $this->rmodel->getAllResTaurant();
        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/restaurant/manage', $data);
        // $this->load->view('admin/layout/footer');
    }
    public function getstatebyid()
    {
        $cntryid =  $this->input->post('countryid');
        $resp =  getStateNameByCntryId($cntryid);
        echo json_encode($resp);
    }
    public function getdistrictbyid()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   getCountryNameById($cityid);
        echo json_encode($resp);
    }
    public function getcitiesbydid()
    {
        $cityid =  $this->input->post('cityid');
        $resp =   getCityNameByDistrictId($cityid);
        echo json_encode($resp);
    }
    public function addNewRestaurant()
    {

        $resturant['restaurant_name'] = $this->input->post('reasturant_name');
        $resturant['country'] = $this->input->post('country_name');
        $resturant['state'] = $this->input->post('state_name');
        $resturant['district'] = $this->input->post('district_name');
        $resturant['city'] = $this->input->post('city_name');
        $resturant['restaurant_category'] = $this->input->post('res_cat');
        $resturant['contact_no'] = $this->input->post('contact_no');
        $resturant['pincode'] = $this->input->post('pin_code');
        $resturant['longitude'] = $this->input->post('r_longitude');
        $resturant['latitude'] = $this->input->post('r_latitude');
        $uname = $this->input->post('r_uname');
        $password = $this->input->post('r_password');
        $lon = $this->input->post('r_longitude');
        $lat = $this->input->post('r_latitude');

        $folder = 'assets/sadmin/restaurant/';
        if (!file_exists($folder)) {

            mkdir($folder, 777, true);
        }

        if (isset($_FILES['reasturant_image1'])) {

            $tmp_name = $_FILES["reasturant_image1"]["tmp_name"];
            $name = basename($_FILES["reasturant_image1"]["name"]);
            move_uploaded_file($tmp_name, "$folder/$name");
            $resturant['reasturant_image_one'] =  $folder  . $name;
        }

        if (isset($_FILES['reasturant_image2'])) {

            $tmp_name = $_FILES["reasturant_image2"]["tmp_name"];
            $name = basename($_FILES["reasturant_image2"]["name"]);
            move_uploaded_file($tmp_name, "$folder/$name");
            $resturant['reasturant_image_two'] =  $folder  . $name;
        }

        if (isset($_FILES['reasturant_image3'])) {

            $tmp_name = $_FILES["reasturant_image3"]["tmp_name"];
            $name = basename($_FILES["reasturant_image3"]["name"]);
            move_uploaded_file($tmp_name, "$folder/$name");
            $resturant['reasturant_image_three'] =  $folder  . $name;
        }



        $response =   $this->rmodel->addNewresturant($resturant, $uname, $password, $lon, $lat);
        echo json_encode($response);
    }
    public function changereststs()
    {
        $sts = $this->input->post('status');
        $resp =  $this->rmodel->changeresSts($sts);
        echo json_encode($resp);
    }
    public function edit($id)
    {
        $data = [];
        $id = base64_decode($id);
        $data['restaurant'] =  $this->rmodel->getrestaurantEdit($id);
        $data['country'] = loadcountry();


        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/restaurant/editrestaurant', $data);
    }

    public function updaterestaurant()
    {

        $resmasterid =  $this->input->post('resmasterid');

        $resturant['restaurant_name'] = $this->input->post('reasturant_name');
        $resturant['country'] = $this->input->post('country_name');
        $resturant['state'] = $this->input->post('state_name');
        $resturant['district'] = $this->input->post('district_name');
        $resturant['city'] = $this->input->post('city_name');
        $resturant['restaurant_category'] = $this->input->post('res_cat');
        $resturant['contact_no'] = $this->input->post('contact_no');
        $resturant['pincode'] = $this->input->post('pin_code');
        $resturant['longitude'] = $this->input->post('r_longitude');
        $resturant['latitude'] = $this->input->post('r_latitude');
        $uname = $this->input->post('r_uname');
        $password = $this->input->post('r_password');
        $lon = $this->input->post('r_longitude');
        $lat = $this->input->post('r_latitude');


        $folder = 'assets/sadmin/restaurant/';
        if (!file_exists($folder)) {

            mkdir($folder, 777, true);
        }

        if (isset($_FILES['reasturant_image1'])) {

            $tmp_name = $_FILES["reasturant_image1"]["tmp_name"];
            $name = basename($_FILES["reasturant_image1"]["name"]);
            move_uploaded_file($tmp_name, "$folder/$name");
            $resturant['reasturant_image_one'] =  $folder  . $name;
        }

        if (isset($_FILES['reasturant_image2'])) {

            $tmp_name = $_FILES["reasturant_image2"]["tmp_name"];
            $name = basename($_FILES["reasturant_image2"]["name"]);
            move_uploaded_file($tmp_name, "$folder/$name");
            $resturant['reasturant_image_two'] =  $folder  . $name;
        }

        if (isset($_FILES['reasturant_image3'])) {

            $tmp_name = $_FILES["reasturant_image3"]["tmp_name"];
            $name = basename($_FILES["reasturant_image3"]["name"]);
            move_uploaded_file($tmp_name, "$folder/$name");
            $resturant['reasturant_image_three'] =  $folder  . $name;
        }

        $response =   $this->rmodel->updateResturant($resturant, $uname, $password, $lon, $lat, $resmasterid);
        echo json_encode($response);
    }
    public function deleterestaurant()
    {
        $id =  $this->input->post('id');
        $response =   $this->rmodel->deleterestaurant($id);
        echo json_encode($response);
    }
}
