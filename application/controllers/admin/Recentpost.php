<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RecentPost extends MY_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->is_session_exist();
        $this->load->model('admin/recentpost_model', 'rmodel');
    }
    public function index()
    {
        $data = [];
        $data['country'] = loadcountry();
        $data['ufiles'] = $this->rmodel->getAllPosts();
        $this->load->view('admin/layout/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/recentpost/manage', $data);
        // $this->load->view('admin/layout/footer');
    }
    public function aprovepost()
    {
        $id = $this->input->post('id');
        $resp =  $this->rmodel->aprovepost($id);
        echo json_encode($resp);
    }
    public function deleteposts()
    {
        $id = $this->input->post('id');
        $resp =  $this->rmodel->deleteposts($id);
        echo json_encode($resp);
    }
}
